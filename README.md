# README #
** 本文書は下記のレポジトリ内に内包されている. **  
コードはGitレポジトリにより管理しており, 以下の公開レポジトリをCloneすることで閲覧できる.  
`git clone git@bitbucket.org:cobook/pic_master.git`   
なお, `git clone`後には必ず`git pull`を行うこと. 
詳細なGitレポジトリの利用方法は公式サイトにゆずる.   

## What is this repository for? ##
長谷川研究室藤井(H29)卒が修士論文のために製作した2次元(zr)3速度(zrθ)円筒座標コード. 
Hybrid Particle-in-Cellによるレーザー生成プラズマと縦磁場との相互作用を計算した. 
** 修士論文題目 : レーザー生成プラズマ中のイオン運動量分布に及ぼす縦磁場の影響  **
本コードはMATLABスクリプトよりコードを実行する. 電磁場および粒子の計算には[MEX-C/C++ルーチン](https://jp.mathworks.com/help/matlab/write-cc-mex-files.html)を用いた.


### レポジトリの概要 ###

`HYBRID`ディレクトリを基点として, `mexfunctions`, `src`, `workspace_template`を確認することが出来る. 
このうち,  
    * `mexfunctions`はMEX-C/C++によるルーチンを格納する.   
    * `src`中の関数は`main_func.m`によって呼び出され, `mexfunctions`を呼び出すシークエンスが記述されている. このシークエンスに関するフロー・チャートは修士論文Fig. 8を参照のこと.    
    * `workspace_template`は実行ファイル`init_cond.m`を有しており, `src/main_func.m`を呼び出す.   
以下では実行環境の導入, MATLABにおける変数へのアクセス, 実行手順について記す.  


### 動作環境 ###
計算に用いた動作環境は以下の通り. それぞれ公式サイトのインストール手順を参考のこと.   
    * OS:				CentOS7   
    * NVRM version: 	NVIDIA UNIV x86_64 Kernel Module 375.51  
    * GCC version:  	gcc version 4.8.5 20150623   
    * MATLAB:			9.2.0.556344 (R2017a)  
MEXファイルをBuildするため, MATLAB Coderなど必要なライブラリも同時にインストールする必要がある. 必要なライブラリをインストールしていない場合には関数毎にどのライブラリをインストールするのか指示があるため, 表示された指示に従うこと.   


### 計算コードの概説 ###
* 行列の基本構造  
行列の添字は
    1. 場の物理量は(z方向のセル,r方向のセル,θ方向のセル)
    2. 粒子の物理量は(粒子のid, 次元(1->z,2->r,3->θ))  
の順である. 通常は(r,θ,z)とすべきであるが, ２次元座標系でコード作成を開始した名残で(z,r,θ)となった. 

* 計算領域      
計算領域の概要は修士論文Fig. 11を参照. 
磁場を定義するセルは, 定義点が原点に重なるように定義している. これをG0とする. 
一方で電場を定義するセルはz, r両方向に半整数グリッド異なる位置に定義され, これをG1とする. 
磁場をG1よりさらに半整数グリッド多くとり拡張したものをG2とする. 

* Electric/Magnetic Field クラス   
電場及び磁場のグリッド上の値は, 3次元的な行列により用意されている. 
`ElectricField_HalfGrid.m`および`MagneticField.m`クラスは`Field`クラスの変数を引き継いで生成される.  
クラスインスタンスへのアクセスを行う際には, 明示的でない場合, Electric fieldの場合はG1, Magnetic fieldの場合はG0への参照が行われることに注意する. 

* Particle/Ghost クラス   
粒子の移動, 粒子の入射スキーム, Particle-in-cell法のための位置情報の取得, 領域外の粒子の削除を行う.  
ここで, Ghostは修士論文3.3.1節において記述した仮想粒子への操作を行うメソッドを含む. 
また, Particle/Ghostクラスに対して操作を行う`FLUX_weight.m` は 
Particle-in-Cellの核である, 粒子の重み(数密度, 電流密度(速度))をセルに分配する機能を内包する. 

* コイル付近の境界条件   
イオンはコイル中に侵入すると消失する. 消失したステップ数は, Particleクラスの従属変数であるitに保存され, 消失粒子の位置および速度は以降不変となる.  


### 実行手順 ###
計算コードの実行手順は以下の通り

0. シード粒子の作成 ` src/Prepare_Particles.m `  
本レポジトリを`git clone`した時点で実行結果が`src/Init_part_pos`に格納されているため本項目は飛ばしても構わない. 粒子数を変更する, イオンの運動量分散などのパラメータを変更するなどの必要があれば`Prepare_Particles.m`を実行する.    
レポジトリをcloneした時点で`src/Init_part_pos`中には120mJのレーザー強度で生成した銅プラズマ中のイオンの運動量分布条件が保存されている. 本粒子分布は実験で得られたフラックス分布から導出している. アルゴリズムの詳細は修士論文4.7節を参照.   
また, 本計算コードでは, `src/Particle.m`により記述されるParticleクラスの`function experimental_param_read`によりInit_part_posに記述された情報を呼び出す.  なお, BEAMTYPEを変更することにより一様粒子分布の生成, Shifted-Maxwellian分布の生成も可能であるが, アップデートを繰り返したため動作は保証できず, 本文書ではサポートできない. 
1. MEX関数のビルド ` mexfunctions/build_mex.m `により時間発展のルーチンをコンパイルする. 
2. `workspace_template`を`workspace_hogehoge`にコピーし、実行用のワークスペースを作成する. コードの実行は`workspace_hogehoge`中で行う.  
3. 実行時間及び計算領域上の超粒子数といった初期条件を変えたい場合は, `workspace_hogehoge/Input/input_driftMB.m`を必要に応じて編集する. パラメータサーベイを開始した後は, このファイル中のパラメータは変更しないことが望ましい.   
4. 電流値およびコイル配置の設定を ` workspace_hogehoge/init_cond.m`にて行う. コイルの配置は横軸にz, 縦軸にrをとった座標系において矩形状に配置する. 原点に一番近い点を変数`coil_pos=[z position, r position]`により定め, z方向およびr方向へのコイルの長さを`coil_length=[z length, r length]`に記述する. 変数`crnt`は励磁電流値を示しており, 修士論文の3.4.6節(3.59)式中のI<sub>ext</sub>に対応する. 
5. workspaceフォルダ内で`mkdir result`により実行結果の保存先フォルダを作成. 
6. `init_cond(I:任意の電流値)`を実行する. I=4000程度で中心軸上での磁場強度B<sub>ref</sub>=1.0 Tに対応する. 計算が発散する場合にはIを小さくとって試して欲しい. 
7. `workspace_hogehoge/result/`以下にファイルが保存される. 


### 解析 ###
* データベースの概略  
データは`workspace_hogehoge/result`内に`.mat`ファイルとして格納される.  
MATLABのUIを利用している場合, 格納された.matファイルをダブルクリックしてMATLABのworkspaceに読み込ませることで, 一変数として呼び出すことが出来る. 格納した変数は`plot`や`imagesc`などで可視化出来る. 一度全ての変数について確認することが望ましい.   
なお, `matlab.mat`には初期状態における全ての変数値が保存されており, 解析の際にはこのファイルを最初に呼び出すことで, 解析時に各種パラメータを呼び出すことが可能である. 

* 解析コードの概要  
修士論文の図に用いた解析コードは`workspace_template`中の, `plot_DIAGNOSIS.m`(物理量の可視化全般), `plot_TRAJECTORY.m`(粒子軌道の可視化), `plot_ANGULAR_CURRENT`(フラックスの角度依存性)の三種類である. 
`folder=`において計算結果が格納されたフォルダ名を指定することで解析を行い, `workspace_hogehoge/seminar`以下に結果を保存する. (メンテナンス不足)

### 本コードで未解決の問題 ###
修士論文の3.4.9節を参照のこと. 
Hybrid PICに特有のコイルとプラズマ領域の境界の問題, プラズマと真空領域の問題が判然としない限り, 残念ながらシミュレーション結果が正しいとは言えない. 

### 結び ###
修士論文発表会では定性的な結果を示したに過ぎず問題は山積みであった. また, 開発者はプラズマとは何か, PICとは何かを理解するところからの手探りの開発に始まった. 二年間の短い短距離走を想定していたため, 結果の可視化が容易なMATLABにより記述を開始した. このため計算速度の観点からも本コードを今後の研究に使うことは考えられない.

今回アルゴリズムとして利用したCAM-CL法は, 低密度領域を上手く扱えない問題があり, 多大な労力を費やしたがこの結果が芳しく無かった. 
従って, 今後Hybrid Particle-in-cellにて数値解析を行う場合には, 低密度の電子を扱える最新の手法を用いるのが望ましく, 具体的には以下のそれぞれの手法を是非とも比較してみてほしい.   
    * Muñoz, P. A., et al. "A new hybrid code (CHIEF) implementing the inertial electron fluid equation without approximation." arXiv preprint arXiv:1612.03818 (2016).   
    * Amano, Takanobu, Katsuaki Higashimori, and Keisuke Shirakawa. "A robust method for handling low density regions in hybrid simulations for collisionless plasmas." Journal of Computational Physics 275 (2014): 197-212.  
    * Kunz, Matthew W., James M. Stone, and Xue-Ning Bai. "Pegasus: A new hybrid-kinetic particle-in-cell code for astrophysical plasma dynamics." Journal of Computational Physics 259 (2014): 154-174.    
いずれの手法もプラズマ中の衝撃波動など高周波の現象を対象としたコードであるため大きな計算コストを要するが, 
モデルの妥当性が得られないまま計算を行い, 
そのことを後から釈明するよりはるかによいだろうというのが, 修士論文を通じて得た教訓である. 



<!--
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
* Repo owner or admin
* Other community or team contact
-->


<!--
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
-->

