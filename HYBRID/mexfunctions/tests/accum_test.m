n = 1;
ITE = 10000;
A = floor(rand(100000,2)*n) + 1.0;

tic
for i = 1:1000
accumarray(A(:,1),A(:,2));
end
toc
tic
for i = 1:1000
mex_accumarray(A);
end
toc



%%

np = 1000;
nz = 10;
nr = 20;

vel = [1:3*np]';
vel = reshape(vel, [np, 3]);

cell_idx = floor(rand(4*np,1)*(nz*nr-5)) + 1.0;

cell_weight = ones(4*np,1);
tic
for i = 1:ITE
    [chg1, flux1] = mex_flux_weight(cell_idx(:),cell_weight(:), vel(:), nz, nr, np);
end
toc

chg1 = reshape(chg1, [nz, nr] );
flux1 = reshape(flux1, [nz, nr, 3]);


tic
for i = 1:ITE
%flux_weight = repmat(cell_weight,[1 3]).*repmat(vel,[4 1]); 
%%{
chg2 = accumarray(cell_idx,cell_weight,[nz*nr, 1], [], 0);

accum_idx = [cell_idx, cell_idx+nz*nr, cell_idx+2*nz*nr];
flux_weight = horzcat(cell_weight, cell_weight, cell_weight) .* vertcat(vel, vel, vel, vel);%repmat too slow
flux2 = accumarray(accum_idx(:), flux_weight(:), [nz*nr*3, 1], [], 0);

chg2 = reshape(chg2, [nz, nr] );
flux2 = reshape(flux2, [nz, nr, 3]);

%chg1 == chg2
end
toc

