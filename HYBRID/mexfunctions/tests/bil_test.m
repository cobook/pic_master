addpath('../../src');
addpath('../');
load('bil.mat');
[nz, nr, nt] = size(ef_temp);
np = 1500000;
tic
for k = 1:10
out = mex_bil_weight_part_OMP(ef_temp(:), fz0r0(1:1000), fz0r1(1:1000), fz1r0(1:1000), fz1r1(1:1000), i(1:1000)-1, j(1:1000)-1, nz, nr, np);
end
toc