A = rand(1000,3);
B = rand(1000,3);

tic
for i = 1:100
K = cross(A,B);
end
toc


gpuA = gpuArray(A);
gpuB = gpuArray(B);
tic
for i = 1:100
K = cross(gpuA,gpuB);
end
toc
gather(K);


tic
for i = 1:100
L = crossprod(A,B);
end
toc
