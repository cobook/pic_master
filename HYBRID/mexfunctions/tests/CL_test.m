addpath('../../src');
addpath('../');
load('CL.mat');

number = 300;
tic
[mgf, elec, rstyJ, curlBp] = mex_CALC_EF(MU0, B0, B_ext, J_ion, kTe, n_ion, QE, self.dz, self.dr, ef.r_coeff, ef.r_rsty0, ef.r_sigma, ef.n_thre, dt, number, ef.rsty);
toc
tic
[mgf2, elec, rstyJ, curlBp2] = Copy_of_mex_CALC_EF_OMP(MU0, B0, B_ext, J_ion, kTe, n_ion, QE, self.dz, self.dr, ef.r_coeff, ef.r_rsty0, ef.r_sigma, ef.n_thre, dt, number, ef.rsty);
toc

isequal(mgf,mgf2)
isequal(curlBp,curlBp2)