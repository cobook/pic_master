// mex_CALC_EF.c
// Author: Hiroki Fujii, Tokyo Tech, (C) 2016
// Hasegawa laboratory

// Input rhs
// 0. EF
// 1. MF
// 2. pos
// 3. vel
// 4. qm
// 5. dt

// Output lhs
// 0. pos
// 1. vel


#include "math.h"
#include "mex.h"
#include <string.h>
#include "omp.h"

//#define _DEBUG_INFO_ 0
#define SHOWSIZE 10
//#define MAX_SIZE 100000

// Prototype
void show_vector(double*,int,int,int);

// main routine
void child(double* field, double* fz0r0, double* fz0r1, double* fz1r0, double* fz1r1, double* idxz, double* idxr, int np, int nz, int nr, double* FS)
{
    int idx; // index inside for loop
    int i;
    int probe;
    double field_z0r0;
    double field_z0r1;
    double field_z1r0;
    double field_z1r1;
    int nn = nz*nr;
    

#pragma omp parallel for private(idx,probe)
    for (i = 0; i<np; i++) {
        for (int j = 0; j<3; j++) {
                idx = i + np*j;
                probe = (int)(idxz[i] + nz*idxr[i]);
                // Fetch weight on cells
                FS[idx] = field[probe          + nn*j]          * fz0r0[i]
                        + field[probe     + nz + nn*j] * fz0r1[i]
                        + field[probe + 1      + nn*j]      * fz1r0[i]
                        + field[probe + 1 + nz + nn*j] * fz1r1[i];
            
        }
    }
}


void mexFunction(int nlhs,
        mxArray *plhs[], 
        int nrhs, 
        const mxArray *prhs[])
{
    double *field, *fz0r0, *fz0r1, *fz1r0, *fz1r1, *idxz, *idxr;
    // Reject bad input:
    if (nrhs !=10) {
        mexErrMsgIdAndTxt("BadInput",
                "Argument number misunderstood. You need exactly 5 arguments.");
    }

    field       =       mxGetPr(prhs[0]);
    fz0r0       =       mxGetPr(prhs[1]);
    fz0r1       =       mxGetPr(prhs[2]);
    fz1r0       =       mxGetPr(prhs[3]);
    fz1r1       =       mxGetPr(prhs[4]);
    idxz        =       mxGetPr(prhs[5]);
    idxr        =       mxGetPr(prhs[6]);        
    mwSize nz = (mwSize)*mxGetPr(prhs[7]);
    mwSize nr = (mwSize)*mxGetPr(prhs[8]);
    mwSize np = (mwSize)*mxGetPr(prhs[9]);
    mwSize nt = 3;
    mwSize nd = 3;
    
   
     double *FS  = (double*)calloc(np*nd, sizeof(double));
     if (FS == NULL)
     {
        mexPrintf("FS allocation error!\n");
        return;
     }


 //        memcpy(field, mxGetPr(prhs[0]), nz*nr*nt*sizeof(double));
//        memcpy(fz0r0, mxGetPr(prhs[1]), np*sizeof(double));
//        memcpy(fz0r1, mxGetPr(prhs[2]), np*sizeof(double));
//        memcpy(fz1r0, mxGetPr(prhs[3]), np*sizeof(double));
//        memcpy(fz1r1, mxGetPr(prhs[4]), np*sizeof(double));
//        memcpy(idxz, mxGetPr(prhs[5]), np*sizeof(double));
//        memcpy(idxr, mxGetPr(prhs[6]), np*sizeof(double));
        
    
    
//========================STORE VALUE========================
    double *out0;  // output pointer
    plhs[0] = mxCreateDoubleMatrix(np, nd, mxREAL);
    out0 = mxGetPr(plhs[0]);
    // call routine
    child(field, fz0r0, fz0r1, fz1r0, fz1r1, idxz, idxr, (int)np, (int)nz, (int)nr, FS);
    memcpy(out0, FS, np*nd * sizeof(double));
//========================END MAIN FUNCTION========================
    free(FS);
    return;
}

void show_vector(double *a, int nz, int nr, int nd) {
    int i,j,k;
    
    for (k=0;k<nd;k++){
        mexPrintf("Dim=%d\n",k+1);
//        for (i=0;i<nz;i++){
        for (i=0;i<SHOWSIZE;i++){
//            for (j=0;j<nr;j++){
            for (j=0;j<SHOWSIZE;j++){
                mexPrintf("%g ",a[i+nz*j+nz*nr*k]);
            }
            mexPrintf("\n");
        }
        mexPrintf("\n\n");
    }
}
