// mex_dep2nodes.c
// Author: Hiroki Fujii, Tokyo Tech, (C) 2016
// Hasegawa laboratory

// Input rhs
// 0. pos
// 1. dz
// 2. dr

// Output lhs
// 0. fi
// 1. i
// 2. hz
// 3. fj
// 4. j
// 5. hr
// 6. wfr
// 7. whr

#include "math.h"
#include "mex.h"
#include "string.h"
#include <sys/time.h>
#include "omp.h"

//#define _DEBUG_INFO_    1
#define SHOWSIZE    3
// #define MAX_SIZE 100000


// Prototype
void show_vector(double*,int,int,int);


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    double *out0, *out1, *out2, *out3, *out4, *out5, *out6, *out7;
    double *pos;
    const int *spart;
    const double inidx = 1.5; // initial indexing position for electric field
    double dz, dr;
    struct timeval tstart, tend;
    long diff;
    
    // Reject bad input:
    if (nrhs !=3) {
        mexErrMsgIdAndTxt("BadInput",
                "Argument number misunderstood. You need exactly 7 arguments.");
    }
    
    // Process Input:
    pos         =       (double*)mxGetData(prhs[0]); // position
    spart       =       mxGetDimensions(prhs[0]);
    int np=*spart, nd=*(spart+1);
    dz          =       mxGetScalar(prhs[1]); 
    dr          =       mxGetScalar(prhs[2]);
    
#ifdef _DEBUG_INFO_
    mexPrintf("========================================== \n");
    mexPrintf("np = %d, nd =%d \n", np, nd);
    show_vector(pos, np, nd, 1);
#endif
    
// Initialize parameters

    
    double*  fi = (double*)mxCalloc(np, sizeof(double));
    double*   i = (double*)mxCalloc(np, sizeof(double));
    double*  hz = (double*)mxCalloc(np, sizeof(double));
    double*  fj = (double*)mxCalloc(np, sizeof(double));
    double*   j = (double*)mxCalloc(np, sizeof(double));
    double*  hr = (double*)mxCalloc(np, sizeof(double));
    double* whr = (double*)mxCalloc(np, sizeof(double));
    double* wfr = (double*)mxCalloc(np, sizeof(double));
    
    if (fi==NULL || i==NULL || hz==NULL || fj==NULL || j==NULL || hr==NULL || whr==NULL || wfr==NULL)
    { 
        mexPrintf("Memory not allocated properly");
        mxFree(fi);
        mxFree(i);
        mxFree(hz);
        mxFree(fj);
        mxFree(j);
        mxFree(hr);
        mxFree(whr);
        mxFree(wfr);
        return;
    }
//     
//     
//     memset(&fi[0],  0x00, sizeof(fi));
//     memset(&i[0],   0x00, sizeof(i));
//     memset(&hz[0],  0x00, sizeof(hz));
//     memset(&fj[0],  0x00, sizeof(fj));
//     memset(&j[0],   0x00, sizeof(j));
//     memset(&hr[0],  0x00, sizeof(hr));
//     memset(&whr[0], 0x00, sizeof(whr));
//     memset(&wfr[0], 0x00, sizeof(wfr));
//     
    
// ====================deposition2nodes=====================

    gettimeofday(&tstart, NULL);
  #pragma omp parallel for
  for (int k=0; k<np; k++)
    {
        fi[k] = inidx + pos[k   ]/dz;
        fj[k] = inidx + pos[k+np]/dr;
        i[k] = floor(fi[k]);
        hz[k] = fi[k] - i[k];
        j[k] = floor(fj[k]);
        hr[k] = fj[k] - j[k];
        
        whr[k] = fj[k] - 2.0;
        wfr[k] = floor(whr[k]);
 
    }
    gettimeofday(&tend, NULL);
    diff = tend.tv_usec - tstart.tv_usec;
    mexPrintf("It took %ld second(us).\n", diff);
    
 
//========================STORE VALUE========================
    plhs[0] = mxCreateDoubleMatrix(np, 1, mxREAL);
    out0 = mxGetPr(plhs[0]);
    memcpy(out0, fi, np * sizeof(double));

    plhs[1] = mxCreateDoubleMatrix(np, 1, mxREAL);
    out1 = mxGetPr(plhs[1]);
    memcpy(out1, i, np * sizeof(double));
 
    plhs[2] = mxCreateDoubleMatrix(np, 1, mxREAL);
    out2 = mxGetPr(plhs[2]);
    memcpy(out2, hz, np * sizeof(double));
    
    plhs[3] = mxCreateDoubleMatrix(np, 1, mxREAL);
    out3 = mxGetPr(plhs[3]);
    memcpy(out3, fj, np * sizeof(double));
    
    plhs[4] = mxCreateDoubleMatrix(np, 1, mxREAL);
    out4 = mxGetPr(plhs[4]);
    memcpy(out4, j, np * sizeof(double));

    plhs[5] = mxCreateDoubleMatrix(np, 1, mxREAL);
    out5 = mxGetPr(plhs[5]);
    memcpy(out5, hr, np * sizeof(double));
 
    plhs[6] = mxCreateDoubleMatrix(np, 1, mxREAL);
    out6 = mxGetPr(plhs[6]);
    memcpy(out6, wfr, np * sizeof(double));
    
    plhs[7] = mxCreateDoubleMatrix(np, 1, mxREAL);
    out7 = mxGetPr(plhs[7]);
    memcpy(out7, whr, np * sizeof(double));
    
//========================END MAIN FUNCTION========================
           
    return;
}


void show_vector(double *a, int nz, int nr, int nd) {
    int i,j,k;
    
    for (k=0;k<nd;k++){
        mexPrintf("Dim=%d\n",k+1);
//        for (i=0;i<nz;i++){
        for (i=0;i<SHOWSIZE;i++){
//            for (j=0;j<nr;j++){
            for (j=0;j<SHOWSIZE;j++){
                mexPrintf("%g ",a[i+nz*j+nz*nr*k]);
            }
            mexPrintf("\n");
        }
        mexPrintf("\n\n");
    }
}
