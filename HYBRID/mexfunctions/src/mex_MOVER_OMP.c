// mex_CALC_EF.c
// Author: Hiroki Fujii, Tokyo Tech, (C) 2016
// Hasegawa laboratory

// Input rhs
// 0. EF
// 1. MF
// 2. pos
// 3. vel
// 4. qm
// 5. dt
// 6. np

// Output lhs
// 0. pos
// 1. vel


#include "math.h"
#include "mex.h"
#include "string.h"
#include "omp.h"

//#define _DEBUG_INFO_ 0
#define SHOWSIZE 10
//#define MAX_SIZE 100000

// Prototype
void show_vector(double*,int,int,int);
void move_all(double*, double*, double*, double*, double, double, int, int);


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    double *EF, *MF, *pos, *vel;
    double qm, dt;
    int np;
    const mwSize *asize;
    // Reject bad input:
    if (nrhs !=7) {
        mexErrMsgIdAndTxt("BadInput",
                "Argument number misunderstood. You need exactly 7 arguments.");
    }
    
    // Process Input:
    EF          =       (double*)mxGetData(prhs[0]); 
    MF          =       (double*)mxGetData(prhs[1]);
    pos         =       (double*)mxGetData(prhs[2]);
    vel         =       (double*)mxGetData(prhs[3]);
    qm          =       mxGetScalar(prhs[4]);
    dt          =       mxGetScalar(prhs[5]);
    np          =       mxGetScalar(prhs[6]);

    asize       =       mxGetDimensions(prhs[2]);
    int M = mxGetM(prhs[2]);  // Number of maximum array size
    mwSize M3 = M*3;
    
#ifdef _DEBUG_INFO_
//    mexPrintf("mz0 = %d\n", mz0);
#endif

    move_all(pos, vel, EF, MF, qm, dt, np, M);

//========================STORE VALUE========================
    double *out0, *out1;
    //const mwSize ndims[3] = {np, nd};
    plhs[0] = mxCreateNumericArray(2, asize, mxDOUBLE_CLASS,  mxREAL);
    out0 = mxGetPr(plhs[0]);
    memcpy(out0, pos, M3 * sizeof(double));
    plhs[1] = mxCreateNumericArray(2, asize, mxDOUBLE_CLASS,  mxREAL);
    out1 = mxGetPr(plhs[1]);
    memcpy(out1, vel, M3 * sizeof(double));
//========================END MAIN FUNCTION========================
    return;
}

void move_all(
        double *pos,
        double *vel,
        double *EF,
        double *MF, 
        double qm, 
        double dt, 
        int np,
        int M              
        )
{
    double qmdt = qm*dt;
    double qmdt2 = qm*dt/2; // Divide by 2 due, half acceleration
    
#pragma omp parallel for
    for (int i = 0; i<np; i++) {
        double a[3];
        double velt[3];
        double r = pos[M+i];
        if (r==0) r = 1;
        double dtr = dt/r;
        a[0] = qmdt2*EF[np*0+i];
        a[1] = qmdt2*EF[np*1+i];
        a[2] = qmdt2*EF[np*2+i];
        
        // Add half accceleration
        velt[0] = vel[    i] + a[0];
        velt[1] = vel[M*1+i] + a[1];
        velt[2] = vel[M*2+i] + a[2];
        
        // centrifugal force term & rotation by magnetic field
        vel[     i] = velt[0]
                + qmdt * (velt[1]*MF[i + 2*np] - velt[2]*MF[i + np]);
        vel[M*1+i] = velt[1] 
                + qmdt * (velt[2]*MF[i]        - velt[0]*MF[i + 2*np])
        + velt[2]*velt[2]*dtr;
        vel[M*2+i] = velt[2] + qmdt*(velt[0]*MF[i + np]   - velt[1]*MF[i])
        - velt[1]*velt[2]*dtr;
        
        vel[    i] += a[0];
        vel[M*1+i] += a[1];
        vel[M*2+i] += a[2];
        
        // Advance particels
        pos[    i] += vel[    i]*dt;
        pos[M*1+i]  += vel[M*1+i]*dt;
    }
//    }
}



void show_vector(double *a, int nz, int nr, int nd) {
    int i,j,k;
    
    for (k=0;k<nd;k++){
        mexPrintf("Dim=%d\n",k+1);
//        for (i=0;i<nz;i++){
        for (i=0;i<SHOWSIZE;i++){
//            for (j=0;j<nr;j++){
            for (j=0;j<SHOWSIZE;j++){
                mexPrintf("%g ",a[i+nz*j+nz*nr*k]);
            }
            mexPrintf("\n");
        }
        mexPrintf("\n\n");
    }
}
