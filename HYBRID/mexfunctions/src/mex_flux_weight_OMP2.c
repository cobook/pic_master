// mex_flux_weight.c
// Author: Hiroki Fujii, Tokyo Tech, (C) 2016
// Hasegawa laboratory

// Input rhs
// 0. index of cell number
// 1. weight on each cell
// 2. particle velocity
// 3. nz
// 4. nr
// 5. np

// Output lhs
// 0. chg
// 1. flux

#include "mex.h"
#include "string.h"
#include "omp.h"

//#define _DEBUG_INFO_ 1
//#define MAX_SIZE 100000

mwSize accumarray(double*, double*, double*, int);
void weight_child(int,double*,double*,double*,double*,double*);
void GenerateIdxWeight(double*,double*,double*,double*,double*,double*,double*,double*,int,int,int,double*,double*,int);


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    mwSize i, j, k, nz, nr;
    mwSize c, d1, d2, d3, d;
    double *a, *cell_weight, aa, *m, *cell_idx, *f_idx,
            *chg, *p, *flux, *q, *out0, *out1, *out2, *out3, *out4;
    double *vel_t, *vel;
    double *lb, *rb, *lt, *rt;
    double *f1, *f2, *f3, *fz, *fr, *ft;
    double *part_i, *part_j;
    int idx, nn, np;
    int dim = 3;
    double *fwz, *fwr, *fwt;
    
    // Reject bad input:
    if (nrhs !=10) {
        mexErrMsgIdAndTxt("BadInput",
                "Argument number misunderstood. You need exactly 10 arguments.");
    }
    // 0. index part.i
    if ( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 1) {
        mexErrMsgIdAndTxt("BadInput:0",
                "flux_weight.mex: Inputs must be a double matrix.");
    }
    // 1. index part.j
    if ( !mxIsDouble(prhs[1]) || mxGetN(prhs[1]) != 1) {
        mexErrMsgIdAndTxt("BadInput:1",
                "flux_weight.mex: Inputs must be a double matrix.");
    }
    // 2. velocity
    if ( !mxIsDouble(prhs[2]) || mxGetN(prhs[2]) != 3) {
        mexErrMsgIdAndTxt("BadInput:2",
                "flux_weight.mex: Inputs must be a double matrix.");
    }
    // 3. nz
    // 4. nr
    // 5. np
    if ( !mxIsDouble(prhs[3]) || mxIsComplex(prhs[3]) || mxGetNumberOfElements(prhs[3]) != 1 ||
            !mxIsDouble(prhs[4]) || mxIsComplex(prhs[4]) ||mxGetNumberOfElements(prhs[4]) != 1 ||
            !mxIsDouble(prhs[5]) || mxIsComplex(prhs[5]) || mxGetNumberOfElements(prhs[5]) != 1  ) {
        mexErrMsgIdAndTxt("BadInput:arrayProduct:notScalar",
                "Input multiplier must be a scalar.");
    }
    // 6. lb
    // 7. rb
    // 8. lt
    // 9. rt
    
    // Process Input:
    part_i         =          mxGetPr(prhs[0]); // index for accumulations
    part_j         =          mxGetPr(prhs[1]); // Weight on each cell
    vel_t         =          mxGetPr(prhs[2]); // Particle velocity
    nz          = (mwSize)*mxGetPr(prhs[3]);  // The size of input matrix
    nr          = (mwSize)*mxGetPr(prhs[4]);  // The size of input matrix
    np          = (mwSize)*mxGetPr(prhs[5]);  // The number of particles
    nn          = nz * nr;
    lb          =          mxGetPr(prhs[6]);
    rb          =          mxGetPr(prhs[7]);
    lt          =          mxGetPr(prhs[8]);
    rt          =          mxGetPr(prhs[9]);
    int M = mxGetM(prhs[2]);
  
    // accumuration matirces
    cell_idx = (double*)calloc(4*np, sizeof(double));
    cell_weight = (double*)calloc(4*np, sizeof(double));
    vel = (double*)calloc(3*np, sizeof(double));    
    
    GenerateIdxWeight(cell_idx, cell_weight, part_i, part_j, lb, rb, lt, rt, nz, nr, np, vel, vel_t, M);
    
#ifdef _DEBUG_INFO_
    mexPrintf("nz = %d\n", nz);
    mexPrintf("nr = %d\n", nr);
    mexPrintf("np = %d\n", np);
    mexPrintf("nn = %d\n", nn);
    
    show_vector(cell_idx, 4*np);
    show_vector(cell_weight, 4*np);
    show_vector(vel, np*3);
#endif
    // Early return for empty input:
    if (np == 0) {
        plhs[0] = mxCreateDoubleMatrix(0, 2, mxREAL);
        return;
    }

    
    // Collect values:
    chg   = (double*)calloc(nn, sizeof(double));    
    p = chg;

     // Create output:
    c  = accumarray( p, cell_idx, cell_weight, 4*np);
#ifdef _DEBUG_INFO_
    mexPrintf("line3 \n");
    mexPrintf("Size of p = %d\n", sizeof(p));
#endif
    plhs[0] = mxCreateDoubleMatrix(nn, 1, mxREAL);
    out0     = mxGetPr(plhs[0]);
    memcpy(out0,     chg, c * sizeof(double));
//    memcpy(out + c, q0, c * sizeof(double));
    free(chg);
    chg = NULL;

    // Create output for current flux

    fwz   = calloc(4 * np, sizeof(double));
    fwr   = calloc(4 * np, sizeof(double));
    fwt   = calloc(4 * np, sizeof(double));
    
    weight_child(np, fwz, fwr, fwt, cell_weight, vel);
       
    fz    = calloc(nn, sizeof(double));
    fr    = calloc(nn, sizeof(double));
    ft    = calloc(nn, sizeof(double));    
    f1 = fz;
    f2 = fr;
    f3 = ft;
    
    d1 = accumarray(f1, cell_idx, fwz, 4*np);
    d2 = accumarray(f2, cell_idx, fwr, 4*np);
    d3 = accumarray(f3, cell_idx, fwt, 4*np);

    
    plhs[1] = mxCreateDoubleMatrix(nn, 3, mxREAL);
    out1    = mxGetPr(plhs[1]);

    for (j = 0; j < nn; j++) {
        out1[j       ] = f1[j];
        out1[j + 1*nn] = f2[j];
        out1[j + 2*nn] = f3[j];
    }
    
#ifdef _DEBUG_INFO_
    plhs[2] = mxCreateDoubleMatrix(nn, 1, mxREAL);
    out2    = mxGetPr(plhs[2]);
    plhs[3] = mxCreateDoubleMatrix(nn, 1, mxREAL);
    out3    = mxGetPr(plhs[3]);
    plhs[4] = mxCreateDoubleMatrix(nn, 1, mxREAL);
    out4    = mxGetPr(plhs[4]);
    memcpy((out2), f1, d1 * sizeof(double));
    memcpy((out3), f2, d2 * sizeof(double));
    memcpy((out4), f3, d3 * sizeof(double));
#endif

    // Free temporary memory:    
    free(fz);
    free(fr);
    free(ft);
    free(fwz);
    free(fwr);
    free(fwt);
    free(cell_idx);
    free(cell_weight);
    free(vel);
        
    return;
}

void GenerateIdxWeight(double* cell_idx,
        double* cell_weight,
        double* idx,
        double* jdx,
        double* lb,
        double* rb,
        double* lt,
        double* rt,
        int nz,
        int nr,
        int np,
        double* vel,
        double* vel_t,
        int M)
{
#pragma omp parallel for
    for(int i=0; i<np; i++){

        cell_idx[i       ] = (idx[i]  )+  nz*(jdx[i]-1);
        cell_idx[i +   np] = (idx[i]+1)+  nz*(jdx[i]-1);
        cell_idx[i + 2*np] = (idx[i]  )+  nz*(jdx[i]  );
        cell_idx[i + 3*np] = (idx[i]+1)+  nz*(jdx[i]  );
        
        cell_weight[i       ] = lb[i];
        cell_weight[i +   np] = rb[i];
        cell_weight[i + 2*np] = lt[i];
        cell_weight[i + 3*np] = rt[i];
        
        vel[i     ] = vel_t[i    ];
        vel[i+  np] = vel_t[i+  M];
        vel[i+2*np] = vel_t[i+2*M];
    }
}


mwSize accumarray(double *element, double *index, double *value, int n){
    mwSize nrows, idx;
    int i;
    nrows = (mwSize)index[0] - 1;
#ifdef _DEBUG_INFO_
    mexPrintf("Accumarray function start\n");
    show_vector(element);
#endif    
    // Loop over elements:
    for (i = 0; i < n; i++) {
        idx = (int)(index[i]+0.5) - 1;
        element[idx] += value[i];
        if (idx + 1 > nrows) {    // Repeated element in column 1
            nrows = idx + 1;
        }
#ifdef _DEBUG_INFO_
//        mexPrintf("idx = %d \n", idx);
//        mexPrintf("nrows = %f \n", nrows);
//        mexPrintf("value[i] = %f \n", value[i]);
#endif
    }
#ifdef _DEBUG_INFO_
    mexPrintf("Accumarray function ends\n");    
    show_vector(element);
#endif
    return nrows;
}

void weight_child(int np, double* fwz, double* fwr, double* fwt, double* cell_weight, double* vel)
{
    #pragma omp parallel for 
    for (int j = 0; j < np; j++) {
        for (int k = 0; k < 4; k++) {
            fwz[j+k*np] = cell_weight[j+k*np]*vel[j     ];
            fwr[j+k*np] = cell_weight[j+k*np]*vel[j+  np];
            fwt[j+k*np] = cell_weight[j+k*np]*vel[j+2*np];
        }
    }
}

void show_vector(double *a, int n) {
//    int n;
    //n = sizeof(a);
    int i;
    
    for (i=0;i<n;i++){
        mexPrintf("%f \n",a[i]);
    }
    mexPrintf("\n\n");
}
