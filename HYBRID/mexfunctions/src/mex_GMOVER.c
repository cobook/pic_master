// mex_CALC_EF.c
// Author: Hiroki Fujii, Tokyo Tech, (C) 2016
// Hasegawa laboratory

// Input rhs
// 0. EF
// 1. MF
// 2. pos
// 3. vel
// 4. qm
// 5. dt

// Output lhs
// 0. pos
// 1. vel


#include "math.h"
#include "mex.h"
#include "string.h"
#include "omp.h"

//#define _DEBUG_INFO_ 0
#define SHOWSIZE 10
//#define MAX_SIZE 100000

// Prototype
void show_vector(double*,int,int,int);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    double *EF, *MF, *pos, *vel;
    double qm, dt;
    int iter;
    const mwSize *asize;
    // Reject bad input:
    if (nrhs !=7) {
        mexErrMsgIdAndTxt("BadInput",
                "Argument number misunderstood. You need exactly 6 arguments.");
    }
    
    // Process Input:
    EF          =       (double*)mxGetData(prhs[0]); 
    MF          =       (double*)mxGetData(prhs[1]);
    pos         =       (double*)mxGetData(prhs[2]);
    vel         =       (double*)mxGetData(prhs[3]);
    qm          =       mxGetScalar(prhs[4]);
    dt          =       mxGetScalar(prhs[5]);
    iter        =       (int)mxGetScalar(prhs[6]);

    asize       =       mxGetDimensions(prhs[0]);
    mwSize np=*asize, nd=*(asize+1);
    mwSize nn = np*nd;
    
#ifdef _DEBUG_INFO_
//    mexPrintf("mz0 = %d\n", mz0);
#endif
// Initialize parameters
//    double curlBp[mz0*mr0*md0];
//    memset(&curlBp[0], 0x00, sizeof(curlBp));
    double a[3];
    double velt[3];
    double r; // radius
    double dtr;
    double qmdt = qm*dt;
    double qmdt2 = qm*dt/2;
//#pragma omp parallel num_threads(12) 
//{
    int i;
//#pragma omp for schedule(static) private(a,velt)
    
    for (int k = 0; k<iter; k++){
    for (i = 0; i<np; i++) {
        r = pos[np+i];
        if (r==0) r = 1;
        dtr = dt/r;
        a[0] = qmdt2*EF[np*0+i];
        a[1] = qmdt2*EF[np*1+i];
        a[2] = qmdt2*EF[np*2+i];
        
        // Add half accceleration
        velt[0] = vel[     i] + a[0];
        velt[1] = vel[np*1+i] + a[1];
        velt[2] = vel[np*2+i] + a[2];        
        
        // centrifugal force term & rotation by magnetic field
        vel[     i] = velt[0] + qmdt * (velt[1]*MF[i + 2*np] - velt[2]*MF[i + np]);
        vel[np*1+i] = velt[1] + qmdt * (velt[2]*MF[i]        - velt[0]*MF[i + 2*np])
                + velt[2]*velt[2]*dtr;
        vel[np*2+i] = velt[2] + qmdt*(velt[0]*MF[i + np]   - velt[1]*MF[i])
                - velt[1]*velt[2]*dtr;
        
        vel[     i] += a[0];
        vel[np*1+i] += a[1];
        vel[np*2+i] += a[2];        
       
        // Advance particels
        pos[     i] += vel[     i]*dt;
        pos[np*1+i] += vel[np*1+i]*dt;  
    }
    }
//    }
    


//========================STORE VALUE========================
    double *out0, *out1;
    //const mwSize ndims[3] = {np, nd};
    plhs[0] = mxCreateNumericArray(2, asize, mxDOUBLE_CLASS,  mxREAL);
    out0 = mxGetPr(plhs[0]);
    memcpy(out0, pos, nn * sizeof(double));
    plhs[1] = mxCreateNumericArray(2, asize, mxDOUBLE_CLASS,  mxREAL);
    out1 = mxGetPr(plhs[1]);
    memcpy(out1, vel, nn * sizeof(double));
//========================END MAIN FUNCTION========================
    return;
}

void crossprod(double *C, double *A, double *B, int np){
    int i;

    // Major change - Using column major indexing
    for(i = 0; i < np; i++) {
        C[i] = A[i + np]*B[i + 2*np] - A[i + 2*np]*B[i + np];
        C[i + np] = A[i + 2*np]*B[i] - A[i]*B[i + 2*np]; // Change here too
        C[i + 2*np] = A[i]*B[i + np] - A[i + np]*B[i];
    }
}


// Hall term calculation
void hall_term(double* ht, double* Ji, double* ni, double QE, double* curlBp, double* Bint, double mu, int mz0, int mr0, int md0){
    int i, j, k;
    int kp, km;
    int mm0 = mz0*mr0;
    double mq = 1/QE;
    double mmu = 1/mu;
        
    for (k=0;k<md0;k++){
         kp = (k+4)%md0;
         km = (k+2)%md0;
         for (i=1;i<mz0-1;i++){
            for (j=1;j<mr0-1;j++){
               // ht = 1/QE.*cross(Je, Bint)/ni
                ht[i+j*mz0+mm0*k] = mq / ni[i+j*mz0] 
                    * ((curlBp[i+j*mz0+mm0*kp]*mmu - Ji[i+j*mz0+mm0*kp]) * Bint[i+j*mz0+mm0*km]
                    -  (curlBp[i+j*mz0+mm0*km]*mmu - Ji[i+j*mz0+mm0*km]) * Bint[i+j*mz0+mm0*kp]);
            }
        }
    }
}

// Pressure term calculation
void pressure_term(double* pt, double* ni, double kTe, double QE, double* nablan, int mz0, int mr0, int md0 ){
    int i, j, k;
    int mm0 = mz0*mr0;
    double mqT = - 1/QE*kTe;
        
    for (k=0;k<md0-1;k++){
        for (i=1;i<mz0-1;i++){
            for (j=1;j<mr0-1;j++){
                pt[i+j*mz0+mm0*k] = mqT * nablan[i+j*mz0+mm0*k] / ni[i+j*mz0];
            }
        }
    }
}

// Resistivity and resistivity*J 
void set_rsty(double* rstyJ, double* rsty, double mu, double* curlBp, double* ni, double coeff, double rsty0, double sigma, double n_thre, int mz0, int mr0, int md0) {
    int i, j, k;
    int mm0 = mz0*mr0;
    double mmu = 1/mu;

    for (i=1;i<mz0-1;i++){
        for (j=1;j<mr0-1;j++){
            rsty[i + j*mz0] = 0.5*1e-12*(coeff/ni[i+j*mz0]-rsty0)*(1-tanh((ni[i+j*mz0]-n_thre)/sigma)) + rsty0;
            for (k=0;k<md0;k++){
                rstyJ[i+j*mz0+mm0*k] = mmu*curlBp[i+j*mz0+mm0*k]*rsty[i+j*mz0];
            }
        }
    }
#ifdef _DEBUG_INFO_
    mexPrintf("========================================== \n");
    mexPrintf("rstyJ :\n");
    show_vector(rstyJ, mz0,mr0,md0);
#endif
    return;
}

/**

void input_process(double* curlBp, double* Bint, double* nablan, 
        double *B, double *Bext, double *ni,
        double dz,double dr,int mz0,int mr0,int md0, int nz0, int nr0, int nd0){

    int i, j, k;
    int nn0=nz0*nr0;
    int nz2=nz0+2;
    int nr2=nr0+2;
    int nn2=nz2*nr2;
    int zidx, ridx;
    int id, jd;
    double Bp[(nz2)*(nr2)*(nd0)];

    // Calculate Bp=B-Bext
    // SIZE(Bp) : nz0+2, nr0+2, 3
    for (k=0;k<nd0;k++){
        for (i=0;i<nz2;i++){
            for (j=1;j<nr2;j++){
                zidx = i-1;
                ridx = j-1;  
                if (zidx==nz0)
                {
                    zidx = nz0-1;
                }
                else if (zidx==-1)
                {
                    zidx = 0;
                }
                
                if (ridx==nr0){
                    ridx = nr0-1;
                }
                Bp[i+nz2*j+nn2*k] = B[zidx+nz0*ridx+nn0*k] - Bext[zidx+nz0*ridx+nn0*k];
            }
            // Boundary on the axis
            if (k==0) Bp[i+nz2*0+nz2*nr2*0] =   Bp[i+nz2*2+nz2*nr2*0];
            else      Bp[i+nz2*0+nz2*nr2*k] = - Bp[i+nz2*2+nz2*nr2*k];
        }
    }

#ifdef _DEBUG_INFO_
    mexPrintf("========================================== \n");
    mexPrintf("Bp :\n");
    show_vector(Bp, nz2,nr2,nd0);
#endif

    // Calculate interpolated B
    int i0, j0, i1, j1;
    int mm0 = mz0*mr0;
    for (k=0;k<md0;k++){
        for (i=0;i<mz0;i++){
            for (j=1;j<mr0;j++){
                i0 = i;
                j0 = j;
                i1 = i-1;
                j1 = j-1;
                if (i==0)
                {
                    i1 = 0;
                }
                else if (i==mz0-1)
                {
                    i0 = mz0-2;
                }
                if (j==0)
                {
                    j1=0;
                }
                if (j==mr0-1)
                {
                    j0 = mr0-2;
                }

                //Interpolation
                Bint[i + mz0*j + mm0*k] = 0.25 * (B[i0 + j0*nz0 + nn0*k]
                                               + B[i1 + j0*nz0 + nn0*k]
                                               + B[i0 + j1*nz0 + nn0*k]
                                               + B[i1 + j1*nz0 + nn0*k]);
            }
        }
    }
#ifdef _DEBUG_INFO_
    mexPrintf("========================================== \n");
    mexPrintf("Bint :\n");
    show_vector(Bint, mz0,mr0,md0);
#endif

    // Calculate rotation Bp
    rotate_B(curlBp, Bp, dz, dr, mz0, mr0, md0);
    
    // Calculate gradient ni
    gradient_ni(nablan, ni, dz, dr, mz0, mr0, md0);
    
    return;
}

// Function for the Calculation of gradient ni
// Simple stencil calculation
void gradient_ni(double *nablan, double *ni, double dz, double dr, int mz0, int mr0, int md0) {
    int i,j;
    int ip, im, jp, jm;
    int mm0 = mz0*mr0;
    double a;
    double dr2 = 0.5/dr;
    double dz2 = 0.5/dz;
    for (i=0;i<mz0;i++){
        for (j=1;j<mr0;j++){
           
            // In z
            a = 1;
            ip = i+1;
            im = i-1;
            if (ip==mz0){
                ip = mz0-1;
                a = 2;
            }
            if (im==-1){
                im = 0;
                a = 2;
            }

            nablan[i+j*mz0+mm0*0] = a * dz2 *(ni[ip+j*mz0] - ni[im+j*mz0]) ;
            // In r
            a = 1;
            jp = j+1;
            jm = j-1;
            if (jp==mr0){
                jp = mr0-1;
                a = 2;
            }
            if ( j==1 ) {
                a = 2;
                nablan[i+j*mz0+mm0*1] = a * dr2 *(ni[i+jp*mz0] - ni[i+j*mz0]) ;
            } else {
                nablan[i+j*mz0+mm0*1] = a * dr2 *(ni[i+jp*mz0] - ni[i+jm*mz0]) ;
            }
        }
    }

#ifdef _DEBUG_INFO_
//    mexPrintf("========================================== \n");
//    mexPrintf("nablan :\n");
//    show_vector(nablan, mz0,mr0,md0);
#endif
 
    return;
}

// Function for the Calculation of rotation E
void rotate_E(double *curlE, double *E, double dz, double dr, int nz0, int nr0, int nd0) {
    int i0, j0, i1, j1;
    int i, j;
    int nn0 = nz0*nr0;
    int nz1 = nz0+1;
    int nr1 = nr0+1;
    int nn1 = nz1*nr1;
    double ddr = 0.5/dr;
    double ddz = 0.5/dz;
    double dArdz, dAzdr, rj, rj0, rj1;
    
    for (i=0;i<nz0;i++){
        i0 = i;
        i1 = i+1;
        for (j=1;j<nr0;j++){
            j0 = j;
            j1 = j+1;

            rj  = (double)j;
            rj0 = (double)j0-0.5;
            rj1 = (double)j1-0.5;

            // Z direction (k=0)
            curlE[i+j*nz0+nn0*0] = ddr / rj * (
                  E[i1+j1*nz1+nn1*2]*rj1 
                - E[i1+j0*nz1+nn1*2]*rj0
                + E[i0+j1*nz1+nn1*2]*rj1 
                - E[i0+j0*nz1+nn1*2]*rj0
             );
            
            // R direction (k=1)
            curlE[i+j*nz0+nn0*1] = - ddz * (
              E[i1+j1*nz1+nn1*2]
            - E[i0+j1*nz1+nn1*2]
            + E[i1+j0*nz1+nn1*2] 
            - E[i0+j0*nz1+nn1*2]
             );
            
            // Theta direction (k=2)
            dArdz = ddz * (
                  E[i1+j1*nz1+nn1*1] 
                - E[i0+j1*nz1+nn1*1]
                + E[i1+j0*nz1+nn1*1] 
                - E[i0+j0*nz1+nn1*1]
                );
            dAzdr = ddr * (
                  E[i1+j1*nz1+nn1*0] 
                - E[i1+j0*nz1+nn1*0]
                + E[i0+j1*nz1+nn1*0]
                - E[i0+j0*nz1+nn1*0]
                    );
             curlE[i+j*nz0+nn0*2] = dArdz - dAzdr;
        }
       // Boundary conditions on the axis
//        curlE[i + nn0*0] = 4/dr * (E[i0+1*nz0+nn0*2] + E[i1+1*nz0+nn0*2])/2;
//        the equation above is too sensitive and doesn't work properly, 
//        Let's just use the same value
        curlE[i + nn0*0] = curlE[i + 1*nz0 + nn0*0];
        curlE[i + nn0*1] = 0;
        curlE[i + nn0*2] = 0;
    }

#ifdef _DEBUG_INFO_
//    mexPrintf("========================================== \n");
//    mexPrintf("curlE :\n");
//    show_vector(curlE, nz0,nr0,nd0);
#endif

    return;
}




// Function for the Calculation of rotation Bp
void rotate_B(double *curlBp, double *Bp, double dz, double dr, int mz0, int mr0, int md0) {
    int i0, j0, i1, j1;
    int i, j;
    int mm0 = mz0*mr0;
    int mz1 = mz0+1;
    int mr1 = mr0+1;
    int mm1 = mz1*mr1;
    double ddr = 0.5/dr;
    double ddz = 0.5/dz;
    double dArdz, dAzdr, rj, rj0, rj1;
    
    for (i=1;i<mz0-1;i++){
        for (j=1;j<mr0-1;j++){
            i0 = i;
            j0 = j;
            i1 = i+1;
            j1 = j+1;

            rj  = (double)j-0.5;
            rj0 = (double)j0-1;
            rj1 = (double)j1-1;

            // Z direction (k=0)
            curlBp[i+j*mz0+mm0*0] = ddr / rj * (
                  Bp[i1+j1*mz1+mm1*2]*rj1 
                - Bp[i1+j0*mz1+mm1*2]*rj0
                + Bp[i0+j1*mz1+mm1*2]*rj1 
                - Bp[i0+j0*mz1+mm1*2]*rj0
             );
            
            // R direction (k=1)
            curlBp[i+j*mz0+mm0*1] = - ddz * (
              Bp[i1+j1*mz1+mm1*2]
            - Bp[i0+j1*mz1+mm1*2]
            + Bp[i1+j0*mz1+mm1*2] 
            - Bp[i0+j0*mz1+mm1*2]
             );
            
            // Theta direction (k=2)
            dArdz = ddz * (
                  Bp[i1+j1*mz1+mm1*1] 
                - Bp[i0+j1*mz1+mm1*1]
                + Bp[i1+j0*mz1+mm1*1] 
                - Bp[i0+j0*mz1+mm1*1]
                );
            dAzdr = ddr * (
                  Bp[i1+j1*mz1+mm1*0] 
                - Bp[i1+j0*mz1+mm1*0]
                + Bp[i0+j1*mz1+mm1*0]
                - Bp[i0+j0*mz1+mm1*0]
                    );
             curlBp[i+j*mz0+mm0*2] = dArdz - dAzdr;
        }
    }

    // Boundary conditions
    copy_cells(curlBp, mz0, mr0, md0, mm0);

#ifdef _DEBUG_INFO_
    mexPrintf("========================================== \n");
    mexPrintf("curlBp :\n");
    show_vector(curlBp, mz0,mr0,md0);
#endif

    return;
}

 ***/

void show_vector(double *a, int nz, int nr, int nd) {
    int i,j,k;
    
    for (k=0;k<nd;k++){
        mexPrintf("Dim=%d\n",k+1);
//        for (i=0;i<nz;i++){
        for (i=0;i<SHOWSIZE;i++){
//            for (j=0;j<nr;j++){
            for (j=0;j<SHOWSIZE;j++){
                mexPrintf("%g ",a[i+nz*j+nz*nr*k]);
            }
            mexPrintf("\n");
        }
        mexPrintf("\n\n");
    }
}
