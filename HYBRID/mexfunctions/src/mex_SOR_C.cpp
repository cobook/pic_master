// mex_SOR_C.cpp
#include "mex.h" 
#include <math.h>
#include <stdio.h>
#include <string.h>

/*!
 * @SOR @:Successive Over-Relaxation
 * @param[inout] A System Matrix
 * @param[in] x initial input
 * @param[in] b RHS
 * @param[in] n Number of elements
 * @param[in] w acceleration factor ([0,2])
 * @param[inout] max_iter (Number of iterations)
 * @param[inout] eps maximum error 
 */

void SOR(double *A, double *x, double *b, double *w, int max, double eps, int N, double *ans, double *err, double *iter){
  int i,j,k;
  double xold[N];
  double omega = w[0];
  
  memcpy(xold,x,sizeof(double)*N);

  for(k=0;k<max;k++){
    err[0]=0.0;
    for(i=0;i<N;i++){
      x[i]=b[i];
      for(j=0;j<N;j++) {
    	if(j>i) {
          x[i]-=A[i+N*j]*xold[j];
    	}else if(j<i) {
          x[i]-=A[i+N*j]*x[j];
    	}
      }
      x[i]=x[i]/A[i+N*i];
      x[i] = xold[i] + omega*(x[i]-xold[i]);
    }
    for(i=0;i<N;i++) {
      err[0]+=fabs(xold[i]-x[i]);         
      xold[i]=x[i];
    }
    if(err[0]<eps){
        break;     
    }
  }  
  memcpy(ans, x, sizeof(double)*N);
  //memcpy(iter, &k, sizeof(k));
  iter[0] = (double)k;
  /*
   * for (i=0; i<N; i++) {
        mexPrintf("x[%i] = %f\n", i, x[i]);
  }
  for (i=0; i<N; i++) {
        mexPrintf("ans[%i] = %f\n", i, ans[i]);
  }  
   **/
  
}

void mexFunction( int Nreturned, mxArray *returned[], int Noperand, const
mxArray *operand[] ){
double *A, *x, *b, *w, *tol;
double *ans, *err, *iter;
double *max;
int rows,cols,n;
mxArray *A_in_m, *x_in_m, *b_in_m, *w_in_m, *max_in_m, *tol_in_m;
mxArray *ans_in_m, *err_in_m, *iter_in_m;
A_in_m = mxDuplicateArray(operand[0]);
x_in_m = mxDuplicateArray(operand[1]);
b_in_m = mxDuplicateArray(operand[2]);
w_in_m = mxDuplicateArray(operand[3]);
max_in_m = mxDuplicateArray(operand[4]);
tol_in_m = mxDuplicateArray(operand[5]);
A = mxGetPr(A_in_m);
x = mxGetPr(x_in_m);
b = mxGetPr(b_in_m);
w = mxGetPr(w_in_m);
max = mxGetPr(max_in_m);
tol = mxGetPr(tol_in_m);
/* Matlab Get number of elements */
n = mxGetM(A_in_m);
/* Matlab malloc(). */
/* Matlab number (rows), number(cols), type(mxREAL) */ 
ans_in_m = returned[0] = mxCreateDoubleMatrix(mxGetM(x_in_m),mxGetN(x_in_m), mxREAL);
err_in_m = returned[1] = mxCreateDoubleMatrix(1,1, mxREAL);
iter_in_m = returned[2] = mxCreateDoubleMatrix(1,1, mxREAL);


ans = mxGetPr(ans_in_m);
err = mxGetPr(err_in_m);
iter = mxGetPr(iter_in_m);

SOR(A, x, b, w, *max, *tol, n, ans, err, iter);
}