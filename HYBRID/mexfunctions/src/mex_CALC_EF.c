// mex_CALC_EF.c
// Author: Hiroki Fujii, Tokyo Tech, (C) 2016
// Hasegawa laboratory

// Input rhs
// 0. index of cell number
// 1. weight on each cell
// 2. particle velocity
// 3. nz
// 4. nr
// 5. np

// Input rhs
// 0. mu
// 1. B
// 2. Bext
// 3. Jion
// 4. Te
// 5. nion
// 6. QE
//(
// 7. dz
// 8. dr
//)
// 9. coeff 
// 10. rsty0
// 11. sigma
// 12. n_thre
// 13. dt
// 14. itn (iteration number)
// 15. rsty


// Output lhs
// 0. ef
// 1. vexB
// 2. nablap

#include "math.h"
#include "mex.h"
#include "string.h"


//#define _DEBUG_INFO_ 1
#define SHOWSIZE 10
#define MIN(a, b) ((a) < (b) ? (a) : (b))
//#define MAX_SIZE 100000

// Prototype
void show_vector(double*,int,int,int);
void input_process(double*,double*,double*,double*,double*,double*, double*,double,double,int,int,int,int,int,int,double*);
void rotate_B(double*, double*, double, double, int, int, int);
//void gradient_ni(double*, double*, double, double, int, int, int);
//void set_rsty(double*, double*, double, double*, double*, double, double, double, double, int, int, int);
void copy_cells(double*, int, int, int, int);
void hall_term(double*, double*, double* , double , double*, double* , double , int , int, int);
//void pressure_term(double*, double*, double, double, double*, int, int, int );
void rotate_E(double*, double*, double, double, int, int, int, double*); 
// Electric field calculation
void calc_elec(double*, double*, double*, double*, double, double*, double*, double,
        double*, double*, double, double, double, double, int, int, int, double*);
void filter_field(double*, int, int, int, int, double*);
void elec_boundary(double*, double*, double*, int, int, int);
void rotate_bound(double*,double*, int, int, int);


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    mwSize i, j, k;
    mwSize c, d1, d2, d3, d;
    double *out0, *out1, *out2, *out3, *out4;
    double mu, kTe, QE;
    double dz, dr;
    double *B, *Bext, *Ji, *ni;
    double r_coeff, r_rsty0, n_thre, r_sigma;
    double dt, itn;
    int idx, nn, np;
    int dim = 3;
    const mwSize *Bsize, *Jsize;
    double *rsty;
    double *bw0, *bw1;
    double *wc, *cc1;
// Filter parameter : Gaussian sigma=0.5
    double H[9] = {0.0113, 0.0838, 0.0113, 0.0838, 0.6193, 0.0838, 0.0113, 0.0838, 0.0113};    
    // Reject bad input:
    
    if (nrhs != 20) {
        mexErrMsgIdAndTxt("BadInput",
                "Argument number misunderstood. You need exactly 16 arguments.");
    }
    
    // Process Input:
    mu          =       mxGetScalar(prhs[0]); // index for accumulations
    B           =       (double*)mxGetData(prhs[1]); // Weight on each cell
    Bsize       =       mxGetDimensions(prhs[1]);
    mwSize nz0=*Bsize, nr0=*(Bsize+1), nd0=*(Bsize+2);
    int nn0 = nz0*nr0;
    int nz2 = nz0 + 2;
    int nr2 = nr0 + 2;
    int nn2 = nz2*nr2;
    Bext        =       (double*)mxGetData(prhs[2]); // Particle velocity
    Ji          =       (double*)mxGetData(prhs[3]);  // The size of input matrix
    Jsize       =       mxGetDimensions(prhs[3]);
    int mz0=*Jsize, mr0=*(Jsize+1), md0=*(Jsize+2);
    int mm0 = mz0*mr0;
    kTe          =       mxGetScalar(prhs[4]);
    ni          =       (double*)mxGetData(prhs[5]);
    QE          =       mxGetScalar(prhs[6]);
    dz          =       mxGetScalar(prhs[7]);
    dr          =       mxGetScalar(prhs[8]);
    r_coeff        =       mxGetScalar(prhs[9]);
    r_rsty0        =       mxGetScalar(prhs[10]);
    r_sigma        =       mxGetScalar(prhs[11]);
    n_thre        =       mxGetScalar(prhs[12]);
    dt          =       mxGetScalar(prhs[13]);
    itn          =       mxGetScalar(prhs[14]);
    rsty        =       (double*)mxGetData(prhs[15]);
    bw0  =      (double*)mxGetData(prhs[16]);
    bw1  =       (double*)mxGetData(prhs[17]);  // box_weight
    wc =        (double*)mxGetData(prhs[18]);
    cc1 =        (double*)mxGetData(prhs[19]);

    double cc0[4], cc2[4];
    cc0[0]   = cc1[0];
    cc0[1]   = cc1[1];
    cc0[0+2] = cc1[0+2]-1;
    cc0[1+2] = cc1[1+2]-1;
    
    double h = dt/itn;
    
#ifdef _DEBUG_INFO_
    mexPrintf("mz0 = %d\n", mz0);
    mexPrintf("nz0 = %d\n", nz0);
    mexPrintf("mr0 = %d\n", mr0);
    mexPrintf("nr0 = %d\n", nr0);
    mexPrintf("kTe = %g\n", kTe);
#endif
    if ( nz0!=mz0-1 || nr0!=mr0-1 || nd0!=md0 ){
        mexErrMsgIdAndTxt("BadInput",
                "Dimensions inconsistent.");
    }

    double *curlBp, *Bint, *nablan, *rstyJ, *pt, *ht;
    double *elec, *rotE, *Bs, *Bn, *Bp;


    curlBp = calloc(mm0*md0, sizeof(double));
    Bint = calloc(mm0*md0, sizeof(double));
    nablan = calloc(mm0*md0, sizeof(double));
    rstyJ = calloc(mm0*md0, sizeof(double));
    pt = calloc(mm0*md0, sizeof(double));
    ht = calloc(mm0*md0, sizeof(double));
    elec = calloc(mm0*md0, sizeof(double));
    rotE = calloc(nn0*nd0, sizeof(double));
    Bs = calloc(nn0*nd0, sizeof(double));
    Bn = calloc(nn0*nd0, sizeof(double));
    Bp = calloc(nn2*nd0, sizeof(double));

    /*
// Initialize parameters
    double curlBp[mz0*mr0*md0];
    double Bint[mz0*mr0*md0];
    double nablan[mz0*mr0*md0];
//    double rsty[mz0*mr0*md0];
    double rstyJ[mz0*mr0*md0];
    double pt[mz0*mr0*md0];
    double ht[mz0*mr0*md0];
    double elec[mz0*mr0*md0];
    double rotE[nz0*nr0*nd0];
    double Bs[nz0*nr0*nd0];
    double Bn[nz0*nr0*nd0];
    double Bp[nz2*nr2*nd0];     *
     *
    memset(&curlBp[0], 0x00, sizeof(curlBp));
    memset(&Bint[0], 0x00, sizeof(Bint));
    memset(&nablan[0], 0x00, sizeof(nablan));
    memset(&rstyJ[0], 0x00, sizeof(rstyJ));
    memset(&pt[0], 0x00, sizeof(pt));
    memset(&ht[0], 0x00, sizeof(ht));
    memset(&elec[0], 0x00, sizeof(elec));
    memset(&rotE[0], 0x00, sizeof(rotE));
    memset(&Bs[0], 0x00, sizeof(Bs));
    memset(&Bn[0], 0x00, sizeof(Bn));
    memset(&Bp[0], 0x00, sizeof(Bp));
     **/
    
    int it;
    for (it = 0; it<(int)itn; it++){
#ifdef _DEBUG_INFO_
    mexPrintf("========================================== \n");
    mexPrintf("it = %d  \n",it);
#endif
    //========================START PROCESS INPUT PARAMS========================
    //Change dimensions of magnetic field, number density
    //calculate rotation of magnetic field intensity
    //take gradient of number density
        if (it==0) {
            input_process(curlBp, Bp, Bint, nablan, B, Bext, ni, dz, dr, mz0, mr0, md0, nz0, nr0, nd0, bw1);
        } else if (it%2==0) {
            input_process(curlBp, Bp, Bint, nablan, Bs, Bext, ni, dz, dr, mz0, mr0, md0, nz0, nr0, nd0, bw1);
        } else if (it%2==1) {
            input_process(curlBp, Bp, Bint, nablan, Bn, Bext, ni, dz, dr, mz0, mr0, md0, nz0, nr0, nd0, bw1);
        }

    //========================SET PRESSURE TERM========================
    // Calculate electron pressure from the result of nabla n 
    //    pressure_term(pt, ni, kTe, QE, nablan, mz0, mr0, md0);

        

//========================START RESISTIVITY CALCULATION========================
    //Calculate resistivity term depending on the number density on each cell
        //set_rsty(rstyJ, rsty, mu, curlBp, ni, r_coeff, r_rsty0, r_sigma, n_thre, mz0, mr0, md0);

 /*
    //========================SET HALL TERM========================
    // Calculate hall term 
        hall_term(ht, Ji, ni, QE, curlBp, Bint, mu, mz0, mr0, md0);
    //========================Electric field calculation========================
        for (k=0;k<md0;k++){
            for (i=1;i<mz0-1;i++){
                for (j=1;j<mr0-1;j++){
                    elec[i+mz0*j+mm0*k] = rstyJ[i+mz0*j+mm0*k] + ht[i+mz0*j+mm0*k]; 
                }
            }
        }
 */
        //rotate_bound(curlBp, cc1, mz0, mr0, md0);
              
    
        calc_elec(elec,ht,Ji,ni,QE,curlBp,Bint,mu,rstyJ,rsty,r_coeff,r_rsty0,r_sigma,n_thre,mz0,mr0,md0,bw1);
        // Filter & Boundary conditions
       // filter_field(elec, mz0, mr0, md0, mm0, H);  // filter (3x3 dims) 
        copy_cells(elec, mz0, mr0, md0, mm0);
        //elec_boundary(elec, wc, cc1, mz0, mr0, md0);
        
    //========================Time Advance of magnetic Field========================
        rotate_E(rotE, elec, dz, dr, nz0, nr0, nd0, bw0);    
        //rotate_bound(rotE, cc0, nz0, nr0, nd0);
        filter_field(rotE, nz0, nr0, nd0, nn0, H);  // filter (3x3 dims) 
        
    

        for (i=0;i<nz0;i++){
            for (j=0;j<nr0;j++){
                for (k=0;k<nd0;k++){                   
                    if (it==0) {
                         Bn[i+nz0*j+nn0*k] = B[i+nz0*j+nn0*k] -   h*rotE[i+nz0*j+nn0*k]; 
                    } else if (it==1) {
                         Bs[i+nz0*j+nn0*k] = B[i+nz0*j+nn0*k] - 2*h*rotE[i+nz0*j+nn0*k]; 

                    } else if (it==itn-1) {
                        if (it%2==0) {
                             Bn[i+nz0*j+nn0*k] = Bn[i+nz0*j+nn0*k] - h*rotE[i+nz0*j+nn0*k]; 
                        } else if (it%2==1) {
                             Bs[i+nz0*j+nn0*k] = Bs[i+nz0*j+nn0*k] - h*rotE[i+nz0*j+nn0*k]; 
                        }
                    } else if (it%2==0) {
                         Bn[i+nz0*j+nn0*k] = Bn[i+nz0*j+nn0*k] - 2*h*rotE[i+nz0*j+nn0*k]; 
                    } else if (it%2==1) {
                         Bs[i+nz0*j+nn0*k] = Bs[i+nz0*j+nn0*k] - 2*h*rotE[i+nz0*j+nn0*k]; 
                    }
                }
            }
        }
#ifdef _DEBUG_INFO_
    if(it==1)
    {
    mexPrintf("========================================== \n");
//    mexPrintf("Bp:  \n");
//    show_vector(Bp, mz0,mr0,md0);
//    mexPrintf("Bn:  \n");
//    show_vector(Bn, mz0,mr0,md0);
//    mexPrintf("curlBp:  \n");
//    show_vector(curlBp, mz0,mr0,md0);
    mexPrintf("Bint:  \n");
    show_vector(Bint, mz0,mr0,md0);
//    mexPrintf("rstyJ:  \n");
//    show_vector(rstyJ, mz0,mr0,md0);
//    mexPrintf("ht:  \n");
//    show_vector(ht, mz0,mr0,md0);
//    mexPrintf("elec:  \n");
//    show_vector(elec, mz0,mr0,md0);
    }
#endif


    }

    for (i=0;i<nz0;i++){
        for (j=0;j<nr0;j++){
            for (k=0;k<nd0;k++){
                Bn[i+nz0*j+nn0*k] = 0.5 * (Bn[i+nz0*j+nn0*k] + Bs[i+nz0*j+nn0*k]);
            }
        }
    }
        

//========================STORE VALUE========================
    const mwSize ndims[3] = {nz0, nr0, nd0};
    plhs[0] = mxCreateNumericArray(3, ndims, mxDOUBLE_CLASS,  mxREAL);
    out0 = mxGetPr(plhs[0]);
    memcpy(out0, Bn, nz0*nr0*nd0 * sizeof(double));

    const mwSize mdims[3] = {mz0, mr0, md0};
    plhs[1] = mxCreateNumericArray(3, mdims, mxDOUBLE_CLASS,  mxREAL);
    out1 = mxGetPr(plhs[1]);
    memcpy(out1, elec, mz0*mr0*md0 * sizeof(double));
    plhs[2] = mxCreateNumericArray(3, mdims, mxDOUBLE_CLASS,  mxREAL);
    out2 = mxGetPr(plhs[2]);
    memcpy(out2, rstyJ, mz0*mr0*md0 * sizeof(double));
    plhs[3] = mxCreateNumericArray(3, mdims, mxDOUBLE_CLASS,  mxREAL);
    out3 = mxGetPr(plhs[3]);
    memcpy(out3, curlBp, mz0*mr0*md0 * sizeof(double));
    plhs[4] = mxCreateNumericArray(3, mdims, mxDOUBLE_CLASS,  mxREAL);
    out4 = mxGetPr(plhs[4]);
    memcpy(out4, rotE, nz0*nr0*nd0 * sizeof(double));

    
    // Free temporary memory
    free(curlBp); 
    free(Bint); 
    free(nablan);
    free(rstyJ); 
    free(pt);
    free(ht);
    free(elec);
    free(rotE);
    free(Bs);
    free(Bn);
    free(Bp);
    
//========================END MAIN FUNCTION========================
    return;
}

void copy_cells(double* mat, int mz0, int mr0, int md0, int mm0){
    //int i, j;
    // Boundary conditions
    for (int j=1;j<mr0-1;j++){
        // Top
        mat[0      +j*mz0+mm0*0] = mat[1    +j*mz0+mm0*0];
        mat[0      +j*mz0+mm0*1] = mat[1    +j*mz0+mm0*1];
        mat[0      +j*mz0+mm0*2] = mat[1    +j*mz0+mm0*2];
        // Bottom
        mat[(mz0-1)+j*mz0+mm0*0] = mat[mz0-2+j*mz0+mm0*0];
        mat[(mz0-1)+j*mz0+mm0*1] = mat[mz0-2+j*mz0+mm0*1];
        mat[(mz0-1)+j*mz0+mm0*2] = mat[mz0-2+j*mz0+mm0*2];
    }
    
    for (int i=0;i<mz0;i++){
        // Left (Axis)
        mat[i+0      *mz0+mm0*0] =  mat[i+1      *mz0+mm0*0];
        mat[i+0      *mz0+mm0*1] = -mat[i+1      *mz0+mm0*1];
        mat[i+0      *mz0+mm0*2] = -mat[i+1      *mz0+mm0*2];
        // Right
        mat[i+(mr0-1)*mz0+mm0*0] =  mat[i+(mr0-2)*mz0+mm0*0];
        mat[i+(mr0-1)*mz0+mm0*1] =  mat[i+(mr0-2)*mz0+mm0*1];
        mat[i+(mr0-1)*mz0+mm0*2] =  mat[i+(mr0-2)*mz0+mm0*2];
    }
    return;
}

void filter_field(double* field, // Field to be filtered
        int mz0, // num of z grid
        int mr0, // num of r grid
        int md0, // num of dims
        int mm0, // z*r grid
        double* H)  // filter (3x3 dims)
{
    double field_temp[mm0*md0];
    memcpy(field_temp, field, mm0*md0*sizeof(double));
    int im, i0, ip;
    int jm, j0, jp;
    // Filter on electric field
    for (int i=0;i<mz0-1; i++) {
        if (i==0)
        {
            im = i;
            i0 = i;
            ip = i+1;
        }
        else if (i==mz0-1)
        {
            im = i-1;
            i0 = i;
            ip = i;
        }
        else
        {
            im = i-1;
            i0 = i;
            ip = i+1;
        }
        for (int j=1;j<mr0-1;j++)
        {
            if (j==0)
            {
                jm = j;
                j0 = j;
                jp = j+1;
            }
            else if (j==mr0-1)
            {
                jm = j-1;
                j0 = j;
                jp = j;
            }
            else
            {
                jm = j-1;
                j0 = j;
                jp = j+1;
            }
            for (int k=0;k<md0;k++)
            {
                
                if (k==0){
                    // Apply filter here
                    field_temp[i+mz0*j+mm0*k] =
                            H[0]*field[im+mz0*jm+mm0*k]
                            + H[1]*field[i0+mz0*jm+mm0*k]
                            + H[2]*field[ip+mz0*jm+mm0*k]
                            + H[3]*field[im+mz0*j0+mm0*k]
                            + H[4]*field[i0+mz0*j0+mm0*k]
                            + H[5]*field[ip+mz0*j0+mm0*k]
                            + H[6]*field[im+mz0*jp+mm0*k]
                            + H[7]*field[i0+mz0*jp+mm0*k]
                            + H[8]*field[ip+mz0*jp+mm0*k];
                }
                if (k==1 || k==2){
                    // Apply filter here
                    field_temp[i+mz0*j+mm0*k] = (
                            H[0]*field[im+mz0*jm+mm0*k]
                            + H[1]*field[i0+mz0*jm+mm0*k]
                            + H[2]*field[ip+mz0*jm+mm0*k]
                            + H[3]*field[im+mz0*j0+mm0*k]
                            + H[4]*field[i0+mz0*j0+mm0*k]
                            + H[5]*field[ip+mz0*j0+mm0*k]
                            + H[6]*field[im+mz0*jp+mm0*k]
                            + H[7]*field[i0+mz0*jp+mm0*k]
                            + H[8]*field[ip+mz0*jp+mm0*k]
                            );
                }
            }
        }
        
    }
    memcpy(field, field_temp, mm0*md0*sizeof(double));
    return;
}



// Electric field calculation
void calc_elec(double*elec, double* ht, double* Ji, double* ni, double QE, double* curlBp, double* Bint, double mu,
        double* rstyJ, double* rsty, double coeff, double rsty0, double sigma, double n_thre, int mz0, int mr0, int md0, double* bw1){
    //int i, j, k;
    //int kp, km;
    int mm0 = mz0*mr0;
    double mq = 1/QE;
    double mmu = 1/mu;
        
    for (int i=1;i<mz0-1;i++){
        for (int j=1;j<mr0-1;j++){

            
            for (int k=0;k<md0;k++){
                       
                int kp = (k+4)%md0;
                int km = (k+2)%md0;
                // ht = 1/QE.*cross(Je, Bint)/ni
                ht[i+j*mz0+mm0*k] = mq / ni[i+j*mz0]
                        * ((curlBp[i+j*mz0+mm0*kp]*mmu - Ji[i+j*mz0+mm0*kp]) * Bint[i+j*mz0+mm0*km]
                        -  (curlBp[i+j*mz0+mm0*km]*mmu - Ji[i+j*mz0+mm0*km]) * Bint[i+j*mz0+mm0*kp]);
                
                // Resistivity * J calculation
                rstyJ[i+j*mz0+mm0*k] = mmu*curlBp[i+j*mz0+mm0*k]*rsty[i+j*mz0];     
                
                // Electric field calculation
                elec[i+mz0*j+mm0*k] = rstyJ[i+mz0*j+mm0*k] + ht[i+mz0*j+mm0*k];   
            }                          
        }
    }
    
    /* This takes so long time... 
    
    // Apply filter using MATLAB function prepared in ../src
    mxArray *lhs[1];
    mxArray *infilter[4];
    mwSize esize[3] = {mz0,mr0,3};
    double GPU = 1;
    double dmz0 = (double)mz0;
    double dmr0 = (double)mr0;
    infilter[0] = mxCreateDoubleMatrix(1,1,mxREAL);
    infilter[1] = mxCreateDoubleMatrix(1,1,mxREAL);
    infilter[2] = mxCreateDoubleMatrix(1,1,mxREAL);
    infilter[3] = mxCreateNumericArray(3,esize,mxDOUBLE_CLASS,mxREAL);
    
    memcpy(mxGetPr(infilter[0]), (&GPU), sizeof(double));
    memcpy(mxGetPr(infilter[1]), (&dmz0), sizeof(double));
    memcpy(mxGetPr(infilter[2]), (&dmr0), sizeof(double));
    memcpy(mxGetPr(infilter[3]), elec, sizeof(double)*mz0*mr0*3);
    mexCallMATLAB(1, lhs, 4, infilter, "field_filter");
    memcpy(elec, (double*)mxGetPr(lhs[0]), sizeof(double)*mz0*mr0*3);
    
    */
}

// Boundary of wall cells
// E perp should be the same
// E tangent should be zero in boundary cells
void elec_boundary(double* elec, double* wc, double* cc, int mz0, int mr0, int md0)
{
// wc contains index of wall cells
// in the form of
// wc = [[zmin zmax]
//      [rmin rmax]]
    int mm0 = mz0*mr0;
// Clear all values inside wc box in elec
// except neiboring ez to the vacuum region
    for (int i = wc[0];i<wc[0+2];i++){
        for (int j = wc[1];j<wc[1+2];j++){
            //Ez
            if (i==wc[2]){
                elec[i + mz0*j + 0*mm0] = elec[(i+1) + mz0*j];
            }else{
                elec[i + mz0*j + 0*mm0] = 0;
            }
            //Er
            elec[i +mz0*j + 1*mm0] = 0;
            //Etheta
            elec[i +mz0*j + 2*mm0] = 0;
        }
    }
//*  Coil cells boundary inappropriate
 // cc contains index of wall cells
// in the form of
// cc = [[zmin zmax]
//      [rmin rmax]]
// Coil cells
for (int i = cc[0];i<cc[0+2];i++){
    for (int j = cc[1];j<cc[1+2];j++){
        //Ez
        if (i==cc[0]){
            elec[i + mz0*j + 0*mm0] = elec[(i-1) + mz0*j + 0*mm0];
        }else if (i==cc[2]){
            elec[i + mz0*j + 0*mm0] = elec[(i+1) + mz0*j + 0*mm0];
        }else{
            elec[i + mz0*j + 0*mm0] = 0;
        }
        //Er
        if (j==cc[1]){
            elec[i + mz0*j + 1*mm0] = elec[i + mz0*(j-1) + 1*mm0];
        }else if (j==cc[3]){
            elec[i + mz0*j + 1*mm0] = elec[i + mz0*(j+1) + 1*mm0];
        }else{
            elec[i + mz0*j + 1*mm0] = 0;
        }        
        //Etheta
        elec[i +mz0*j + 2*mm0] = 0;
    }
}
 //*/

}
        
// Set rotE 0 to the corner of coil cells
void rotate_bound(double* field, 
        double* cc,
        int nz, 
        int nr, 
        int nd)
{
    int nn = nz*nr;
    // Coil cells
    for (int i = cc[0];i<cc[0+2];i++){
        for (int j = cc[1];j<cc[1+2];j++){
            //axial
            field[i + nz*j + 0*nn] = 0;            
            //radial
            field[i + nz*j + 1*nn] = 0;
            //theta
            field[i + nz*j + 2*nn] = 0;
        }
    }
   /*     
    for (int k = 0;k < nd0;k++){
        rotE[ (int)cc[0]    + nz0* (int)cc[1]      + k*nn0] = 0;
        rotE[ (int)cc[0]    + nz0*((int)cc[1+2]-1) + k*nn0] = 0;
        rotE[((int)cc[2]-1) + nz0* (int)cc[1]      + k*nn0] = 0;
        rotE[((int)cc[2]-1) + nz0*((int)cc[1+2]-1) + k*nn0] = 0;        
    }
    */
}



// Hall term calculation
void hall_term(double* ht, double* Ji, double* ni, double QE, double* curlBp, double* Bint, double mu, int mz0, int mr0, int md0){
    //int i, j, k;
    //int kp, km;
    int mm0 = mz0*mr0;
    double mq = 1/QE;
    double mmu = 1/mu;
       
    for (int i=1;i<mz0-1;i++){
        for (int j=1;j<mr0-1;j++){
            for (int k=0;k<md0;k++){
                int kp = (k+4)%md0;
                int km = (k+2)%md0;
                
                // ht = 1/QE.*cross(Je, Bint)/ni
                ht[i+j*mz0+mm0*k] = mq / ni[i+j*mz0]
                    * ((curlBp[i+j*mz0+mm0*kp]*mmu - Ji[i+j*mz0+mm0*kp]) * Bint[i+j*mz0+mm0*km]
                    -  (curlBp[i+j*mz0+mm0*km]*mmu - Ji[i+j*mz0+mm0*km]) * Bint[i+j*mz0+mm0*kp]);
            }
        }
    }
}

// Advection term calculation
void advection_term(){
}

// Pressure term calculation
void pressure_term(double* pt, double* ni, double kTe, double QE, double* nablan, int mz0, int mr0, int md0 ){
    //int i, j, k;
    int mm0 = mz0*mr0;
    double mqT = - 1/QE*kTe;

    for (int i=1;i<mz0-1;i++){
    for (int j=1;j<mr0-1;j++){
    for (int k=0;k<md0-1;k++){
                pt[i+j*mz0+mm0*k] = mqT * nablan[i+j*mz0+mm0*k] / ni[i+j*mz0];
            }
        }
    }
}


void input_process(double* curlBp, double* Bp, double* Bint, double* nablan, 
        double *B, double *Bext, double *ni,
        double dz,double dr,int mz0,int mr0,int md0, int nz0, int nr0, int nd0, double* bw1){

    //int i, j, k;
    int nn0=nz0*nr0;
    int nz2=nz0+2;
    int nr2=nr0+2;
    int nn2=nz2*nr2;
    //int zidx, ridx;
    //int id, jd;
//    double Bp[(nz2)*(nr2)*(nd0)];
    

    //int i0, j0, i1, j1;
    int mm0 = mz0*mr0; 
    
    //int ip, im, jp, jm;    
    //double a;
    double dr2 = 0.5/dr;
    double dz2 = 0.5/dz;    

    int mz1 = mz0+1;
    int mr1 = mr0+1;
    int mm1 = mz1*mr1;
    //double dArdz, dAzdr, rj, rj0, rj1;
        
    // Calculate Bp=B-Bext
    // SIZE(Bp) : nz0+2, nr0+2, 3
    
    for (int i=0;i<nz2;i++){
        for (int j=1;j<nr2;j++){
            for (int k=0;k<nd0;k++){
                int zidx = i-1;
                int ridx = j-1;  
                if (zidx==nz0)
                {
                    zidx = nz0-1;
                }
                else if (zidx==-1)
                {
                    zidx = 0;
                }
                
                if (ridx==nr0){
                    ridx = nr0-1;
                }
                Bp[i+nz2*j+nn2*k] = B[zidx+nz0*ridx+nn0*k] - Bext[zidx+nz0*ridx+nn0*k];
                
                // Boundary on the axis
                if (k==0) Bp[i+nz2*0+nz2*nr2*0] =   Bp[i+nz2*2+nz2*nr2*0];
                else      Bp[i+nz2*0+nz2*nr2*k] = - Bp[i+nz2*2+nz2*nr2*k];
            }

        }
    }

#ifdef _DEBUG_INFO_
//    mexPrintf("========================================== \n");
//    mexPrintf("Bp :\n");
//    show_vector(Bp, nz2,nr2,nd0);
#endif

    

    for (int i=0;i<mz0;i++){
        for (int j=1;j<mr0;j++){
            for (int k=0;k<md0;k++){
//==================================================================
// Calculate interpolated B               
                int i0 = i;
                int j0 = j;
                int i1 = i-1;
                int j1 = j-1;
                if (i==0)
                {
                    i1 = 0;
                }
                else if (i==mz0-1)
                {
                    i0 = mz0-2;
                }
                if (j==0)
                {
                    j1=0;
                }
                if (j==mr0-1)
                {
                    j0 = mr0-2;
                }

                if (k!=0) {
                //Interpolation
                Bint[i + mz0*j + mm0*k] = ((j0) * B[i0 + j0*nz0 + nn0*k]
                        + (j0) * B[i1 + j0*nz0 + nn0*k]
                        + (j1) * B[i0 + j1*nz0 + nn0*k]
                        + (j1) * B[i1 + j1*nz0 + nn0*k]) / (4*(j0-0.5));
                } else {
                Bint[i + mz0*j + mm0*k] = 0.25 * ( B[i0 + j0*nz0 + nn0*k]
                        +  B[i1 + j0*nz0 + nn0*k]
                        +  B[i0 + j1*nz0 + nn0*k]
                        +  B[i1 + j1*nz0 + nn0*k]);  
                }
            }
            /*
//==================================================================
//  Calculation of gradient ni
// Simple stencil calculation
            // In z
            a = 1;
            ip = i+1;
            im = i-1;
            if (ip==mz0){
                ip = mz0-1;
                a = 2;
            }
            if (im==-1){
                im = 0;
                a = 2;
            }

            nablan[i+j*mz0+mm0*0] = a * dz2 *(ni[ip+j*mz0] - ni[im+j*mz0]) ;
            // In r
            a = 1;
            jp = j+1;
            jm = j-1;
            if (jp==mr0){
                jp = mr0-1;
                a = 2;
            }
            if ( j==1 ) {
                a = 2;
                nablan[i+j*mz0+mm0*1] = a * dr2 *(ni[i+jp*mz0] - ni[i+j*mz0]) ;
            } else {
                nablan[i+j*mz0+mm0*1] = a * dr2 *(ni[i+jp*mz0] - ni[i+jm*mz0]) ;
            } 
             **/
//==================================================================
// Calculate rotation Bp  
            if (!(i==0 || i==mz0-1 || j==0 || j==mr0-1))
            {
                
                int i0 = i;
                int j0 = j;
                int i1 = i+1;
                int j1 = j+1;
                
                double rj  = (double)j-0.5;
                double rj0 = (double)j0-1;
                double rj1 = (double)j1-1;

                // Z direction (k=0)
                curlBp[i + j*mz0 + mm0*0] = dr2 / rj * (
                        Bp[i1+j1*mz1+mm1*2]*rj1
                        - Bp[i1+j0*mz1+mm1*2]*rj0
                        + Bp[i0+j1*mz1+mm1*2]*rj1
                        - Bp[i0+j0*mz1+mm1*2]*rj0
                        );
                
                // R direction (k=1)
                curlBp[i + j*mz0 + mm0*1] = - dz2 * (
                        Bp[i1+j1*mz1+mm1*2]
                        - Bp[i0+j1*mz1+mm1*2]
                        + Bp[i1+j0*mz1+mm1*2]
                        - Bp[i0+j0*mz1+mm1*2]
                        );
                
                // Theta direction (k=2)
                double dArdz = dz2 * (
                        Bp[i1+j1*mz1+mm1*1]
                        - Bp[i0+j1*mz1+mm1*1]
                        + Bp[i1+j0*mz1+mm1*1]
                        - Bp[i0+j0*mz1+mm1*1]
                        );
                double dAzdr = dr2 * (
                        Bp[i1+j1*mz1+mm1*0]
                        - Bp[i1+j0*mz1+mm1*0]
                        + Bp[i0+j1*mz1+mm1*0]
                        - Bp[i0+j0*mz1+mm1*0]
                        );
                curlBp[i + j*mz0 + mm0*2] = dArdz - dAzdr;
                
/*
                // Box weighting
                // Inside coil, current should be zero
                curlBp[i + j*mz0 + mm0*0] *= bw1[i+j*mz0+mm0*0];
                curlBp[i + j*mz0 + mm0*1] *= bw1[i+j*mz0+mm0*1];
                curlBp[i + j*mz0 + mm0*2] *= bw1[i+j*mz0+mm0*2]; 
                */
                
            }
        }
    }
    
    
    // considering rapid expansion and the reduction of accuracy near the axis, 
    // boudnary value of Bp modified using quadratic equations
    for (int i=0;i<mz0;i++){
            // quadratic equation b exists
            curlBp[i + 1*mz0 + mm0*0] = 1.5*curlBp[i + 2*mz0 + mm0*0] - 0.5*curlBp[i + 3*mz0 + mm0*0];
            // quadratic equation b = 0
            curlBp[i + 1*mz0 + mm0*1] = 1.5*curlBp[i + 2*mz0 + mm0*1] - 0.5*curlBp[i + 3*mz0 + mm0*1];
            curlBp[i + 1*mz0 + mm0*2] = 1.5*curlBp[i + 2*mz0 + mm0*2] - 0.5*curlBp[i + 3*mz0 + mm0*2]; 
    }

    // Boundary conditions
    copy_cells(curlBp, mz0, mr0, md0, mm0);
    
    return;
}
/*
// Function for the Calculation of gradient ni
// Simple stencil calculation
void gradient_ni(double *nablan, double *ni, double dz, double dr, int mz0, int mr0, int md0) {
    int i,j;
    int ip, im, jp, jm;
    int mm0 = mz0*mr0;
    double a;
    double dr2 = 0.5/dr;
    double dz2 = 0.5/dz;
    for (i=0;i<mz0;i++){
        for (j=1;j<mr0;j++){
           
            // In z
            a = 1;
            ip = i+1;
            im = i-1;
            if (ip==mz0){
                ip = mz0-1;
                a = 2;
            }
            if (im==-1){
                im = 0;
                a = 2;
            }

            nablan[i+j*mz0+mm0*0] = a * dz2 *(ni[ip+j*mz0] - ni[im+j*mz0]) ;
            // In r
            a = 1;
            jp = j+1;
            jm = j-1;
            if (jp==mr0){
                jp = mr0-1;
                a = 2;
            }
            if ( j==1 ) {
                a = 2;
                nablan[i+j*mz0+mm0*1] = a * dr2 *(ni[i+jp*mz0] - ni[i+j*mz0]) ;
            } else {
                nablan[i+j*mz0+mm0*1] = a * dr2 *(ni[i+jp*mz0] - ni[i+jm*mz0]) ;
            }
        }
    }

#ifdef _DEBUG_INFO_
//    mexPrintf("========================================== \n");
//    mexPrintf("nablan :\n");
//    show_vector(nablan, mz0,mr0,md0);
#endif
 
    return;
}
 */

// Function for the Calculation of rotation E
void rotate_E(double *curlE, double *E, double dz, double dr, int nz0, int nr0, int nd0, double* bw0) {
    //int i0, j0, i1, j1;
    //int i, j;
    int nn0 = nz0*nr0;
    int nz1 = nz0+1;
    int nr1 = nr0+1;
    int nn1 = nz1*nr1;
    double dr2 = 0.5/dr;
    double dz2 = 0.5/dz;
    //double dArdz, dAzdr, rj, rj0, rj1;
    
    for (int i=0;i<nz0;i++){
        for (int j=1;j<nr0;j++){
            int i0 = i;
            int i1 = i+1;
            int j0 = j;
            int j1 = j+1;
            
            double rj  = (double)j;
            double rj0 = (double)j0-0.5;
            double rj1 = (double)j1-0.5;

            // Z direction (k=0)
            curlE[i+j*nz0+nn0*0] = dr2 / rj * (
                  E[i1+j1*nz1+nn1*2]*rj1 
                - E[i1+j0*nz1+nn1*2]*rj0
                + E[i0+j1*nz1+nn1*2]*rj1 
                - E[i0+j0*nz1+nn1*2]*rj0
             );
            
            // R direction (k=1)
            curlE[i+j*nz0+nn0*1] = - dz2 * (
              E[i1+j1*nz1+nn1*2]
            - E[i0+j1*nz1+nn1*2]
            + E[i1+j0*nz1+nn1*2] 
            - E[i0+j0*nz1+nn1*2]
             );
            
            // Theta direction (k=2)
            double dArdz = dz2 * (
                  E[i1+j1*nz1+nn1*1] 
                - E[i0+j1*nz1+nn1*1]
                + E[i1+j0*nz1+nn1*1] 
                - E[i0+j0*nz1+nn1*1]
                );
            double dAzdr = dr2 * (
                  E[i1+j1*nz1+nn1*0] 
                - E[i1+j0*nz1+nn1*0]
                + E[i0+j1*nz1+nn1*0]
                - E[i0+j0*nz1+nn1*0]
                    );
             curlE[i+j*nz0+nn0*2] = dArdz - dAzdr;
             
             curlE[i+j*nz0+nn0*0] *= bw0[i+j*nz0+nn0*0];
             curlE[i+j*nz0+nn0*1] *= bw0[i+j*nz0+nn0*1];
             curlE[i+j*nz0+nn0*2] *= bw0[i+j*nz0+nn0*2];
                     
        }
       // Boundary conditions on the axis
       // curlE[i + nn0*0] = 4/dr * (E[i0+1*nz0+nn0*2] + E[i1+1*nz0+nn0*2])/2;
//        the equation above is too sensitive and doesn't work properly, 
//        Let's just use the same value
        curlE[i + nn0*0] = curlE[i + 1*nz0 + nn0*0];
        curlE[i + nn0*1] = 0;
        curlE[i + nn0*2] = 0;
    }

#ifdef _DEBUG_INFO_
//    mexPrintf("========================================== \n");
//    mexPrintf("curlE :\n");
//    show_vector(curlE, nz0,nr0,nd0);
#endif

    return;
}



/*
// Function for the Calculation of rotation Bp
void rotate_B(double *curlBp, double *Bp, double dz, double dr, int mz0, int mr0, int md0) {
    int i0, j0, i1, j1;
    int i, j;
    int mm0 = mz0*mr0;
    int mz1 = mz0+1;
    int mr1 = mr0+1;
    int mm1 = mz1*mr1;
    double dr2 = 0.5/dr;
    double dz2 = 0.5/dz;
    double dArdz, dAzdr, rj, rj0, rj1;
    
    for (i=1;i<mz0-1;i++){
        for (j=1;j<mr0-1;j++){
            i0 = i;
            j0 = j;
            i1 = i+1;
            j1 = j+1;

            rj  = (double)j-0.5;
            rj0 = (double)j0-1;
            rj1 = (double)j1-1;

            // Z direction (k=0)
            curlBp[i+j*mz0+mm0*0] = dr2 / rj * (
                  Bp[i1+j1*mz1+mm1*2]*rj1 
                - Bp[i1+j0*mz1+mm1*2]*rj0
                + Bp[i0+j1*mz1+mm1*2]*rj1 
                - Bp[i0+j0*mz1+mm1*2]*rj0
             );
            
            // R direction (k=1)
            curlBp[i+j*mz0+mm0*1] = - dz2 * (
              Bp[i1+j1*mz1+mm1*2]
            - Bp[i0+j1*mz1+mm1*2]
            + Bp[i1+j0*mz1+mm1*2] 
            - Bp[i0+j0*mz1+mm1*2]
             );
            
            // Theta direction (k=2)
            dArdz = dz2 * (
                  Bp[i1+j1*mz1+mm1*1] 
                - Bp[i0+j1*mz1+mm1*1]
                + Bp[i1+j0*mz1+mm1*1] 
                - Bp[i0+j0*mz1+mm1*1]
                );
            dAzdr = dr2 * (
                  Bp[i1+j1*mz1+mm1*0] 
                - Bp[i1+j0*mz1+mm1*0]
                + Bp[i0+j1*mz1+mm1*0]
                - Bp[i0+j0*mz1+mm1*0]
                    );
             curlBp[i+j*mz0+mm0*2] = dArdz - dAzdr;
        }
    }

    // Boundary conditions
    copy_cells(curlBp, mz0, mr0, md0, mm0);

#ifdef _DEBUG_INFO_
    mexPrintf("========================================== \n");
    mexPrintf("curlBp :\n");
    show_vector(curlBp, mz0,mr0,md0);
#endif

    return;
}
*/

void show_vector(double *a, int nz, int nr, int nd) {
    int i,j,k;
    
    for (k=0;k<nd;k++){
        mexPrintf("Dim=%d\n",k+1);
//        for (i=0;i<nz;i++){
        for (i=0;i<MIN(SHOWSIZE,nz);i++){
//            for (j=0;j<nr;j++){
            for (j=0;j<MIN(SHOWSIZE,nr);j++){
                mexPrintf("%g ",a[i+nz*j+nz*nr*k]);
            }
            mexPrintf("\n");
        }
        mexPrintf("\n\n");
    }
}


/*
// Resistivity and resistivity*J 
void set_rsty(double* rstyJ, double* rsty, double mu, double* curlBp, double* ni, double coeff, double rsty0, double sigma, double n_thre, int mz0, int mr0, int md0) {
    int i, j, k;
    int mm0 = mz0*mr0;
    double mmu = 1/mu;

    for (i=1;i<mz0-1;i++){
        for (j=1;j<mr0-1;j++){
            rsty[i + j*mz0] = 0.5*1e-12*(coeff/ni[i+j*mz0]-rsty0)*(1-tanh((ni[i+j*mz0]-n_thre)/sigma)) + rsty0;
            for (k=0;k<md0;k++){
                rstyJ[i+j*mz0+mm0*k] = mmu*curlBp[i+j*mz0+mm0*k]*rsty[i+j*mz0];
            }
        }
    }
#ifdef _DEBUG_INFO_
//    mexPrintf("========================================== \n");
//    mexPrintf("rstyJ :\n");
//    show_vector(rstyJ, mz0,mr0,md0);
#endif
    return;
}
*/
