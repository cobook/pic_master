// mex_flux_weight.c
// Author: Hiroki Fujii, Tokyo Tech, (C) 2016
// Hasegawa laboratory

// Input rhs
// 0. index of cell number
// 1. weight on each cell
// 2. particle velocity
// 3. nz
// 4. nr
// 5. np

// Output lhs
// 0. chg
// 1. flux

#include "mex.h"
#include "string.h"

//#define _DEBUG_INFO_ 1
//#define MAX_SIZE 100000

mwSize accumarray(double*, double*, double*, int);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    mwSize i, j, k, nz, nr;
    mwSize c, d1, d2, d3, d;
    double *a, *cell_weight, aa, *m, *cell_idx, *vel, *f_idx,
            *chg, *p, *flux, *q, *out0, *out1, *out2, *out3, *out4;
    double *f1, *f2, *f3, *fz, *fr, *ft;
    int idx, nn, np;
    int dim = 3;
    double *fwz, *fwr, *fwt;
    
    // Reject bad input:
    if (nrhs !=6) {
        mexErrMsgIdAndTxt("BadInput",
                "Argument number misunderstood. You need exactly 6 arguments.");
    }
    // 0. Cell index
    if ( !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 1) {
        mexErrMsgIdAndTxt("BadInput:0",
                "flux_weight.mex: Inputs must be a double matrix.");
    }
    // 1. Cell weight
    if ( !mxIsDouble(prhs[1]) || mxGetN(prhs[1]) != 1) {
        mexErrMsgIdAndTxt("BadInput:1",
                "flux_weight.mex: Inputs must be a double matrix.");
    }
    // 2. velocity
    if ( !mxIsDouble(prhs[2]) || mxGetN(prhs[2]) != 1) {
        mexErrMsgIdAndTxt("BadInput:2",
                "flux_weight.mex: Inputs must be a double matrix.");
    }
    // 3. nz
    // 4. nr
    // 5. np
    if ( !mxIsDouble(prhs[3]) || mxIsComplex(prhs[3]) || mxGetNumberOfElements(prhs[3]) != 1 ||
            !mxIsDouble(prhs[4]) || mxIsComplex(prhs[4]) ||mxGetNumberOfElements(prhs[4]) != 1 ||
            !mxIsDouble(prhs[5]) || mxIsComplex(prhs[5]) || mxGetNumberOfElements(prhs[5]) != 1  ) {
        mexErrMsgIdAndTxt("BadInput:arrayProduct:notScalar",
                "Input multiplier must be a scalar.");
    }
    
    // Process Input:
    cell_idx    =          mxGetPr(prhs[0]); // index for accumulations
    cell_weight =          mxGetPr(prhs[1]); // Weight on each cell
    vel         =          mxGetPr(prhs[2]); // Particle velocity
    nz          = (mwSize)*mxGetPr(prhs[3]);  // The size of input matrix
    nr          = (mwSize)*mxGetPr(prhs[4]);  // The size of input matrix
    np          = (mwSize)*mxGetPr(prhs[5]);  // The number of particles
    nn          = nz * nr;
#ifdef _DEBUG_INFO_
    mexPrintf("nz = %d\n", nz);
    mexPrintf("nr = %d\n", nr);
    mexPrintf("np = %d\n", np);
    mexPrintf("nn = %d\n", nn);
    
    show_vector(cell_idx, 4*np);
    show_vector(cell_weight, 4*np);
    show_vector(vel, np*3);
#endif
    // Early return for empty input:
    if (np == 0) {
        plhs[0] = mxCreateDoubleMatrix(0, 2, mxREAL);
        return;
    }
    // Collect values:
    chg   = mxCalloc(nn, sizeof(double));
    p = chg;

     // Create output:
    c  = accumarray( p, cell_idx, cell_weight, 4*np);
#ifdef _DEBUG_INFO_
    mexPrintf("line3 \n");
    mexPrintf("Size of p = %d\n", sizeof(p));
#endif
    plhs[0] = mxCreateDoubleMatrix(nn, 1, mxREAL);
    out0     = mxGetPr(plhs[0]);
    memcpy(out0,     chg, c * sizeof(double));
//    memcpy(out + c, q0, c * sizeof(double));
    // Free temporary memory:
    mxFree(chg);
    
    // Create output for current flux
    
    fz    = mxCalloc(nn, sizeof(double));
    fr    = mxCalloc(nn, sizeof(double));
    ft    = mxCalloc(nn, sizeof(double));
    flux  = mxCalloc(nn*dim, sizeof(double));
    fwz   = mxCalloc(4 * np, sizeof(double));
    fwr   = mxCalloc(4 * np, sizeof(double));
    fwt   = mxCalloc(4 * np, sizeof(double));
    for (k = 0; k < 4; k++) {
        for (j = 0; j < np; j++) {
            fwz[j+k*np] = cell_weight[j+k*np]*vel[j     ];
            fwr[j+k*np] = cell_weight[j+k*np]*vel[j+  np];
            fwt[j+k*np] = cell_weight[j+k*np]*vel[j+2*np];
        }
    }
#ifdef _DEBUG_INFO_
    mexPrintf("line2 \n\n");
    mexPrintf("fwz = \n");
    show_vector(fwz, 4*np);
    mexPrintf("fwr = \n");
    show_vector(fwr, 4*np);
    mexPrintf("fwt = \n");    
    show_vector(fwt, 4*np);
       
    mexPrintf("d1 = %d \n", d1);
    mexPrintf("d2 = %d \n", d2);    
    
#endif
        
    f1 = fz;
    f2 = fr;
    f3 = ft;
    
    d1 = accumarray(f1, cell_idx, fwz, 4*np);
    d2 = accumarray(f2, cell_idx, fwr, 4*np);
    d3 = accumarray(f3, cell_idx, fwt, 4*np);
#ifdef _DEBUG_INFO_
    mexPrintf("line5 \n\n");
    // mexPrintf("d1 = %d \n", d1);
    // mexPrintf("d2 = %d \n", d2);
    // mexPrintf("d3 = %d \n", d3);
    
    mexPrintf("f1 = \n");
    show_vector(f1, nn);
    mexPrintf("f2 = \n");
    show_vector(f2, nn);
    mexPrintf("f3 = \n");
    show_vector(f3, nn);
#endif
    plhs[1] = mxCreateDoubleMatrix(nn, 3, mxREAL);
    out1    = mxGetPr(plhs[1]);

    for (j = 0; j < nn; j++) {
        out1[j       ] = f1[j];
        out1[j + 1*nn] = f2[j];
        out1[j + 2*nn] = f3[j];
    }
    
#ifdef _DEBUG_INFO_
    plhs[2] = mxCreateDoubleMatrix(nn, 1, mxREAL);
    out2    = mxGetPr(plhs[2]);
    plhs[3] = mxCreateDoubleMatrix(nn, 1, mxREAL);
    out3    = mxGetPr(plhs[3]);
    plhs[4] = mxCreateDoubleMatrix(nn, 1, mxREAL);
    out4    = mxGetPr(plhs[4]);
    memcpy((out2), f1, d1 * sizeof(double));
    memcpy((out3), f2, d2 * sizeof(double));
    memcpy((out4), f3, d3 * sizeof(double));
#endif
    mxFree(fz);
    mxFree(fr);
    mxFree(ft);
    mxFree(flux);
    mxFree(fwz);
    mxFree(fwr);
    mxFree(fwt);
    //free(f1);
    //free(f2);
    //free(f3);
    //free(q0);
    return;
}

mwSize accumarray(double *element, double *index, double *value, int n){
    mwSize nrows, idx;
    int i;
    nrows = (mwSize)index[0] - 1;
#ifdef _DEBUG_INFO_
    mexPrintf("Accumarray function start\n");
    show_vector(element);
#endif    
    // Loop over elements:
    for (i = 0; i < n; i++) {
        idx = (int)(index[i]+0.5) - 1;
        element[idx] += value[i];
        if (idx + 1 > nrows) {    // Repeated element in column 1
            nrows = idx + 1;
        }
#ifdef _DEBUG_INFO_
//        mexPrintf("idx = %d \n", idx);
//        mexPrintf("nrows = %f \n", nrows);
//        mexPrintf("value[i] = %f \n", value[i]);
#endif
    }
#ifdef _DEBUG_INFO_
    mexPrintf("Accumarray function ends\n");    
    show_vector(element);
#endif
    return nrows;
}

void show_vector(double *a, int n) {
//    int n;
    //n = sizeof(a);
    int i;
    
    for (i=0;i<n;i++){
        mexPrintf("%f \n",a[i]);
    }
    mexPrintf("\n\n");
}
