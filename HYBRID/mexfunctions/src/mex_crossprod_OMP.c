// [C] = crossprod(A,B)
#include "mex.h"
#include "omp.h"

#define max(X,Y) (X<Y ? Y : X)

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    size_t m;
    double *A, *B; // Change
    double *C;
    int i;
    int nProc = omp_get_max_threads();
    mexPrintf("nProc = %d \n", nProc);
    omp_set_num_threads(max(12,nProc));
    
    m = mxGetM(prhs[0]);
    A = mxGetPr(prhs[0]); // Change
    B = mxGetPr(prhs[1]); // Change

    plhs[0] = mxCreateDoubleMatrix((int) m, 3, mxREAL);
    C = mxGetPr(plhs[0]);
#pragma omp parallel num_threads(nProc) 
    #pragma omp for
    // Major change - Using column major indexing
    for(i = 0; i < m; i++) {
        C[i] = A[i + m]*B[i + 2*m] - A[i + 2*m]*B[i + m];
        C[i + m] = A[i + 2*m]*B[i] - A[i]*B[i + 2*m]; // Change here too
        C[i + 2*m] = A[i]*B[i + m] - A[i + m]*B[i];
    }
}