// RunMax.c
// Author: Jan Simon, Heidelberg, (C) 2016
// License: BSD (use/copy/change/redistribute on own risk, mention the author)

#include "mex.h"

//#define _DEBUG_INFO_ 1
#define MAX_SIZE 100000

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  mwSize m, n, c, i;
  double *a, *b, aa,
         *p0, *p, *q0, *q, *out;
  int idx;

    // Reject bad input:
    if (nrhs != 1 || !mxIsDouble(prhs[0]) || mxGetN(prhs[0]) != 2) {
       mexErrMsgIdAndTxt("JSimon:RunMax:BadInput",
                         "RunMax.mex: Inputs must be a double matrix.");
    }
    // Input:
    m = MAX_SIZE;
    n = mxGetM(prhs[0]);
    a = mxGetPr(prhs[0]);
    b = a + n;
    // Early return for empty input:
    if (n == 0) {
       plhs[0] = mxCreateDoubleMatrix(0, 2, mxREAL);
       return;
    }
    // Collect values:
    p0 = mxCalloc(m, sizeof(double));
    //q0 = malloc(n * sizeof(double));
    p  = p0;
    //q  = q0;
    // First element:
    aa = a[0];
    //*p = 0;
    //*q = b[0];
    // Loop over elements:
    for (i = 0; i < n; i++) {
        idx = (int)a[i] - 1;
        p[idx] += b[i];
        if (a[i] > aa) {    // Repeated element in column 1
            aa = a[i];
        }
#ifdef _DEBUG_INFO_
        mexPrintf("idx = %d \n", idx);
        mexPrintf("aa = %f \n", aa);
        mexPrintf("b[i] = %f \n", b[i]);
#endif
    }
    // Create output:
    c       = (mwSize)aa;  // Number of rows in output
#ifdef _DEBUG_INFO_
    mexPrintf("c = %d \n", c);
#endif
    plhs[0] = mxCreateDoubleMatrix(c, 1, mxREAL);
    out     = mxGetPr(plhs[0]);
    memcpy(out,     p0, c * sizeof(double));
//    memcpy(out + c, q0, c * sizeof(double));
    // Free temporary memory:
    mxFree(p0);
    //free(q0);
    return;
  }
