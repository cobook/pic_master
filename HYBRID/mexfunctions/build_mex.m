mex -O src/mex_MOVER.c
mex -O src/mex_GMOVER.c
mex -O src/mex_dep2nodes.c
mex -O src/mex_crossprod.c
mex -O src/mex_CALC_EF.c
mex -O src/mex_accumarray.c

%% OMP funcs
mex -O src/mex_bil_weight_part_OMP.c CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp"
%mex -O src/mex_bil_weight_part_OMP_temp.c CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp"
mex -O src/mex_accumarray_OMP.c CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp"
mex -O src/mex_flux_weight_OMP.c  CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp"
mex -O src/mex_flux_weight_OMP2.c  CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp"

mex -O src/mex_CALC_EF_OMP.c CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp"
mex -O src/mex_MOVER_OMP.c CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp"
