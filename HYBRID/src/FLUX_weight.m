function [J_filtered, n_filtered, vel_fil] = FLUX_weight( GPUFLAG, BOUNDARY, nz, nr, part, node_volume, spwt)
% FLUX WEIGHTING
% linear weighting in z direction
% bilinear weighting in r direction
% l: left, r: right, t: top, b: bottom

% after calculating raw value of number density,
% apply filter so that the result doesn't diverge

% boundaries are expanded one line for each side with the value of side
% line

% weight parameters are decided according to the equation
%                    M*phi(center) + 4S*phi(side) + 4K*phi(corner)
% phi(filtered) = -------------------------------------------------
%                                M+4S+4K

GPUFLAG = 0; % Set 0 because GPU resource limited when running in parallel

%%{
% Digital filter
% Weight parameters

% Weight parameters
M = 30; % Main
S = 4;  % Side
K = 1;  % Corner

denom = M+4*S+4*K;


H = 1/denom* ...
    [K, S, K;
    S, M, S;
    K, S, K];
%}
%{
% Gaussian filter
H = fspecial('gaussian', [3 3], 0.8);
%}

% The domain length is larger than magnetic field by 1
nz = nz + 1;
nr = nr + 1;
% reduce character number
np = part.np;

%% Weighting functions
% weight cylindrically ( bilinearly )
lb = part.fz0r0 .* spwt;
rb = part.fz1r0 .* spwt;
lt = part.fz0r1 .* spwt;
rt = part.fz1r1 .* spwt;

switch 'mex2'
    case 'pre'
        
        % Import particle position information
        i = part.d_i(1:np);
        j = part.d_j(1:np);
        
        %% cell nodes set for using accumarray
        cell_idx = [i+nz*(j-1); i+1+nz*(j-1);  i+nz*j;  i+1+nz*j];
        cell_weight = [ lb;
            rb;
            lt;
            rt];
        
        
        vel = part.v(1:np,:);
        % tic
        
        switch 'mex'
            case 'pre'
                

                
                %% Utilize MEX Function
                % Input  : part.v, cell_weight, cell_idx, nz, nr, np
                % Output : flux, chg
                chg = accumarray(cell_idx,cell_weight,[nz*nr, 1], [], 0);
                accum_idx = [cell_idx, cell_idx+nz*nr, cell_idx+2*nz*nr];
                %flux_weight = horzcat(cell_weight, cell_weight, cell_weight) .* vertcat(part.v(1:np,:), part.v(1:np,:), part.v(1:np,:), part.v(1:np,:));%repmat too slow
                flux_weight = repmat(cell_weight,[1 3]).*repmat(vel,[4 1]);
                flux = accumarray(accum_idx(:), flux_weight(:), [nz*nr*3, 1], [], 0);
                
            case 'mex'
                [chg, flux] = mex_flux_weight_OMP(cell_idx(:), cell_weight(:), vel(:), nz, nr, np);
        end
        
    case 'mex2'
        %% Weighting functions
        % weight cylindrically ( bilinearly )
        lb = part.fz0r0 .* spwt;
        rb = part.fz1r0 .* spwt;
        lt = part.fz0r1 .* spwt;
        rt = part.fz1r1 .* spwt;
        [chg, flux] = mex_flux_weight_OMP2(part.d_i,part.d_j,part.v,nz,nr,np,lb,rb,lt,rt);       
end


chg = reshape(chg, [nz, nr] );
flux = reshape(flux, [nz, nr, 3]);

%% Node expansion for the ease of filtering
[chg, flux] = copy_cells(BOUNDARY, chg, flux);

n = eval_number_density( chg, node_volume );
J = eval_current_density( flux, node_volume, part.QE );

%% Break if you don't apply any filter
if (S==0 && K==0)
    n_filtered = n;
    J_filtered = J;
    vel_fil = J_filtered./repmat(n_filtered,[1, 1, 3])./part.QE;
    return
end

%% GPU utilization configuration 
if (GPUFLAG>=1)
    gpu = gpuDevice;
    rows = gpuArray.colon(1,nz)';
    cols = gpuArray.colon(1,nr);
    vrows = gpuArray.colon(1,nz)';
    vcols = gpuArray.colon(1,nr);    
    dims = reshape(gpuArray.colon(1,3),[1 1 3]);
    %vel = gpuArray(vel);
    n = gpuArray(n); 
    J = gpuArray(J);
else
    [cols, rows] = meshgrid(1:nr, 1:nz);
    [vcols, vrows, dims] = meshgrid(1:nr, 1:nz, 1:3);
end

%% stencil calculation for arrayfunction
% thse functions needs to be declared inside function

function out = J_filter(row, col, dim)
    if (row~=1 && row~=nz && col~=1 && col~=nr)
        % name index value
        up = row-1;
        down = row+1;
        left = col-1;
        right = col+1;
        % Side components
        rowsum = J(up,col,dim) + J(down,col,dim);
        colsum = J(row,left,dim) + J(row,right,dim);
        % Corner components
        cornersum = J(up,left,dim) + J(up,right,dim) ...
            + J(down,left,dim) + J(down,right,dim);

        out = (J(row,col,dim)*M + (rowsum + colsum)*S + cornersum*K)/denom;
    else
        out = NaN;
    end
end

function out = n_filter(row,col)
    if (row~=1 && row~=nz && col~=1 && col~=nr)
        % name index value
        up = row-1;
        down = row+1;
        left = col-1;
        right = col+1;
        % Side components
        rowsum = n(up,col) + n(down,col);
        colsum = n(row,left) + n(row,right);
        % Corner components
        cornersum = n(up,left) + n(up,right) ...
            + n(down,left) + n(down,right);

        out = (n(row,col)*M + (rowsum + colsum)*S + cornersum*K)/denom;
    else
        out = NaN;
    end
end

%% Thiese lines corresponds to the zeroing of derivative function
% on the axis for Fourier series expansion
n(:,2) = (n(:,3)*1.5 - n(:,4)*0.5);
n(n(:,2)<0,2) = 0;
% Just the same as n
J(:,2,1) = (J(:,3,1)*1.5 - J(:,4,1)*0.5);
% Flux weighted current in r and theta
J(:,2,2:3) = 0.3333*J(:,3,2:3);
% averaging 
n(2:end-1,2) = 1/3*(n(2:end-1,2) + n(1:end-2,2) + n(3:end,2));
J(2:end-1,2,:) = 1/3 * (J(2:end-1,2,:) + J(1:end-2,2,:) + J(3:end,2,:));

%% Filter chg and flux 
n_filtered = zeros(size(n));
n_filtered(:,2:end) = imfilter(n(:,2:end), H, 'symmetric');
if(GPUFLAG>=1)
    wait(gpu);
    n_filtered = gather(n_filtered);
end

%J_filtered = arrayfun(@J_filter, vrows, vcols, dims);
J_filtered = zeros(size(J));
J_filtered(:,2:end,:) = imfilter(J(:,2:end,:),H, 'symmetric');
if(GPUFLAG>=1)
    wait(gpu);
    J_filtered = gather(J_filtered);
end

%% Convert to number density and current
% Avoid singular point
[n_filtered, J_filtered] = copy_cells(BOUNDARY, n_filtered, J_filtered);

%% Velocity should be derived using number density and current
vel_fil = J_filtered./repmat(n_filtered,[1, 1, 3])./part.QE;
vel_fil(isnan(vel_fil)) = 0;
vel_fil(~isfinite(vel_fil)) = 0;
end

function n_ion = eval_number_density( chg,node_volume)
%EVAL_NUMBER_DENSITY
% evaluate the value of number density
    n_ion = chg./node_volume;
end

function J_ion = eval_current_density( flux,node_volume, Ze)
%EVAL_CURRENT_DENSITY
% evaluate the value of number density
    J_ion = Ze*flux./repmat(node_volume, [1,1,3]);
    
end
function [n,J] =  copy_cells(BOUNDARY, n, J)
if strcmp(BOUNDARY, 'a')
    % Copy cells assuming neumann boundary
    n(:,1) = n(:,2);
    n(:,end) = n(:,end-1);
    n(1,:) = n(2,:);
    n(end,:) = n(end-1,:);
    
    J(:,1,1) =  J(:,2,1);
    J(:,1,2:3) = -J(:,2,2:3);
    J(:,end,:) =  J(:,end-1,:);
    
    %% side walls
    J(1,:,1) = - J(2,:,1);
    J(1,:,2:3) = J(2,:,2:3);
    J(end,:,1) = - J(end-1,:,1);
    J(end,:,2:3) = J(end-1,:,2:3);
elseif strcmp(BOUNDARY, 'p')
    if isnan(n(1,1))
        % Copy cells assuming periodic boundary
        n(:,1) = n(:,2);
        n(:,end) = n(:,end-1);
        n(1,:) = n(end-1,:);
        n(end,:) = n(2,:);
        
        J(:,1,1) =  J(:,2,1);
        J(:,1,2) = -J(:,2,2);
        J(:,1,3) = -J(:,2,3);
        J(:,end,:) = J(:,end-1,:);
        J(1,:,:) = J(end-1,:,:);
        J(end,:,:) = J(2,:,:);
    else
        % Copy cells assuming periodic boundary
        n(:,1) = n(:,2);
        n(:,end) = n(:,end-1);
        n(2,:) = n(2,:) + n(end,:);
        n(end-1,:) = n(end-1,:) + n(1,:);
        n(1,:) = n(end-1,:);
        n(end,:) = n(2,:);
        
        J(:,1,1) =  J(:,2,1);
        J(:,1,2:3) = -J(:,2,2:3);
        J(:,end,:) = J(:,end-1,:);
        J(2,:,:) = J(2,:,:) + J(end,:,:);
        J(end-1,:,:) = J(end-1,:,:) + J(1,:,:);
        J(1,:,:) = J(end-1,:,:);
        J(end,:,:) = J(2,:,:);
    end
end
end
