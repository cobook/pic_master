function [EF, MF] = bil_Part_Weight(part, ef, mf)
%% Refer to "Plasma Physics via computer simulation, pp 336
np = part.np;
i = part.d_i(1:np);
j = part.d_j(1:np);

%% Bilinear information
fz0r0 = part.fz0r0(1:np);
fz0r1 = part.fz0r1(1:np);
fz1r0 = part.fz1r0(1:np);
fz1r1 = part.fz1r1(1:np);
%% Use child functions
ef_temp = ef.fetch();
ef_temp(:,1,2:3) = 0;
mf_temp = mf.exp_interp();
mf_temp(:,1,2:3) = 0;
    function out = gpu_child(field)
        function out = field_weight(row, dim)
            out = field(i(row),j(row),dim)* fz0r0(row) + field(i(row),j(row)+1,dim) * fz0r1(row) + field(i(row)+1,j(row),dim) * fz1r0(row) + field(i(row)+1,j(row)+1,dim) * fz1r1(row);
        end
        weightFcn = @field_weight;
        %rows = 1:np;
        rows = gpuArray.colon(1,np)';
        dims = gpuArray.colon(1,3);
        out_gpu = arrayfun(weightFcn, rows, dims);
        out = gather(out_gpu);
    end

    function out = cpu_child(field)
        [nz, nr, nt] = size(field);
        nn = nz*nr;
        field_z0r0 = field(i   + (j-1)   *nz + nn*[0,1,2]);
        field_z0r1 = field(i   + (j)     *nz + nn*[0,1,2]);
        field_z1r0 = field(i+1 + (j-1)   *nz + nn*[0,1,2]);
        field_z1r1 = field(i+1 + (j)     *nz + nn*[0,1,2]);
        out = field_z0r0.*fz0r0_rep ...
            + field_z0r1.*fz0r1_rep ...
            + field_z1r0.*fz1r0_rep ...
            + field_z1r1.*fz1r1_rep;
    end

    function out = mex_child(field)
        [nz, nr, nt] = size(field);
        out = mex_bil_weight_part_OMP(field, fz0r0, fz0r1, fz1r0, fz1r1, i-1, j-1, nz, nr, np);
    end

GPUFLAG=0;
MEXFLAG=1;
if GPUFLAG==1
    gpu = gpuDevice;
    EF_gpu = gpu_child(ef_temp);
    MF_gpu = gpu_child(mf_temp);
    
    EF = gather(EF_gpu);
    MF = gather(MF_gpu);
    
    wait(gpu);
elseif MEXFLAG==1
    EF = mex_child(ef_temp);
    MF = mex_child(mf_temp);
else
    
    fz0r0_rep = repmat(fz0r0, [1,3]);
    fz0r1_rep = repmat(fz0r1, [1,3]);
    fz1r0_rep = repmat(fz1r0, [1,3]);
    fz1r1_rep = repmat(fz1r1, [1,3]);
    EF = cpu_child(ef_temp);
    MF = cpu_child(mf_temp);
end


end
        


            