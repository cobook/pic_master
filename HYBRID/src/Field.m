classdef Field < handle
    % Parent class of field operations 
    properties (SetAccess=protected, GetAccess=public)
        z;
        r;
        theta;
        nz0;
        nr0;
        nz1;
        nr1;
        nz2;
        nr2;
        dz;
        dr;
        GPU;
        gr;
        gz;
        filter;
        coil_cell;
        coil_mask;
        wall_cell;
        wall_mask;
        type;
        box_weight1;
        box_weight0;
        
        % Each corresponds to
        gr2;   % Expanded magnetic field
        gr1;   % Electric field half grid
        gr0;   % particle boundary
        % Corresponding weights for interpolation calculation
        w2;
        w1;
        w0;
    end
    
    methods
               
        function field = Field(nz, nr, dz, dr, initial)
            % initialization
            if nargin == 4
                initial = 0;
                field.z = initial*zeros(nz,nr);
                field.r = initial*zeros(nz,nr);
                field.theta = initial*zeros(nz,nr);
                field.dz = dz;
                field.dr = dr;
                field.GPU = 0;
            elseif nargin == 5 
                initial = varargin{5};
                field.z = initial*zeros(nz,nr);
                field.r = initial*zeros(nz,nr);
                field.theta = initial*zeros(nz,nr);
                field.dz = dz;
                field.dr = dr;
                field.GPU = 0;
            end
        end
        
        function set_coil_cell(self, value, nz, nr)
            % Set the cells value of coils
            % The particles shouldn't be accelerated to fiercely near the coil
            self.coil_cell = value;
            self.coil_mask = self.generate_mask(self.coil_cell, nz, nr);
        end
        
        function set_box_weight(self, cells)
            % Decide gains around boundaries
            gap = 0; % gap which seems to be on the box cells
            factor = 1.5; % decide the slope of tanh
            
            zp = (- tanh( ([1:self.nz1]' - (cells(1,2)) -1 )*factor -gap ) + 1) / 2;
            zm = (  tanh( ([1:self.nz1]' - (cells(1,1)) +1 )*factor +gap ) + 1) / 2;
            rp = (- tanh( ([1:self.nr1]  - (cells(2,2)) -1 )*factor -gap ) + 1) / 2;
            rm = (  tanh( ([1:self.nr1]  - (cells(2,1)) +1 )*factor +gap ) + 1) / 2;
            mask = 1 - zp.*zm.*rp.*rm;            
            mask = mask.*self.wall_mask;
            
            
            self.box_weight1 = repmat(mask,[1 1 3]);
            self.box_weight0 = 0.25*(self.box_weight1(1:end-1,1:end-1,:) + self.box_weight1(2:end,1:end-1,:) ...
                +self.box_weight1(1:end-1,2:end,:) + self.box_weight1(2:end,2:end,:));          
        end        
        
        function set_wall_cell(self,value, nz, nr)
            % Set the cells of
            % The particles shouldn't be accelerated to the z direction by unrealistic large
            % field
            wall_pos = value;
            self.wall_cell = [1, floor(wall_pos/self.dr)+1; 1, nr];
            self.wall_mask = self.generate_mask(self.wall_cell, nz, nr);
        end
        
        function mask = generate_mask(self, cell, nz, nr)
            mask = ones(nz,nr);
            [sz, sr] = size(self.gr1);
            minz = max(1,cell(1,1));
            maxz = min(sz,cell(1,2));
            minr = max(1,cell(2,1));
            maxr = min(sr,cell(2,2));
            mask(minz:maxz,minr:maxr) = 0;
        end
        
        function set_z(self, value)
            self.z = value;
        end
        
        function set_r(self, value)
            self.r = value;
        end
        
        function set_theta(self, value)
            self.theta = value;
        end
            
        function interpolated = linear_interpolate2d(self, fi, fj)  
            % linear weighting in z and r
           interpolated_z = interp2(self.z, fj, fi);
           interpolated_r = interp2(self.r, fj, fi);
           interpolated = [interpolated_z, interpolated_r];
        end

            
        function [EF, rstyJ] = linear_interpolate3d_ef(self, fi, fj, i, j)
            % linear weighting to z, r and theta          
            fjr = (fj-1.5);         
            fjr(fjr<0.5) = 0.5; 
            
            
           ez = self.z;
           er = self.r;
           et = self.theta;            
            
            GPUtemp = self.GPU;
            GPUtemp = 0;
           if GPUtemp ==1
               fj = gpuArray(fj);
               fi = gpuArray(fi);
               %flag = gpuArray(flag);
           end

           % temporal Boundaries
           er(:,1) = er(:,2);
           et(:,1) = et(:,2);
           
            % Perform interpolation depending on the particle position
            interpolated_z = interp2(ez, fj, fi);
            interpolated_r = interp2(er.*self.gr1, fj, fi)./fjr;
            interpolated_t = interp2(et.*self.gr1, fj, fi)./fjr;

            
            EF = [interpolated_z, interpolated_r, interpolated_t];
           
           if GPUtemp == 1
               EF = gather(EF);
           end

           rstyJ = zeros(size(fj));
           
        end
        
        function set_cyl_weight(self)
            self.w2 = cat(3, ones(self.nz2,self.nr2), self.gr2, self.gr2);
            self.w1 = cat(3, ones(self.nz1, self.nr1), self.gr1, self.gr1); 
            self.w0 = cat(3, ones(self.nz0, self.nr0), self.gr0, self.gr0);
        end
        
        function out = cyl_interpolate1to0(self, x)
            A = x.*self.w1;
            temp = 0.25*(A(1:end-1,1:end-1,:) + A(1:end-1,2:end,:) + A(2:end,1:end-1,:) + A(2:end,2:end,:));
            out = temp./self.w0;
            out(:,1,:) = out(:,2,:);
        end
        
        function out = cyl_interpolate0to2(self, x)
            out = padarray(x, [1 1 0], 'symmetric');
            out(:,1,2:3) = out(:,3,2:3);           
        end
        
        function out = cyl_interpolate2to1(self, x)
            A = x.*self.w2;
            temp = 0.25*(A(1:end-1,1:end-1,:) + A(1:end-1,2:end,:) + A(2:end,1:end-1,:) + A(2:end,2:end,:));
            out = temp./self.w1;
        end
        
        
        function MF = linear_interpolate3d_mf(self, fi, fj)


            
            %% prepare weighting on the axis
            fjr = (fj-1.5);                                  
            fjr(fjr<0.5) = 0.5;             
            %% linear weighting to z, r and theta
            %GPUtemp = self.GPU;
            GPUtemp = 0;
           if GPUtemp ==1
               gpu = gpuDevice;
               fj = gpuArray(fj);
               fi = gpuArray(fi);
           end
           
           %{
           
            %% Preapre interpolation function 
            function out = lin_interpolate(X)
                A = padarray(X, [1 1], 'both');
                out = 0.25*(A(1:end-1,1:end-1,:) + A(1:end-1,2:end,:) + A(2:end,1:end-1,:) + A(2:end,2:end,:));
                out(:,1) = out(:,2);
            end
                       
            mz = lin_interpolate(self.z);
            mr = self.cyl_interpolate0to1(self.r);
            mt = self.cyl_interpolate0to1(self.theta);


           % temporal Boundaries
           mr(:,1) = mr(:,2);
           mt(:,1) = mt(:,2);
           %}
           
           
           mf_temp2 = self.cyl_interpolate0to2(mf.fetch());
           mf_temp1 = self.cyl_interpolate2to1(mf_temp2);
           mz = mf_temp1(:,:,1);
           mr = mf_temp1(:,:,2);
           mt = mf_temp1(:,:,3);
            
            
            interpolated_z = interp2(mz, fj, fi);
            interpolated_r = interp2(mr.*self.gr1, fj, fi)./fjr;
            interpolated_t = interp2(mt.*self.gr1, fj, fi)./fjr;
            
            MF = [interpolated_z, interpolated_r interpolated_t];

            
            if GPUtemp == 1
                wait(gpu);
                MF = gather(MF);
            end
            
        end
        
        function interpolated = bilinear_weight3d(self, i, j, hz, hr)
            % 2d bilinear weighting cylindrical in r direction
            high_z = self.z(i,j+1)*(1-hz) + self.z(i+1,j+1)*hz;
            low_z = self.z(i,j)*(1-hz) + self.z(i+1,j)*hz;
            bz = ((j^2 - (j -1 + hr)^2)*low_z + ((j - 1 + hr)^2 - (j-1)^2)*high_z) / (j^2 - (j-1)^2);
            high_r = self.r(i,j+1)*(1-hz) + self.r(i+1,j+1)*hz;
            low_r = self.r(i,j)*(1-hz) + self.r(i+1,j)*hz;
            br = ((j^2 - (j -1 + hr)^2)*low_r + ((j - 1 + hr)^2 - (j-1)^2)*high_r) / (j^2 - (j-1)^2);
            high_theta = self.theta(i,j+1)*(1-hz) + self.theta(i+1,j+1)*hz;
            low_theta = self.theta(i,j)*(1-hz) + self.theta(i+1,j)*hz;
            btheta = ((j^2 - (j -1 + hr)^2)*low_theta + ((j - 1 + hr)^2 - (j-1)^2)*high_theta) / (j^2 - (j-1)^2);
            interpolated = [bz, br, btheta];
        end
        
        function field3d = fetch(self)
            % assemble field value for the use of cross product
            field3d = cat(3,self.z, self.r, self.theta);
        end
  

        function set_GPU(self, flag)
            % Set private variable GPU
            self.GPU = flag;
        end
        
        %% Renew identical field intensity  
        function register(self, field)
           self.z = field(:,:,1);
           self.r = field(:,:,2);
           self.theta = field(:,:,3);
        end

    end
    
end

