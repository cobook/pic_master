classdef MagneticField < Field
    % Magnetic field calculation and operation    
    properties
        normal; % Normalization parameter sets 
        B_REF;  % Reference value for Alfven wave propagation
        A;      % Matrix A for modifying divergence_Bp
        BOUNDARY; % Boundari condition
        debug;  % Debugger flag
        z1;     % z component of G1
        r1;     % r component of G1
        t1;     % theta component of G1
    end
    
    methods
        
        function mf = MagneticField(nz0,nr0,dz,dr,normal,reference,BOUNDARY)
            % Initialization using Field class
            mf = mf@Field(nz0,nr0,dz,dr);
            mf.nz0 = nz0;
            mf.nr0 = nr0;
            mf.nz1 = nz0+1;
            mf.nr1 = nr0+1;
            mf.nz2 = nz0+2;
            mf.nr2 = nr0+2;

            
            mf.normal = normal;
            mf.B_REF = reference;   
            mf.BOUNDARY = BOUNDARY;
            mf.gr2 = abs(meshgrid(1:mf.nr2,1:mf.nz2)-2.);
            mf.gr1 = abs(meshgrid(1:mf.nr1,1:mf.nz1)-1.5);
            mf.gr0 = abs(meshgrid(1:mf.nr0,1:mf.nz0)-1);    
            mf.set_cyl_weight();            
            mf.wall_mask = ones(mf.nz1,mf.nr1);           
            mf.debug = 0;
            if mf.debug==1
                disp('MF Debug on');
            end
            mf.set_box_weight([0 0; 0 0]);
        end                    
        
        %% hybrid code
        function [ curlBp, Bstar, Bnumber, number, Berror ] = CL(self, ef, number, MU0, B_ext, J_ion, kTe, n_ion, part, dt)
            % cyclic leapfrog method
            QE = part.QE;            
            B0 = self.fetch();                        
            ef.set_resistivity(n_ion);  
            
            [mgf, elec, rstyJ, curlBp] = mex_CALC_EF(MU0, B0, B_ext, J_ion, kTe, n_ion, QE, self.dz, self.dr, ef.r_coeff, ef.r_rsty0, ef.r_sigma, ef.n_thre, dt, number, ef.rsty, self.box_weight0, self.box_weight1, self.wall_cell-1, self.coil_cell-1);
            
            if isnan(sum(mgf(:)))
                disp('Error : MF contains Nan value');
                mgf(isnan(mgf)) = 0;
            end
            if ~isfinite(sum(mgf(:)))
                disp('Error : MF contains Inf value');
                mgf(~isfinite(mgf)) = 0;
            end
            % Filter the result of magnetic field value for the better
            % stability of the code
            mgf_fil = field_filter(self.GPU, self.nz1, self.nr1, (mgf - B_ext).*self.box_weight0) + B_ext;

            %% Register resistivity value
            ef.register_rstyJ(rstyJ);

            %% Save the output to the field class
            % Make sure axis values for singurarity
            % r component and theta component are 0 on the axis
            % (explicitly declare here just in case)
            mgf_fil(:,1,2:3) = 0;            
            self.register(mgf_fil);
        end

        
        
        function [Berror, number, flag] = errorcheck(self, Bstar, Bnumber, flag, dt, number, n_ion, part, MU0)
                % Compute error and determine if subcyling time should be
                % shorten or not
                Berror = abs(max(Bstar(:)-Bnumber(:)));

                if Berror > 1e-4
                    Bnorm_sqnion = (self.B_REF/self.normal.B0)./sqrt(n_ion);
                    Bnorm_sqnion(isinf(Bnorm_sqnion)) = 0;
                    VAtemp = max(Bnorm_sqnion(:))/sqrt(part.M*MU0);
                    if VAtemp * dt/number > min(self.dz,self.dr)
                        CFL_number = 0.5;
                        number = 4 +   floor(floor(dt/min(self.dz,self.dr)*VAtemp/CFL_number));
                        disp('iteration number modified');
                        disp(strcat('Number=', num2str(number)));
                    else                 
                        disp('iteration number unchanged');
                        disp('Electric field itself may have problem');
                        flag = 0;
                    end
                    if number > 1000
                        disp('CL_iteration too large');
                    end
                else
                    flag = 0;
                end
                
                if Berror < 1e-5
                    number = 4;
                end
        end
        
        function out = half_expand(self)
            % prepare function for yielding geometry out of original one.
            X = padarray(self.fetch(),[1 1 0],'replicate','both');
            if (ndims(X)==2)
                X(:,1) = X(:,3);
            elseif (ndims(X)==3)
                % Boundary condition on the axis
                X(:,1,1) = X(:,3,1);    % z direction on the axis
                X(:,1,2:3) = -X(:,3,2:3);   % r,theta direction on the axis
            end
            % prepare function for interpolating the center of the grid
            % value
            out = 0.25*(X(1:end-1,1:end-1,:) + X(1:end-1,2:end,:) + X(2:end,1:end-1,:) + X(2:end,2:end,:));
        end
        
  
        function [Bout0, divBpre, divBout] = satisfy_divB(self, mgf, iter)
            % Weighted flux average
            %B_temp = self.cyl_interpolate0to2(mgf);            
            %Bpre = self.cyl_interpolate2to1(B_temp);
            % Calculate divergence
            divBpre = self.DIV_RZ(mgf);                   
            divBout = divBpre;
            Bout0 = mgf;
            for i = 1:iter
                % Lax-Wendroff scheme
                % Project the difference 
                 
                [grad_r, grad_z] = self.B_project(divBout);
                Bout0 = Bout0 - cat(3, grad_z, grad_r, zeros(self.nz0, self.nr0));
                % Calculate divergence                               
                divBout = self.DIV_RZ(Bout0);
            end
            

        end
       
        
        
        function divB = DIV_RZ(self, B_pre)
            % Divergence of B in cylindrical coordinate
            % r direction should be the gradient of weighted flux
            [rArdr, rArdz] = gradient(self.gr0.*B_pre(:,:,2), self.dr, self.dz);
            [Ardz, Azdz] = gradient(B_pre(:,:,1), self.dr, self.dz);
            divB = 1./self.gr0 .* (rArdr + self.gr0.*Azdz);
            divB(1,:) = 0;
            divB(end,:) = 0;
            divB(:,1) = 0;
        end
        
        function [grad_r, grad_z] = B_project(self, divB)
            % calculate gradient needed for div=0 modification
            % up wind applied after calculating central difference
            if ~sum(self.A(:))
                self.set_A();
            end
            phi = zeros(self.nz0,self.nr0);                
            phi = eval_Bp_SOR(phi,self.A,divB,self.nz0,self.nr0);
            [grad_r, grad_z] = self.difference4th(phi);
        end       
        
        
        function [gr, gz] = difference4th(self, phi)
            %% gradient by 4th-order differentiation
            phir = padarray(phi, [0 2], 'replicate');
            gr = (-phir(:,5:end)+8*phir(:,4:end-1)-8*phir(:,2:end-3)+phir(:,1:end-4))/(12*self.dr);
            phiz = padarray(phi, [2 0], 'replicate');  
            gr(:,1) = 0;
            gz = (-phiz(5:end,:)+8*phiz(4:end-1,:)-8*phiz(2:end-3,:)+phiz(1:end-4,:))/(12*self.dz);            
        end 
                
        function A = set_A(self)
                %set up multiplication matrix for potential solver
                %here we are setting up the Finite Difference stencil
                A = zeros(self.nz0*self.nr0);              %allocate empty nn * nn matrix
                dr = self.dr;
                dz = self.dz;
                nz0 = self.nz0;
                nr0 = self.nr0;
                
                %set regular stencil on internal nodes
                for j=2:nr0-1                    %only internal nodes
                    for i=2:nz0-1
                        u = (j-1)*nz0+i;         %unknown (row index)
                        r = (dr*abs(j-1.0));
                        A(u,u) = -2*(1/(dz*dz)+1/(dr*dr)) - 1./(r.*r);    %phi(i,j)
                        A(u,u-1)=1/(dz*dz);     %phi(i-1,j)
                        A(u,u+1)=1/(dz*dz);     %phi(i+1,j)
                        A(u,u-nz0)=1/(dr*dr)-1/(r*2*dr);    %phi(i,j-1)
                        A(u,u+nz0)=1/(dr*dr)+1/(r*2*dr);    %phi(i,j+1)
                    end
                end   
                
                %%Neumann boundary on r=Lr
                for i=1+1:nz0-1
                    u=(nr0-1)*nz0+i;
                    A(u,u-nz0) = 1/dr;            %phi(i,j-1)
                    A(u,u) = -1/dr;              %phi(i,j)
                end                
                %neumann boundary on z=Lz
                for j=1:nr0
                    u=(j-1)*nz0+nz0;
                    A(u,u-1) = 1/dz;             %phi(i-1,j)
                    A(u,u) = -1/dz;             %phi(i,j)
                end                
                
                % neumann boundary on z=0
                for j=1:nr0
                    u=(j-1)*nz0+1;
                    A(u,u+1) = 1/dz;            %phi(i+1,j)
                    A(u,u) = -1/dz;
                end         
                     
                % neumann boundary on r=0
                for i=2:nz0-1
                    u=(1-1)*nz0+i;
                    A(u,u+nz0) = 1/dr;             %phi(i,j+1)
                    A(u,u) = -1/dr;            %phi(i,j)
                end         
            
                
                                                                                                         
                %% Boundary conditions matrix
                box_wall = zeros(2,2,4);
                box_wall(:,:,1) = [1 nz0; 1 1];
                box_wall(:,:,2) = [1 1; 1 nr0];
                box_wall(:,:,3) = [nz0 nz0; 1 nr0];    
                box_wall(:,:,4) = [1 nz0; nr0 nr0];
                % Set phi=0 on boundaries
                for k = [4]
                    for j=box_wall(2,1,k):box_wall(2,2,k)
                        for i=box_wall(1,1,k):box_wall(1,2,k)
                            u=(j-1)*nz0+i;
                            A(u,:)=zeros(1,nz0*nr0);     %clear row
                            A(u,u)=1;               %phi(i,j)
                        end
                    end
                end                             
                
                A = sparse(A);
                self.A = A;
        end
        
        
        function curlA = cylindrical_ROT(self, A, TYPE)
            % calculate curlA from electric field A
            % no changes in theta direction            
            curlA = zeros(self.nz0,self.nr0,3);            
            switch TYPE
                    
                case 'ROT_E' 
                    % Apply rotation operator to electric field
                    % z direction
                    rAtheta = (self.gr1).*A(:,:,3);         % r*Atheta
                    temp = ( rAtheta(:,2:end) - rAtheta(:,1:end-1) )/self.dr;
                    drAtheta_dr = (temp(1:end-1,:) + temp(2:end,:))/2;
                    curlA(:,:,1) = drAtheta_dr./self.gr0;
                    % Note on the axis, following formula should be applied
                    % Bz(n+3/2) = Bz(n+1/2) - dt*(2/dr)*2*E\theta
                    curlA(:,1,1) = 4/self.dr*(A(1:end-1,2,3)+A(2:end,2,3))/2;
                    % r direction
                    temp = (A(2:end,:,3) - A(1:end-1,:,3))/self.dz;
                    dAtheta_dz = (temp(:,2:end) + temp(:,1:end-1))/2;
                    curlA(:,:,2) = -dAtheta_dz;
                    % theta direction
                    temp = (A(2:end,:,2) - A(1:end-1,:,2))/self.dz;
                    dAr_dz = (temp(:,1:end-1) + temp(:,2:end))/2;
                    temp = (A(:,2:end,1) - A(:,1:end-1,1))/self.dr;
                    dAz_dr = (temp(1:end-1,:) + temp(2:end,:))/2;
                    curlA(:,:,3) = dAr_dz - dAz_dr;          
                    
                    %% Boundary condition
                    curlA(:,1,2:3) = 0;
                    
                    
                otherwise
                    % General case, 
                    % Don't use for calculation
                    % z direction
                    [drAtheta_dr, drAtheta_dz] = gradient((self.gr-1).*A(:,:,3),self.dr,self.dz);
                    curlA(:,:,1) = drAtheta_dr./(self.gr-1);
                    curlA(:,1,1) = curlA(:,2,1);
                    % r direction
                    [dAtheta_dr, dAtheta_dz] = gradient(A(:,:,3), self.dr, self.dz);
                    curlA(:,:,2) = -dAtheta_dz;
                    % theta direction
                    [dAr_dr, dAr_dz] = gradient(A(:,:,2),self.dr,self.dz);
                    [dAz_dr, dAz_dz] = gradient(A(:,:,1),self.dr,self.dz);
                    curlA(:,:,3) = dAr_dz - dAz_dr;
                    
            end           
            
            if any(isnan(curlA(:)))
                disp('MagneticField.m cylindrical_ROT');
                disp(strcat('Error_in_', TYPE));                
            end
        end
        
        function out = exp_interp(self)
            % Combined function of expansion and interpolation. 
            temp = padarray(self.fetch(), [1 1 0], 'symmetric');
            temp = temp.*self.gr2;
            out = 0.25*(temp(1:end-1,1:end-1,:) + temp(1:end-1,2:end,:) + temp(2:end,1:end-1,:) + temp(2:end,2:end,:));
            out = out./self.gr1;
            out(:,1,:) = out(:,2,:);
            self.z1 = out(:,:,1);
            self.r1 = out(:,:,2);
            self.t1 = out(:,:,3);
        end
    end
    
end

