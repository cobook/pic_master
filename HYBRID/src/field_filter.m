function filtered = field_filter( GPUFLAG, nz1, nr1, field)
% FLUX WEIGHTING
% linear weighting in z direction
% bilinear weighting in r direction
% l: left, r: right, t: top, b: bottom

% after calculating raw value of number density,
% apply filter so that the result doesn't diverge

% boundaries are expanded one line for each side with the value of side
% line

% weight parameters are decided according to the equation
%                    M*phi(center) + 4S*phi(side) + 4K*phi(corner)
% phi(filtered) = -------------------------------------------------
%                                M+4S+4K

% Weight parameters
M = 30; % Main
S = 4;  % Side
K = 1;  % Corner



if (S==0 && K==0)
    filtered = field;
    return
end


denom = M+4*S+4*K;
H = 1/denom * [K, S, K;
    S, M, S;
    K, S, K];

fsize = size(field);
if fsize(1)==nz1 % Electric fild etc..
    gr = meshgrid(-1:2:2*(nr1-1),1:nz1);
    gr(:,end) = gr(:,end-1);
    gr(:,1) = gr(:,2);
    field(:,1,2:3) = - field(:,2,2:3);
elseif fsize(1)==nz1-1 % Magnetic field
    gr = meshgrid(0:(nr1-2),1:nz1-1);
    gr(:,end) = gr(:,end-1);
end

% linearly diminish near the axis
field(:,2,2:3) = 0.3333*field(:,3,2:3);
field(:,1,2:3) = field(:,2,2:3);

%{
field(:,:,2:3) = field(:,:,2:3).*repmat(gr,[1,1,2]);
%}

%% GPU utilization configuration
if (GPUFLAG>=1)
    gpu = gpuDevice;
    vrows = gpuArray.colon(1,nz1)';
    vcols = gpuArray.colon(1,nr1);
    dims = reshape(gpuArray.colon(1,3),[1 1 3]);
    field = gpuArray(field);
else
    [cols, rows] = meshgrid(1:nr1, 1:nz1);
    [vcols, vrows, dims] = meshgrid(1:nr1, 1:nz1, 1:3);
end

%E_filtered = arrayfun(@E_filter, vrows, vcols, dims);
filtered = imfilter(field, H, 'symmetric');

if(GPUFLAG>=1)
    wait(gpu);
    filtered = gather(filtered);
end
%{
%E_filtered = E_filtered./repmat(gr,[1,1,3]);
filtered(:,:,2:3) = filtered(:,:,2:3)./repmat(gr,[1,1,2]);
%}

if fsize(1)==nz1-1
    filtered(:,1,2:3) = 0;
end
end
