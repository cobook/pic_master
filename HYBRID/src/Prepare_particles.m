function Prepare_Particles
%This function prepares initial particle distributions on the simulation domain. 
% The parameters are designed so that they fulfill parameters of Sec.
% 4.3.2. 
% The algorithm in this source code just describes the steps of Sec. 4.7 of
% master thesis

if ~exist('Init_part_pos','dir')
    disp('You are not in the proper directory');
    return
end
%% Generate initial parameter from the experimental verified values
% vr : radial velcoity at each angle
% vtheta : angle

% Number of particles created
NUM = 10000000;
% num of grid to be separated
r_sample = 3000;
theta_sample = 1000;
kT0 = 55.8; % [eV]
kT_slope = -0.636;
%% Experimentally verified consts
ra = 1.9e-3; % reference veolocity
ta = 0.1e-6; % time of reference
vd0 = 5670; % drift velocity on the axis
vd_slope = 45.6; % gradient of velocity
M = 1.67e-27*63.5;
Q = 1.6e-19;
%% Prepare angle [rad] v.s. CDF in polynomial
% theta is specified in rad
%% Sampling variables
% theta
lt = 87/90; % Consider up to this angle
dtheta = pi/2*lt/theta_sample;
s_theta = 0:dtheta:pi/2*lt;
thetas = numel(s_theta);
%s_vd = vda-110*180/pi*s_theta;
s_kT = Q*(kT0+kT_slope.*s_theta*(180/pi));
s_sigma = sqrt(s_kT/M);
% velocity
ra2 = ra*2;
dr = ra2/r_sample;
s_r = 0:dr:ra2;
%% Monte carlo limitations
lim_alpha = 0.94;  % limit for monte carlo calculation. excessing value should be ignored
%theta_lim = [pi/2/theta_sample, pi/2*lim_alpha];
%vel_lim =  [max(vda-3*s_sigma(1), vda2*(1-lim_alpha)), min(vda+3*s_sigma(1), vda2*lim_alpha)];
theta_lim = [0.001, pi/2*lim_alpha];
r_lim = [max(ra2*(1-lim_alpha), ra-3*s_sigma(1)*ta), min(ra2*lim_alpha, ra + 3*s_sigma(1)*ta)];
%% Grid values
[g_theta, g_r] = meshgrid(s_theta, s_r);
g_vd = vd0+vd_slope*(180/pi).*g_theta;
g_kT = Q*(kT0+kT_slope.*g_theta*(180/pi));
g_sigma = sqrt(g_kT/M);

%% Function assumed to hold in my theory
%F = A .*g_vr.^2 .* exp(-(g_vr - g_vd).^2 ./ g_sigma.^2);
F = 1 /ta^3.* exp(-(g_r/ta - g_vd).^2 ./(2* g_sigma.^2));
%F = imfilter(F, fspecial('gaussian', [5 5]));
F = F.*cos(g_theta);
F = F .* g_r.^2 .* max(sin(g_theta),0.005);% cylindrical effect, avoid singular point
%F = F.* g_r .*  max(sin(g_theta),0.0005);
% here sqrt denotes that r is composed of transverse two vectors
F = F/sum(F(:));% Normalize
figure;imagesc(0:pi/2/1000:pi/2, 0:ra2/200:ra2,F);axis xy
fsize = 35;
ylabel('Position [m/s]', 'FontSize', fsize)
xlabel('Angle [rad]', 'FontSize', fsize)
drawnow

[dict_theta, dict_r] = Create_iCDF_2Ddictionary(F, g_theta, dtheta, g_r, dr);
save('Init_part_pos/dict.mat', 'dict_theta','dict_r', 'F'); 

prob1 = rand(NUM,1);
theta = fetch_dict(dict_theta, thetas, prob1, theta_lim);
figure;hist(theta,200);
drawnow

prob2 = rand(NUM,1);
r = fetch_dict2D(g_theta, g_r, dict_r, theta, prob2, r_lim);
figure;hist(r,200);
drawnow
vr = r/ta;
figure;hist(vr, 200);
save('Init_part_pos/part0.mat', 'r','theta','vr'); 

end

function iCDF_dict_fil = Create_iCDF_dictionary(fx, x, dx)

CDF = (tril(ones(numel(x))*dx,0)*fx')';

%{
p = polyfit(x, CDF, 4);

    function out = CDF_poly(temp)
        out = polyval(p, temp);
    end
%}

    function out = CDF_int(temp)
        out = interp1(x, CDF, temp);
    end
    function y = tmpfun(temp,out)
        y = (CDF_int(temp)-out)^2;
    end
    function theta = isin(out)
        options = optimset('TolFun', dx/50);
        theta = fminsearch(@tmpfun,0,options,out);
    end

% dictionary creation
sample_prob = 0:1/(numel(x)-1):1;
% Cummrative distribution function
iCDF_dict_fil = arrayfun(@isin, sample_prob);

%{
psize = 1;
h = fspecial('average', [1 psize]);
temp = padarray(iCDF_dict, [0 psize], 'symmetric');
temp(:,1:psize) = -fliplr(temp(:,psize+2:2*psize+1));
temp(:,end-psize+1:end) = repmat(temp(:,end-psize),[1 psize]) + abs(repmat(temp(:,end-psize), [1 psize]) - fliplr(temp(:,end-psize-psize:end-psize-1)));
temp(:,1+psize:end-psize) = iCDF_dict;
iCDF_dict = imfilter(temp, h, 'replicate');
iCDF_dict_fil = iCDF_dict(:, 1+psize:end-psize);
%}

end


function [dict_theta, dict_r] = Create_iCDF_2Ddictionary(f, t, dt, v, dv)

ft = sum(f, 1);
ft = ft/sum(ft*dt);
dict_theta = Create_iCDF_dictionary(ft, t(1,:), dt);

fv = zeros(size(f));
dict_r = zeros(size(f));
parfor k = 1:numel(ft(1,:))
    temp = f(:,k);
    fv(:,k) = temp/sum(temp*dv);
    dict_r(:,k) = Create_iCDF_dictionary(fv(:,k)', v(:,k)', dv)';
end

end

function out = fetch_dict2D(g_theta, g_vr, dict_vr, theta, prob2, thre)
    
    dtheta = g_theta(1,2)-g_theta(1,1);

    function iCDF = sampler(theta, p)
        tidx = floor(theta/dtheta) + 1;
        tidxp = theta/dtheta + 1 -tidx;
        dict = dict_vr(:,tidx)*(1-tidxp) + dict_vr(:,min(tidx+1, numel(dict_vr(:,1))))*tidxp;
        
        idx = floor(p*numel(dict)+1);
        idxp = p*numel(dict)+1-idx;
        if idxp > numel(dict)
            idxp = idx;
        end
        
        iCDF = dict(idx)*(1-idxp) + dict(min(idx+1, numel(dict)))*idxp;
        
        if iCDF >= thre(2) || iCDF <= thre(1)
            iCDF = sampler(theta, rand());
        end
        
    end
out = arrayfun(@sampler, theta, prob2);
end

function out = fetch_dict(dict, dx, prob, thre)
% dict : generated dictionary of inverse CDF
% dx : sampling span
% prob : random number (0 to 1)
% threachold : possible value allowed [thre_min, thre_max]

    function iCDF = sampler(p)
        idx = floor(p*numel(dict)+1);
        idxp = p*numel(dict)+1-idx;
        if idxp > numel(dict)
            idxp = idx;
        end

        iCDF = dict(idx)*(1-idxp) + dict(min(idx+1, numel(dict)))*idxp;

        if iCDF >= thre(2) || iCDF <= thre(1)
            iCDF = sampler(rand());
        end
    end
out = arrayfun(@sampler, prob);
end
