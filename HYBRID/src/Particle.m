classdef Particle < handle
    %Particle class
    %   Mover, injection schemes are implemented
    
    properties(SetAccess=private, GetAccess=public)
        % basic specification
        pos;        % positions [m]
        v;          % velocities [m/s]
        max_part;   %max_number of particles
        vth;    %veocity[m/s]
        vst;    %velocity standard deviation[m/s]
        T;      %temperature[eV]
        QE;     %unit charge
        M;      %mass of unit particle
        QE_M;   %acceleration coefficient denoted as QE/M
        dz;     %grid spacing in z direction [m]
        dr;     %grid spacing in r direction [m]
        % injection parameters
        vz_drift;    %velocity[m/s]
        vr_drift;    %velocity[m/s]
        z_inj_node; %injection cells in z direction
        r_inj_node; %injection cells in r direction
        inject_num_per_time; % number of particles per time
        inject_volume;      % Volume where to inject
        sample_posr;    % r position dictionary for gaussian distributed injection
        wall_pos;       % distant of the wall position from the left boundary
        id;             % Particle identification number
%        GPUFLAG;
        store_it;       % iteration number of each partilce until they go out of the geometry
        np;             % Number of particles which exists on the geometry
        
        %% depotion information
        d_i;    % indice to which cell particle belongs in z direction
        d_fi;   % particle position in z in cell numbers
        d_hz;   % d_fi-d_i
        d_j;    % indice to which cell particle belongs in r direction
        d_fj;   % particle position in r in cell numbers
        d_hr;   % d_fj-d_j
        
        %% bilinear deposition information
        % bilinearly weighted depending on the radial positions
        fz0r0;
        fz0r1;
        fz1r0;
        fz1r1;
    end
    
    methods
        function part = Particle( max_part, dz, dr, QE, Ti, M)
            part.pos = zeros(max_part,3);
            part.v = zeros(max_part,3);
            part.dz = dz;
            part.dr = dr;
            part.QE = QE;
            part.max_part = max_part;
            part.T = Ti;         
            part.M = M;
            part.QE_M = QE/M;
            part.store_it = zeros(max_part,1);
            part.vth = sqrt(2*Ti/M);    % thermal velocity
            part.vst = sqrt(Ti/M);      % standard deviation of the velocity            
            part.np = 0;              
        end
        
        
        
        function set_wall_pos(self, value)
            % register wall's position 
            self.wall_pos = value;
        end
        
        function set_injection( self, vz_drift, vr_drift, z_inj_node, r_inj_node, inpt, inject_volume )
            % set injection factors. this should be called only for the
            % uniform injection function
            % Input         
            % vz_drift      : drift velocity in z
            % vr_drift      : drift velocity in r
            % z_inj_node    : nodes used for the injection in z direction
            %    specified as
            % (zmin, zmax) [cell number]
            % r_inj_node    : nodes used for the injection in r direction
            % inpt          : number of particles injected every time step
            % inject_volume : volume of cells used for the injection
            self.vz_drift = vz_drift;
            self.vr_drift = vr_drift;
            self.inject_num_per_time = inpt;
            self.inject_volume = inject_volume;
            
            % Preparation for gaussian distribution
            sigma = 1.;
            A1 = 2*sigma^2/(2*sigma^2 - sqrt(2)*(1-sigma^2));
            A2 = 1/(2*(sigma^2/(1-sigma^2)) - sqrt(2)*sigma);
            function out = CDF(r)
                %out = erf(r/sigma/sqrt(2))-sqrt(2/pi)/sigma*r.*exp(-r.^2/2/sigma^2);
                out = A1*(1-exp(-r.^2/2/sigma^2))-A2*sqrt(2)*sigma .* erf(r/sigma/sqrt(2));
            end
            
            function y = tmpfun(r,out)
                y = (CDF(r)-out)^2;
            end
            function r = isin(out)
                r = fminsearch(@tmpfun,0,[],out);
            end
            self.z_inj_node = z_inj_node;
            self.r_inj_node = r_inj_node;          
        end
             
        function np = experimental_param_read(self, N, time)
            % Read file value from "Init_part_pos" folder
            % Input
            % N     : Number of macroparticles
            % time  : time of ballistic expansion from a point source
            % Output
            % np    : Number of particles
            addpath('../src/Init_part_pos');
            load('part0.mat');
            rmpath('../src/Init_part_pos');
            z = vr.*cos(theta);
            r = vr.*sin(theta);
            self.v(:,1) = z(1:N);
            self.v(:,2) = r(1:N);
            self.pos(:,1) = self.v(:,1)*time + self.wall_pos;
            self.pos(:,2) = self.v(:,2)*time;
            self.pos(:,3) = 0;
            
            %% Assume perturbation of Ti=Te=1
            kT = 1.6e-19;
            m = 1.67e-27*63.5;
            v_st = sqrt(kT/m);   
            vpr = v_st*randn(N,1);
            vpt = v_st*randn(N,1);
            vpz = v_st*randn(N,1);
            
            
            self.v(:,1) = self.v(:,1) + vpz;
            self.v(:,2) = self.v(:,2) + vpr;
            self.v(:,3) = self.v(:,3) + vpt;
            
            np = N;
            self.id(1:N) = (1:N)';
            self.np = np;            
        end
        
        function np = maxwell_dist(self, v_d, N, time, ratio)
            % Shifted maxwellian distribution. 
            % Input
            % v_d : drift velocity of maxwellian beam
            % N : total number injected
            % time :  drift time when generated on the coordinate
            % ratio : ratio of long and short side of ellipse
            % Output
            % np : number of particles
            % 
            % This function determins initial distribution of plasma beam
            % assuming all the particles are emitted from point source
            % Standard deviation in 1d is \sqrt{kT/m}, in 3d is \sqrt{3kT/m}
            % whereas thermal velocity is defined by \sqrt{2kT/m}
            
            % Just a random perturbation
            v_pert = self.vth / 8;
            
            vstz = self.vst^2;
            vstx = (self.vst*ratio)^2;
            vsty = vstx;
            st = 0;
            Sigma = [vstz, st, st;
                st, vstx, st;
                st, st, vsty];
            detSigma = det(Sigma);
            invSigma = inv(Sigma);
            m = 3;
            factor = (1/sqrt(2*pi))^m/sqrt(detSigma);
            %%%%%%%%%%%%%%%%%%%%%%%%
            % Previous setup (spherical)
            %%%%%%%%%%%%%%%%%%%%%%%%
            i_CDF = mvnrnd([0 0 0], Sigma, N);
            i_CDF = sortrows(i_CDF,-1);
            
            while min((i_CDF(:,1) + v_d)*time) < 0 %|| i_CDF(:,2)*time <4e-4
                idx = find( (((i_CDF(:,1) + v_d)*time) <0) );
                pos_num = idx(1) - 1;   % number of particles which is supposed to be located at z>=0
                neg_num = N - pos_num;  % number of particles which is supposed to be located at z<0
                temp = mvnrnd([0 0 0], Sigma, neg_num);
                i_CDF = vertcat(i_CDF(1:pos_num,:), temp);
                i_CDF = sortrows(i_CDF,-1);
            end
            
            
            
            seed = randperm(N)';
            i_CDF(:,1) = i_CDF(seed    );
            i_CDF(:,2) = i_CDF(seed+  N);
            i_CDF(:,3) = i_CDF(seed+2*N);
            
            self.v(1:N,1) = v_d + i_CDF(:,1);
            %self.v(1:N,1) = i_CDF(:,1);
            self.v(1:N,2) = sqrt(i_CDF(:,2).^2 + i_CDF(:,3).^2);
            
            % Proceed particles
            self.pos(1:N,1) = self.v(1:N,1)*time;
            self.pos(1:N,2) = self.v(1:N,2)*time;
            
            % Decide theta velocity
            r_max = max(self.pos(1:N,2));
            self.v(1:N,1) = self.v(1:N,1) + randn(N,1)*v_pert;
            self.v(1:N,2) = self.v(1:N,2) + randn(N,1)*v_pert.*self.pos(1:N,2)/r_max;
            self.v(1:N,3) = 0;%self.QE_M*(self.v(1:N,1)/2.*MF(1:N,2)-self.v(1:N,2)/2.*MF(1:N,1))*(time - ofs/v_d);% + randn(N,1)*v_pert;
            
            self.pos(1:N,1) = abs(self.pos(1:N,1)) + self.wall_pos;
            self.v(1:N,1) = (self.v(1:N,1));
            
            np = N;
            self.id(1:N) = (1:N)';
            self.np = np;
            
        end
        
        function np = uniform_injection( self, np, lr, lz, inject_num) 
            % Uniformly place particles on the geometry.
            % you must call set_injection beforehand for this to work
            % Input
            % np         : Number of particles before injection
            % lr         : length of the injection area in r direction
            % lz         : length of the injection area in z direction  
            % inject_num : Number of particles injected by the calling this function
            
            % Generate random seeds
            prob = rand(inject_num,1);
            % inverse function of parabolla
            function out = iParabola(p)
               out = lr*sqrt(p);
            end
            posr = iParabola(prob);            
            %figure;            hist(posr,50);
            %xlim([0 lr]);
                
            if ( inject_num > 0 )
                % position distribution
                self.pos(np+1:np+inject_num,1) = (self.z_inj_node(1)-1)*self.dz + rand(inject_num,1)*lz;
                self.pos(np+1:np+inject_num,2) = posr;
                % velocity distribution
                self.v(np+1:np+inject_num,1) = self.vz_drift + ( randn(inject_num,1) + randn(inject_num,1)+ randn(inject_num,1))/3*self.vth;
                self.v(np+1:np+inject_num,2) = self.vr_drift + ( randn(inject_num,1) + randn(inject_num,1) + randn(inject_num,1))/3*self.vth;
                self.v(np+1:np+inject_num,3) = 0*( randn(inject_num,1) + randn(inject_num,1) + randn(inject_num,1))/3*self.vth;
                if ( self.max_part < np )
                    disp(['Warning!! the number of particle exceeded the limitation!!']);
                end
            end      
            
            self.id(1:self.np+inject_num) = (1:self.np+inject_num)';
            self.np = self.np+inject_num;
            np = self.np;
        end                

        function reflective_all( self )
            % Reflective Boundary conditions
            
            % the reflection at the left wall is implemented
            raxis = 1;            
            temp1 = sign((self.pos(:,raxis) - self.wall_pos));
            self.pos(:,raxis) = (self.pos(:,raxis) -self.wall_pos ) .* temp1 + self.wall_pos;
            self.v(:,raxis) = self.v(:,raxis) .* temp1;
            
            % the reflection at bottom of the boundary is implemented
            zaxis = 2;            
            temp2 = sign(self.pos(:,zaxis));
            self.pos(:,zaxis) = self.pos(:,zaxis) .* temp2;
            self.v(:,zaxis) = self.v(:,zaxis) .* temp2;
        end
        
        function [ np, del_flag] = geometry_check(self, BOUNDARY, box, Lz, Lr)
            % Check if particle positions are inside geometry 
            % Input 
            % BOUNDARY      : Absorption or periodic
            % box           : Absorption boxes where particle dissappears
            % Lz            : Length of the geometry in z direction
            % Lr            : Length of the geometry in r direction
            % Output        
            % np            : Number of present particles 
            % del_flag      : delete flag of particles. 
            
            np = self.np;
            i = self.d_i;
            j = self.d_j;
            %reflective boundary on bottom          %r<0
            self.reflective_all(); % 1:parallel to z, 2: parallel to r

            in_box1 = zeros(np,1);
            in_box2 = zeros(np,1);
          
            if(exist('box','var'))
                % delete all which pass above the coil boundary
                in_box1 = i(1:np) < box(1,2,1) & i(1:np) > box(1,1,1) & j(1:np)>=box(2,1,1);
            end
            
            if strcmp(BOUNDARY, 'a')
                out_of_geometry =  self.pos(1:np,1)>=Lz | self.pos(1:np,2)>=Lr | self.pos(1:np,1)<0 ;
                % absorbing boundary on left, right or if in object
                in_box = in_box1 | in_box2;
                del_flag = in_box | out_of_geometry;
                np_to_del = sum(del_flag==1);
                nppre = np;
                np = np - np_to_del;
                self.kill_all(del_flag, np, nppre, in_box);
                
            elseif strcmp(BOUNDARY, 'p')
                out_of_field_left = self.pos(1:np,1)<0; 
                out_of_field_right = self.pos(1:np,1)>=Lz;
                out_of_field_top = self.pos(1:np,2)>=Lr;
                % absorbing boundary on left, right or if in object
                self.pos(:,1) = self.pos(:,1) + out_of_field_left*Lz - out_of_field_right*Lz;
                self.pos(:,2) = self.pos(:,2) + out_of_field_top.*(Lr - self.pos(:,2) );
                self.v(:,2) = self.v(:,2) -2*self.v(:,2).*out_of_field_top;      
            end         
            self.set_np(np);
        end
     
        
        function kill_all(self, del_flag, np, nppre, in_box)
            % kill all the particles specified by del_flag.
            % Input
            % del_flag      : target of deletion if 1
            % np            : number of present particles
            % nppre         : number of paticles to be deleted
            % in_box        : particles which is outside of the geometry
            
            if (sum(del_flag(:))>0)
                self.pos(in_box,:) = -1e20;
                self.v(in_box,:) = -1e20;
                self.store_it(in_box) = -1e20;
                inv_del_flag = (del_flag==0);
                nonzero_del_flag = (del_flag~=0);
                self.pos(1:np,:) = self.pos(inv_del_flag, :);
                self.v(1:np,:) = self.v(inv_del_flag, :);
                self.pos(np+1:nppre,:) = self.pos(nonzero_del_flag,:);
                self.v(np+1:nppre,:) = self.pos(nonzero_del_flag,:);
                self.id(1:np) = self.id(inv_del_flag,:);
                self.id(np+1:nppre,:) = self.id(nonzero_del_flag,:);
            end
            self.store_it(1:np) = self.store_it(1:np) + 1;
        end
        
        function push_all(self, dt)
            % advance all particles by time step dt
            self.pos = self.pos + self.v*dt;            
        end
        
        
        function accel_all(self, EF, MF, np, dt)
            % accelerate all the particles
            % using Buneman-Boris method
            % Input 
            % EF            : electric fields applied on each particle
            % MF            : magnetic fields applied on each particle
            % np            : number of present particles
            % dt            : time step
            temp = self.QE_M*EF;
            a = zeros(self.max_part, 3);
            a(1:np,:) = temp(1:np,:);
            self.v = self.v + a*dt/2;
            r = vertcat( self.pos(1:np,2), Inf*ones(self.max_part - np, 1));
            self.v = self.v ...
                + [                             self.QE_M*(self.v(:,2).*MF(:,3) - self.v(:,3).*MF(:,2)), ...  % z direction
                self.v(:,3).*self.v(:,3)./r + self.QE_M*(self.v(:,3).*MF(:,1) - self.v(:,1).*MF(:,3)), ...    % r direction
                -self.v(:,2).*self.v(:,3)./r + self.QE_M*(self.v(:,1).*MF(:,2) - self.v(:,2).*MF(:,1)) ]*dt;   % theta direction
            self.v = self.v + a*dt/2;
        end       
        
        function cyclotronic_mover_all( self, EF, MF, dt)
            % Cyclotronic mover. 
            % Please refer to Sec. 3.4.2 of master thesis
            [self.pos, self.v] = mex_MOVER_OMP( EF, MF, self.pos, self.v, self.QE_M, dt, self.np);
        end
        
        function set_pos(self, pos, part)
            % set positions from another Particle class
            self.pos = pos;
            self.d_i = part.d_i;
            self.d_j = part.d_j;
            self.d_fi = part.d_fi;
            self.d_fj = part.d_fj;
            self.d_hz = part.d_hz;
            self.d_hr = part.d_hr;
        end
            
        function [fi, i, hz, fj, j, hr] = deposition2nodes(self, dz, dr)
            % deposition of particles on the surrounding cells
            % Input 
            % dz        : grid spacing in z direction
            % dr        : grid spacing in r direction            
            % Output
            % fi        : real i index of particle's cell in z direction
            % i         : integral part
            % hz        : the reminder
            % fj        : real j index of particle's cell in r direction
            % j         : integral part
            % hr        : the reminder
            fi = 1.5+self.pos(:,1)/dz;        %real i index of particle's cell
            i = floor(fi);                  %integral part
            hz = fi-i;                      %the remainder
            
            fj = 1.5+self.pos(:,2)/dr;        %real j index of particle's cell
            j = floor(fj);                  %integral part
            hr = fj-j;                      %the remainder
                        
            self.d_fi = fi;
            self.d_i = i;
            self.d_hz = hz;
            self.d_fj = fj;
            self.d_j = j;
            self.d_hr = hr;
            %% Bilinear functions
            self.bil_weight();
        end
        
        function bil_weight(self)
            %% Refer to "Plasma Physics via computer simulation, pp 336
            % 14-11 Weighting in cylindrical coorinates for particles and
            % fields Written by Birdsall
            j = self.d_j(1:self.np);
            hz = self.d_hz(1:self.np);
            hr = self.d_hr(1:self.np);
            
            switch 'mat'
                case 'mat'
                    temp = j-1.5;
                    temp(temp<0) = 0;
                    rj0_2 = (temp   ).^2;        % Cell to which particle belongs
                    rj1_2 = (j - 0.5).^2;        % referenced cell for bilinear weighting
                    rji_2 = (j - 1.5 + hr).^2;   % radiul position of particles
                    high = rj1_2 - rji_2;        % higher grid
                    low  = rji_2 - rj0_2;        % lower grid
                    denom = rj1_2 - rj0_2;       % denominator

                    %% Register values
                    self.fz0r0 = ( high ) .* (1-hz)./denom;
                    self.fz0r1 = ( low  ) .* (1-hz)./denom;
                    self.fz1r0 = ( high ) .*    hz ./denom;
                    self.fz1r1 = ( low  ) .*    hz ./denom;
                    
                case 'mex'
                    [self.fz0r0,self.fz0r1,self.fz1r0,self.fz1r1] = mex_bil_weight_child(j,hz,hr);
            end

        end
        
        function set_vel(self, vel)
            self.v = vel;
        end
        
        function set_np(self, np)
            self.np = np;
        end
        
        function set_id(self)
            self.id = (1:self.np)';
        end
                
  
    end
    
end

