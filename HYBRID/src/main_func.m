function main_func(coil_pos, coil_length, current)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hybid Particle In Cell (PIC) code in MATLAB
% Oriented for the interaction between laser-induced plasma and magnetic
% field
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


global nz nr nn dz dr Lz Lr n0
global box
global GPUFLAG
%% SET CONSTANTS
[MU0, QE, EPS0, C0, K, AMU] = CONSTANTS;

%% Resultant folder name. 
% Put into 'result' folder in each workspace
dd = datestr(now,'mmdd_HHMM'); % Acquire Current Date
INPUTMODE = 'p';
switch INPUTMODE
    case 'p'
        input_driftMB;
        if BEAMTYPE == 'EXPERIMENT'
            coil_str = strcat(num2str(coil_pos(1)),'_',num2str(coil_pos(2)),'_',num2str(coil_length(1)), '_', num2str(coil_length(2)));
            folder = strcat('result/result',dd, ...
                '_TE',num2str(Te), ...
                '_COIL',coil_str,...
                '_CRNT',num2str(current),...
                '_MZ', num2str(Baxis),...
                '_WITHPERT'... %  ,'_NOWALL'...
                );
            if exist('RSTY','var')
                folder = strcat(folder, '_RSTY', num2str(RSTY));
            end            
        end
        
    case 'd'
        input_dispersion;
        mT = floor(Baxis)*1000;
        folder = strcat('result',dd,'_',num2str(mT),'mT');
end
%% Show where the files will be saved 
disp(strcat('To be saved in ' , blanks(1), folder));
disp('Initialization processing... Please be patient, this could take a while.');

%% Normalization factors
% Prepare reference values
B0 = B_REF;  % Reference field intensity
N0 = n0;  % Referenced density for the alfven wave
VA = B0 / sqrt(N0*M*MU0);% Alfven speed
OMEGA_CI = QE*B0/M; % Cycrotron frequency
OMEGA_PI = sqrt(N0*QE^2/EPS0/M); % plasma wave frequency
dt = CYTIME/OMEGA_CI; % Time step
%% Temperature coversion
kTi = Ti*QE; % Convert [eV] to [J]
kTe = Te*QE;  % Convert [eV] to [J]
%% CFL condition check
CFL = CFL_CONDITION_CHECK(OMEGA_PI, OMEGA_CI, C0, B0 / sqrt(n_thre*M*MU0), dz, dr, dt);
%% Perform standarization if NORMALIZE flag set to 1
NORMALIZE = 0; % If you wish to standarize or not
if NORMALIZE == 1
    normal = Normalization(B0, VA, OMEGA_CI, M, N0, QE, MU0);
    % Paramters
    dr = dr/normal.LENGTH;
    dz = dz/normal.LENGTH;
    r_inject = r_inject/normal.LENGTH;
    z_inject = z_inject/normal.LENGTH;
    v_drift = v_drift/normal.VEL;
    M = M/normal.MASS;
    n0 = n0/normal.NDEN;
    n_thre = n_thre/normal.NDEN;
    
    if (strcmp(INPUTMODE,'p'))
        ofs = ofs/normal.LENGTH;
        drift_t = drift_t/normal.TIME;
    end
    QE = QE/normal.CHARGE;
    kTi = kTi/normal.TEMPERATURE; %
    kTe = kTe/normal.TEMPERATURE; % Converted from [EV] to [J]
    MU0 = MU0/normal.MU0;
    mf_ext = mf_ext/normal.B0;
    dt = dt/normal.TIME;
    
    disp(strcat('dr: ', num2str(dr), '        dz: ', num2str(dz)));
    disp(strcat('nz: ', num2str(nz), ' nr: ', num2str(nr)));
    disp(strcat('r_inject: ', num2str(r_inject), ' z_inject: ', num2str(z_inject)));
    disp(strcat('v_drift: ', num2str(v_drift), '   dt: ', num2str(dt)));    
else
    % Put 1 for all values. Don't perform standalization
    normal = Normalization(1, 1, 1, 1, 1, 1, 1);
end

%% assignment of valuables
Lz = (nz-1)*dz;         %domain length in x direction
Lr = (nr-1)*dr;         %domain length in y direction
nn = nz*nr;             %total number of nodes

%% Compute node volume
[node_volume, inject_volume, lz, lr] = eval_VOLUME(nz, nr, dz, dr, z_inj_node, r_inj_node);  

%% compute some other values
switch INPUTMODE
    case 'p'
        spwt = nump/max_part; % super particles weight
    case 'd'
        spwt = n0*lr*lr*pi*lz/inject_num_per_time; % super particles weight
end

%% ghost particle weights on each cell
gpwt = round(node_volume/node_volume(1,1))*node_volume(1,1); % ghost particles weight
gpwt = gpwt(2:end-1,2:end-1);
gpwt = gpwt(:);

object = zeros(nz+1,nr+1);
%create an object domain for visualization
for j=box(2,1,1):box(2,2,1)
    object(box(1,1,1):box(1,2,1),j)=ones(box(1,2,1)-box(1,1,1)+1,1);
end
for j=box(2,1,2):box(2,2,2)
    object(box(1,1,2):box(1,2,2),j)=ones(box(1,2,2)-box(1,1,2)+1,1);
end


%% Create electric field class instance 
ef = ElectricField_HalfGrid(nz,nr,dz,dr,n0,box,n_thre, BOUNDARY);    %z-r components[V/m]
emass = M/1837/63.5;
ef.set_const_resistivity(emass,QE,dt, normal.B0/normal.CHARGE*normal.LENGTH^3);% normal.LENGTH^3*normal.B0/normal.CHARGE);
if exist('RSTY','var')
    ef.reset_rsty0(RSTY, normal.B0/normal.CHARGE*normal.LENGTH^3);
end
ef.set_coil_cell(coil1_cell, nz+1, nr+1);
ef.set_wall_cell(wall_pos, nz+1, nr+1);
ef.set_pt_mask();

%% Create magnetic field class instance
mf = MagneticField(nz,nr,dz,dr,normal,B_REF, BOUNDARY);    %z-r-theta components[T]=[Wb/m2]a
mf.set_coil_cell(coil1_cell, nz+1, nr+1);
mf.set_wall_cell(wall_pos, nz+1, nr+1);
[mf_ext] = mf.satisfy_divB(mf_ext, 10); % Modify mf_ext so that divB = 0
mf.register(mf_ext);

%% GPU FLAG set
%% FLAGS
GPUFLAG  = 1; % If you use GPU or not
if GPUFLAG == 1
    ef.set_GPU(GPUFLAG);
    mf.set_GPU(GPUFLAG);
end
%% Create Actual & ghost particles class instances
part = Particle(max_part, dz, dr, QE, kTi, M);
part.set_wall_pos(wall_pos);
ghost = Ghost( dz, dr, QE, M, nr, nz);
ghost.set_box_weight(coil1_cell);
ghost.deposition2nodes(dz, dr);

%% Zero accumuration variables
TIME = 0;          % Actual time
e_it = 0;          % Energy
idx_current = 0;   % index of current store
current_sample = 100; % Interval of storing current
store_current = zeros(ts/current_sample,nr+1,3);
current_temp = zeros(current_sample,nr+1,3);

%% BEAMTYPE = GAUSS or UNIFORM or PSOURCE
%% Point source like injection distributed by Shifted-Maxwellian
if strcmp(BEAMTYPE,'PSOURCE')
    part.maxwell_dist(v_drift,max_part,drift_t, ratio);
    part.set_id();
    part.deposition2nodes(dz, dr);
    np = part.geometry_check(BOUNDARY, box, Lz, Lr);
    if np==0
        disp('Particle generation error...\n\n')
    end
%% just use registerd initial distribution prepared by Prepare_MonteCarlo_poly.m    
elseif strcmp(BEAMTYPE, "EXPERIMENT")
    part.experimental_param_read(max_part, drift_t);
    part.set_id();
    part.deposition2nodes(dz, dr);
    np = part.geometry_check(BOUNDARY, box, Lz, Lr);
    if np==0
        disp('Particle generation error...\n\n')
    end
%% Inject uniformly on each cell
elseif strcmp(BEAMTYPE, "UNIFORM")
    part.set_injection(v_drift, 0, z_inj_node, r_inj_node, inject_num_per_time, inject_volume);
    part.uniform_injection(0, lz, lr, inject_num_per_time );
    part.set_id();
    part.deposition2nodes(dz, dr);
    np = part.geometry_check(BOUNDARY, box, Lz, Lr);
    if np==0
        disp('Particle generation error...\n\n')
    end 
end
%pos_temp_ini = part.pos(1:part.np,:);
%part_id_ini = part.id(1:part.np,:);

%% Calculate number/current density of actual & ghost particles
% at time 0,  n(0), J(0)
[J_first, n_first] = FLUX_weight( GPUFLAG, BOUNDARY, nz, nr, part, node_volume, spwt);
[J_ghost, ~] = FLUX_weight( GPUFLAG, BOUNDARY, nz, nr, ghost, node_volume, gpwt.*n_thre);
J00 = J_first + J_ghost;
n00 = n_first + n_thre;
vel_first = J00./repmat(n00, [1 1 3])/part.QE; 
%% 1. advance E(0) to E(1/4) for calculating B(0) to B(1/2),
% E(0)
ef.calc_E_field( MU0, mf.fetch(), mf_ext, J00, kTe, n00, part.QE, 'CAM' );
% n(1/4), J(1/4)
Estar = ef.fetch('Original');
J14 = J00 + dt/4*part.QE_M*part.QE.*repmat(n00,[1 1 3]).*(Estar + cross(vel_first, mf.half_expand,3));
n14 = n00;
% E(1/4)
ef.calc_E_field( MU0, mf.fetch(), mf_ext, J14, kTe, n14, part.QE, 'CAM' );
% B(1/2)
mf.CL(ef, CL_iteration, MU0, mf_ext, J14, kTe, n14, part, dt/2);

%% 2. Advance particles half step
% Actual Particles
part.push_all(dt/2);
np = part.geometry_check(BOUNDARY, box, Lz, Lr);
part.deposition2nodes(dz,dr);
part.set_id();  % set initial particle id after confirming their existance
% Ghost particles
ghost.push_all(dt/2);
ghost.deposition2nodes(dz,dr);
% Weighting
[J_ion_plus, n_ion_new] = FLUX_weight( GPUFLAG, BOUNDARY, nz, nr, part, node_volume, spwt);
[J_ghost_plus, ~, ~] = FLUX_weight( GPUFLAG, BOUNDARY, nz, nr, ghost, node_volume, gpwt.*n_thre);
% Combine field values of actual & ghost particles
J_plus = J_ion_plus + J_ghost_plus;
n_12 = n_ion_new + n_thre;
vel_plus = J_plus./repmat(n_12,[1, 1, 3])./part.QE;

%% just for the naming convention in main loop...
J1 = J00;
n_new = n_12;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Loop starts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for it = 1:ts
    
    %% 1. Current advance method
    ef.calc_E_field( MU0, mf.fetch(), mf_ext, J1, kTe, n_new, part.QE, 'CAM' );
    Estar = ef.fetch('Original');
    % J(n+1/2)
    J_12 = J_plus + dt/2*part.QE_M*part.QE.*repmat(n_new,[1 1 3]).*(Estar + cross(vel_plus, mf.half_expand,3));
    % E(n+1/2)
    ef.calc_E_field( MU0, mf.fetch(), mf_ext, J_12, kTe, n_new, part.QE, 'CAM' );
    
    %% 2 . Particle mover calculation & number density

    if (np>0)
        % Particles
        [EF, MF] = bil_Part_Weight(part, ef, mf);        
        part_old = part;
        part.cyclotronic_mover_all( EF, MF, dt);
        part_old.set_pos(part.pos, part);
        np = part.geometry_check(BOUNDARY, box, Lz, Lr);
        part.deposition2nodes(dz, dr);        
        if np <= 1000
            if (GPUFLAG>=1)
                reset(gpuDevice);
            end
            
            break
        end
        %% 3. Number density
        n_old = n_new;
        % J-, n derived by old information
        [J_ion_minus, n_ion_minus] = FLUX_weight( GPUFLAG, BOUNDARY, nz, nr, part_old, node_volume, spwt);
        % J+, n(n+3/2), v(n+1)
        [J_ion_plus, n_ion_new] = FLUX_weight( GPUFLAG, BOUNDARY, nz, nr, part, node_volume, spwt);
    end    
        
    
    %% Ghost particles
    [EFg, MFg] = ghost.ghost_field_weight(ef, mf);
    ghost_old = ghost;
        
    ghost.cyclotronic_mover_all(EFg,MFg,dt);
    ghost.geometry_check(Lz, Lr);
    gpos_temp = ghost.pos;
    ghost.deposition2nodes(dz, dr);
    
    ghost_old.set_pos(ghost.pos, ghost);  %  update only position for the ghost particle at last time step so as to calculate the inter-time step value
    % J-, n derived by old information
    [J_ghost_minus, ~] = FLUX_weight( GPUFLAG, BOUNDARY, nz, nr, ghost_old, node_volume, gpwt.*n_thre);
    % J+, n+, v(n+1)
    [J_ghost_temp,~] = FLUX_weight( GPUFLAG, BOUNDARY, nz, nr, ghost, node_volume, gpwt.*n_thre);
    J_ghost = J_ghost_temp;
    % J(n+3/2), n(n+3/2),
    ghost_new = ghost;
    ghost_new.push_all(dt/2);
    ghost_new.geometry_check([], Lz, Lr);    

    ghost.deposition2nodes(dz,dr);
    [~, ~, vel_ghost] = FLUX_weight( GPUFLAG, BOUNDARY, nz, nr, ghost_new, node_volume, gpwt.*n_thre);
    % initialize position while setting renewed particle velocity
    ghost.replace(vel_ghost); 

    
    %% * merge the result of ghost and real particles
    J_minus = (J_ion_minus.*repmat(n_ion_minus,[1 1 3]) + J_ghost_minus.*repmat(n_thre,[1 1 3]))./repmat(n_ion_minus + n_thre, [1 1 3]);
    J_plus = (J_ion_plus.*repmat(n_ion_new, [1 1 3]) + J_ghost.*repmat(n_thre, [1 1 3]))./repmat(n_ion_new + n_thre, [1 1 3]);    
    n_new = n_ion_new + n_thre;    
    
    %% 4. Current density
    n1 = (n_old + n_new)/2;      % n(n+1)
    J1 = (J_plus + J_minus)/2;   % J(n+1)
    vel_plus = J1./repmat(n1,[1, 1, 3])./part.QE;  % vel(n+1)

    %% 5. Calculate magnetic field
    % B(n+3/2)
    %    curlBp = zeros(nz+1, nr+1, 3);
    curlBp = mf.CL(ef, CL_iteration, MU0, mf_ext, J1, kTe, n1, part, dt);
    
    %% Save result to files    
    if it == 1
            mkdir(folder);   
    end
    
    %% Store values on the right hand side wall
    ridx = rem(it,current_sample) + 1;
    current_temp(ridx,:,:) = J_ion_plus(end-1,:,:);
    if(mod(it, current_sample) == 0 || it ==ts || it ==1)
        idx_current = idx_current + 1;
        store_current(idx_current,:,1:3) = mean(current_temp);
        save_val('crnt_rwall', store_current, folder, 0);        
    end    
    
    %% Save physical values
    if (mod(it,PERIOD) == 0 || it==ts|| it ==1)
        if it == 1      
            save(strcat(folder,'/mf_ext.mat'),'mf_ext'); 
            save(strcat(folder,'/normal.mat'),'normal');
            save(strcat(folder,'/matlab.mat')); % Save Entire variables in workspace
            %% Save particle initial position & id for coloring
            save_val('pos_temp_ini', part.pos(1:part.np,:), folder, 0);
            save_val('part_id_ini', part.id(1:part.np,:), folder, 0);
            
        end
        save_val('gpos_temp', gpos_temp, folder, it);
        save_val('ef_temp', ef.fetch('Original'), folder, it);
        save_val('mf_temp', mf.fetch(), folder, it);
        save_val('vel_temp', part.v, folder, it);
        save_val('pos_temp', part.pos, folder, it);
        save_val('part_id', part.id, folder, it);
        save_val('n_temp', n_ion_new, folder, it);
        save_val('cB_temp', curlBp, folder, it);
        save_val('rsty_temp', ef.rsty, folder, it);
        save_val('J_temp', J_plus, folder, it);
        save_val('gvel_temp', ghost.v, folder, it);
        save_val('part_it', part.store_it, folder, it);
        save_val('np_temp', np, folder, it);
        disp(strcat('save_',folder));
    end
    
    
    %% Display and save current status
    if (mod(it,1000) == 0 || it==ts || it==1)
        
        TIME = TIME + dt;
        fprintf('Time Step %i, Particles %i, Time : %e\n',it, np, TIME);
        % Store ion energy
        e_it = e_it + 1;
        Ei =  sum(1/2*M*sqrt(part.v(:,1).^2 + part.v(:,2).^2 + part.v(:,3).^2));
        fprintf('Ion energy : %e\n', Ei);
        store_Ei(e_it) = Ei;
        save_val('Ei', store_Ei, folder, 0);
    end
    

    
    %% Store current values for the angular distribution evaluation
    if (mod(it, acurrent_sample) == 0 || it == ts)
        save_val('J_temp', J_ion_plus, folder, it);
    [mf_temp] = mf.satisfy_divB(mf_ext, 2); % 10 times of iteration for satisfying divB=0
     mf.register(mf_temp);

    end
    
    
end
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           Loop ends
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (GPUFLAG>=1)
    reset(gpuDevice);
end
fprintf('Complete!\n');
end

function save_val(valstr, val2save, folder, it)
pathstr = strcat(folder, '/', valstr, num2str(it,'%08d'), '.mat');
eval(strcat(valstr, ' = ', 'val2save;'));
save(pathstr, valstr);
end
