function CFL = CFL_CONDITION_CHECK(OMEGA_PI, OMEGA_CI, C0, VA, dz, dr, dt)
%% Time condition (CFL condition)
spacial_dim = 2; % Number of dimension. 2 if r-z cylindrical

switch 'paper'
    case 'paper'
        % a necessary condition is that the discretisation resolves all
        % relevant spatial and temporal scales
        % the ion inertial length is delta_i = c/omega_pi
        % where omega_pi^2 = n_i q_i^2 / (EPS0*m_i)
        % the ion gyrofrequency omega_ci = q_i B / m_i
        % also determins CFL condition as
        % Reference: Hybrid modeling of plasmas
        % http://arxiv.org/pdf/0911.4435.pdf
        
        % omega_ci ( ion cyclotron frequency )
        %omega_ci = QE*B0/M;
        % delta_i ( velocity maintaining ion intertial length condition )
        delta_i = C0/OMEGA_PI;
        % CFL condition
        CFL = 1/OMEGA_CI/sqrt(spacial_dim)/pi*(min(dz,dr)/delta_i)^2;
        
    case 'simple'
        % definition of CFL just from the dimensional condition below
        % dt<dx/VA/sqrt(spaccial_dim)
        CFL = min(dz,dr)/VA/sqrt(spacial_dim);
end

if (dt > CFL)
    if (~exist('loop','var'))
        disp(['CFL_CONDITION : dt exceeds appropreate value.']);
        disp(['Do you continue?']);
    elseif (dt > C0/omega_ci)
        disp(['CFL_CONDITION : Cyclotronic condition error']);
    end
end

end