classdef ElectricField_HalfGrid < Field
    %     % Electric field calculation and operation
    
    properties (SetAccess = private)
        % old electric field value to raise the speed of SOR
        z_old;
        r_old;
        theta_old;
        
        n0; % electrons number density at certain Electron temperature Te
        box;    % Object where Electric field cannot penetrate
        SOLVERMODE; % Several types of solvers prepared
        BOUNDARY;
        rsty; % resistivity
        rstyJ; % resistivity * current density
        n_thre;
        r_thre; % referenced threashold for the resistivity calculation
        %% 
        %{
        first_loop;
        Az;
        Ar;
        Atheta;
        Az_m;
        Ar_m;
        Atheta_m;
        %}

        %% Pressure term
        pt_mask;
        inv_pt_mask;
        curlBp_mask;
        %% Resistivity coefficients
        % set_const_resistivity() defines all these value
        r_sigma;
        r_rsty0;
        r_coeff;
        r_factor;
        % hiroki defined so that Bp decays exponentially at time constant
        % dt by calculating
        % eta = mu0*Bp*exp(-dt)/nabla^2(Bp)
        r_alpha;
        %%
        %% Electron inertia term
        emass_QE;
        
        debug;
    end
    
    methods % mask for pressure term
        
        function efhg = ElectricField_HalfGrid(nz0,nr0,dz,dr,n0, box, n_thre, boundary)
            efhg = efhg@Field(nz0+1,nr0+1,dz,dr);
            efhg.nz0 = nz0;
            efhg.nr0 = nr0;
            efhg.nz1 = nz0+1;
            efhg.nr1 = nr0+1;
            efhg.nz2 = nz0+2;
            efhg.nr2 = nr0+2;
            efhg.box = box;
            efhg.z_old = zeros(efhg.nz1, efhg.nr1);
            efhg.r_old = zeros(efhg.nz1, efhg.nr1);
            efhg.theta_old = zeros(efhg.nz1, efhg.nr1);
            efhg.n0 = n0;
            efhg.n_thre = n_thre;
            efhg.BOUNDARY = boundary;
            %efhg.first_loop = 1;
            efhg.gr2 = abs(meshgrid(1:efhg.nr2,1:efhg.nz2)-2.);
            efhg.gr1 = abs(meshgrid(1:efhg.nr1,1:efhg.nz1)-1.5);
            efhg.gr0 = abs(meshgrid(1:efhg.nr0,1:efhg.nz0)-1);
  
            efhg.wall_mask = ones(efhg.nz1,efhg.nr1);
            efhg.set_cyl_weight();
            
            %efhg.set_A();
            efhg.debug = 0;
            if efhg.debug==1
                disp('EF Debug on');
            end
            efhg.pt_mask = ones(efhg.nz1,efhg.nr1,3);       
            efhg.inv_pt_mask = zeros(efhg.nz1,efhg.nr1,3);
            efhg.set_box_weight([0 0; 0 0]);            
        end        
            
        function set_pt_mask(self)
            self.pt_mask = repmat(self.coil_mask .* self.wall_mask, [1,1,3]);
            self.inv_pt_mask = 1 - self.pt_mask;
        end
        
        function out = fetch(self,varargin)
            
%             if isempty(length(varargin))
%                 TYPE = 'None';
%             else
%                 TYPE = varargin(1);
%             end
%             
%             if strcmp(TYPE,'Original')
                out = cat(3, self.z, self.r, self.theta);
%             else
%                 ef_z = 0.25*(self.z(1:end-1,1:end-1,:) + self.z(1:end-1,2:end,:) + self.z(2:end,1:end-1,:) + self.z(2:end,2:end,:));
%                 ef_r = 0.25*(self.r(1:end-1,1:end-1,:) + self.r(1:end-1,2:end,:) + self.r(2:end,1:end-1,:) + self.r(2:end,2:end,:));
%                 ef_theta = 0.25*(self.theta(1:end-1,1:end-1,:) + self.theta(1:end-1,2:end,:) + self.theta(2:end,1:end-1,:) + self.theta(2:end,2:end,:));
%                 out = cat(3, ef_z, ef_r, ef_theta);
%             end
        end
        
        function register_rstyJ(self, rstyJ)
            % Register Ohm's law term
            self.rstyJ = rstyJ;
        end
        
        

        
        function [elf_fil, mvexB, pt, dE, Jall, JI] = calc_E_field( self, MU0, B, B_ext, J_ion, kTe, n_ion, QE, type )
            %% Calculate E
            % calculate electric field using massless electron equation of
            % motion
            % current advance method
            [curlBp, B_int, nabla_n, ref_n] = self.CAM_input_process(B, B_ext, n_ion);
            pt = self.pressure_term( ref_n, kTe, QE, nabla_n); 
            pt = pt.*self.pt_mask;
            
            %% Electric field corresponds to -vexB
            [mvexB, ve] = self.hall_term(QE, n_ion, J_ion, curlBp, B_int, MU0);
            if self.emass_QE
                inert_e = self.advection_term(ve);
            end
            %% Integrate electric field value kkkk
            self.set_resistivity(n_ion);
            self.rstyJ = self.rsty .* curlBp/MU0;
            
            if strcmp(type, 'CAM')
                %% Electric field corresponds to -nabla p
                elf = ( mvexB + pt + self.rstyJ);
                if self.emass_QE
                   elf = elf + inert_e; 
                end

           
            elseif strcmp(type, 'CL')
                elf = ( mvexB + self.rstyJ);
            end
            
            elf = self.copy_cells(elf);
            if isnan(sum(elf(:)))
                disp('Error : EF contains Nan value');
                elf(isnan(elf)) = 0;
            end
            if ~isfinite(sum(elf(:)))
                disp('Error : EF contains Inf value');
                elf(~isfinite(elf)) = 0;
            end            
            %% Apply filter on electric field for numeical stability
            %% GPU utilized configuration
            E_filtered = field_filter(self.GPU, self.nz1, self.nr1, elf);
            elf_fil = self.copy_cells(E_filtered);
            
           
            
            if ~strcmp(type, 'CL')
                self.register(elf_fil);
            end
            
        end
        
        
        
        
        %% Set constants of Resistivity
        function set_const_resistivity(self, me, QE, T, nrml)
            % Refer to Amano, T, "Robust Method for Hnadling Low Density
            % Regions in Hybrid Simulations for Collisionless Plasma, J.
            % Comput, Phys., 275, 197, 2014, doi:10.1016/j.jcp.2014.06.048
            % r_me = me;  % Mass of electron
            r_gamma = 1/T;   % Time constant
            self.r_rsty0 = 1e-5 / nrml ;  % Minimum resistivity, 1e-5 by default
            %self.r_coeff1 = 0.5;
            self.r_coeff = r_gamma/QE;
            %self.r_coeff = coeff1 * coeff2 /nrml;
            self.r_factor = 0.5e-12;
            self.r_thre = 1e19;  % Arbitral value which is assumed to be the describable base density 
            self.r_sigma = self.r_thre/10;  % Reference density            
        end
        
        function reset_rsty0(self, value, nrml)
            self.r_rsty0 = value/nrml;
        end
        
        function set_resistivity_coeff(self, MU0, dt) 
            self.r_alpha = MU0*exp(-dt/10);
        end
                        
        function set_resistivity(self, n_ion)
            %rsty = 1e-3*(1 - tanh((n_ion - self.n_thre - self.r_thre)/self.r_thre))/2 + self.r_rsty0;
            %self.rsty = repmat(rsty, [1 1 3]);
            
            self.rsty = self.r_rsty0*ones(self.nz1,self.nr1,3) + (1-self.box_weight1).*1e-2;
            
        end
        
        function set_resistivity_inside_coil(self, Bp0)          
            % calculate laplacian of Bp
            Bp2 = self.cyl_interpolate0to2(Bp0);
            Bp1 = self.cyl_interpolate2to1(Bp2);
            
            cell = self.coil_cell;            
            % Axial direction
            [dBzdr, dBpdz] = gradient(Bp1(:,:,1), self.dr, self.dz); 
            Lz = 4*del2(Bp1(:,:,1), self.dr, self.dz) + 1./(self.gr1*self.dr).* dBzdr;
            cz = self.r_alpha * Bp1(:,:,1)./Lz;
            Lz(~isfinite(Lz)) = 0;
            % Radial direction
            [dBrdr, dBpdz] = gradient(Bp1(:,:,2), self.dr, self.dz); 
            Lr = 4*del2(Bp1(:,:,2), self.dr, self.dz) + 1./(self.gr1*self.dr).* dBrdr;
            %Lr(Lr==0) = Inf;
            cr = self.r_alpha * Bp1(:,:,2)./Lr;
            Lr(~isfinite(Lr)) = 0;
            % Theta direction                    
            [dBtdr, dBpdz] = gradient(Bp1(:,:,3), self.dr, self.dz);
            Lt = 4*del2(Bp1(:,:,3), self.dr, self.dz) + 1./(self.gr1*self.dr).* dBtdr;
            %Lt(Lt==0) = Inf;
            ct = self.r_alpha * Bp1(:,:,3)./Lt;      
            Lt(~isfinite(Lt)) = 0;
            
            self.rsty(:,:,1) = cz;
            self.rsty(:,:,2) = cr; 
            self.rsty(:,:,3) = ct;
                   
        end
        
        
        
        
        function [mvexB, ve] = hall_term(self, QE, n_ion, J_ion, curlBp, B_int, MU0)
            ve = - (curlBp/MU0 - J_ion)/QE./repmat(n_ion, [1, 1, 3]);
            mvexB = cross(-ve, B_int, 3);
        end
        
        function const_advection(self, QE, emass)
            self.emass_QE = emass/QE;
        end
        
        function inert_e = advection_term(self, ve)
            [uzuz, uruz] = self.upwind(ve(:,:,1),ve(:,:,2),ve(:,:,1));
            [uzur, urur] = self.upwind(ve(:,:,1),ve(:,:,2),ve(:,:,2)); 
            [uzut, urut] = self.upwind(ve(:,:,1),ve(:,:,2),ve(:,:,3));             
            inert_e = -self.emass_QE*cat(3,uzuz+uruz, uzur+urur, uzut+urut);
        end
       
        
        function [uzd, urd] = upwind(self, uz,ur,d)
            %% up wind differentiation
            % u : velocity field
            % d : field to be diffrentiated (component of velocity in this case)
            % Case of differentiation in z
            u = uz;
            last = numel(d(:,1));
            dz = [d(1,:);
                d;
                d(last,:)];
            last = last+2;
            ddz = -u.*((u>0).*(dz(2:last-1,:)-dz(1:last-2,:))+(u<0).*(dz(3:last,:)-dz(2:last-1,:)));
            uzd = uz.*ddz/self.dz;
            % Case of differentiation in r
            u = ur;
            last = numel(d(1,:));
            dr = [d(:,1), d, d(:,last)];
            last = last+2;
            ddr = -u.*((u>0).*(dr(:,2:last-1)-dr(:,1:last-2))+(u<0).*(dr(:,3:last)-dr(:,2:last-1)));
            urd = ur.*ddr/self.dr;
        end
        
        
        function pt = pressure_term(self, n_ion, kTe, QE, nabla_n)
            pt = -1/QE./repmat(n_ion, [1 1 3]).*(kTe*nabla_n);
        end
        
        function A =  copy_cells(self, A)
            % Copy cells on the boundary
            % on the Axis
            A(:,1,1) =  A(:,2,1);
            A(:,1,2:3) = A(:,2,2:3);
            % On the top boundary
            A(:,end,:) = A(:,end-1,:);
            if strcmp(self.BOUNDARY, 'a')
                A(1,:,1) = A(2,:,1);
                A(1,:,2:3) = 0;%- A(2,:,2:3);
                A(end,:,1) = A(end-1,:,1);
                A(end,:,2:3) = 0;%- A(end-1,:,2:3);
                %A = self.open_boundary(A);
            elseif strcmp(self.BOUNDARY, 'p')
                A(1,:,:) = A(2,:,:);
                A(end,:,:) = A(end-1,:,:);
            end
        end
        
        
        function [curlBp, B_int, nabla_n, grad_numel] =  CAM_input_process(self, B, B_ext, n_ion)
            % int indicates interpolation after expansion
            
            % prepare function for yielding geometry out of original one.
            function out = field_expand(A)
                X = padarray(A,[1 1 0],'replicate','both');
                if (ndims(X)==2)
                    X(:,1) = X(:,3);
                elseif (ndims(X)==3)
                    % Boundary condition on the axis
                    X(:,1,1) = X(:,3,1);    % z direction on the axis
                    X(:,1,2:3) = -X(:,3,2:3);   % r,theta direction on the axis
                end
                out = X;
            end
            
            % prepare function for interpolating the center of the grid
            % value
            function out = interpolate(A)
                out = 0.25*(A(1:end-1,1:end-1,:) + A(1:end-1,2:end,:) + A(2:end,1:end-1,:) + A(2:end,2:end,:));
            end
            
            % Extend B, to have nz+2, nr+2 elements
            % Expand magnetic field
            Bp = field_expand(B - B_ext);
            B_expand = field_expand(B);
            %% derive the value at the center of the grid
            B_int = interpolate(B_expand.*self.gr2)./self.gr1;
            %% Compute Rotation value
            curlBp = self.cylindrical_ROT(Bp, 'ROT_B');
            %curlBp = curlBp.*self.box_weight1;
            
            %% Assume isothermal pressure as pe=C*numel (C = const)
            grad_numel=real(n_ion);

            probe_z = self.wall_cell(1,2)+2;
            probe = grad_numel(probe_z,:); % Probe for determining number density in the wall
            grad_numel(1:probe_z-1,:) = repmat(probe, [probe_z-1, 1]);
            [fr, fz] = gradient(grad_numel, self.dr, self.dz);
            fr(:,2) = 0.25* ( n_ion(:,4) - n_ion(:,3)); % Determined by the 2nd order Taylor expansion
            nabla_n = cat(3,fz,fr,zeros(self.nz1,self.nr1));
                       
        end
        
        
        %% Calculate rotation
        function curlA = cylindrical_ROT(self, A, TYPE)
            % calculate curlA from electric field A
            % no changes in theta direction
            % 'ROT_B'
            
            curlA = zeros(self.nz1,self.nr1,3);
            
            % Apply rotation operator to magnetic field
            % z direction
            
            rAtheta = (self.gr2).*A(:,:,3);         % r*Atheta
            temp = ( rAtheta(:,2:end) - rAtheta(:,1:end-1) )/self.dr;
            drAtheta_dr = (temp(1:end-1,:) + temp(2:end,:))/2;
            curlA(:,:,1) = drAtheta_dr./(self.gr1);
            
            % r direction
            temp = (A(2:end,:,3) - A(1:end-1,:,3))/self.dz;
            dAtheta_dz = (temp(:,2:end) + temp(:,1:end-1))/2;
            curlA(:,:,2) = -dAtheta_dz;
            % theta direction
            temp = (A(2:end,:,2) - A(1:end-1,:,2))/self.dz;
            dAr_dz = (temp(:,1:end-1) + temp(:,2:end))/2;
            temp = (A(:,2:end,1) - A(:,1:end-1,1))/self.dr;
            dAz_dr = (temp(1:end-1,:) + temp(2:end,:))/2;
            curlA(:,:,3) = dAr_dz - dAz_dr;
            
            % Modify boudnary value so that quadratic conditions met
            % quadratic equation b exists
            curlA(:,2,:) = 0.5*(3*curlA(:,3,:)-2*curlA(:,4,:));
 %{
            % quadratic equation b = 0
%            curlA(:,2,2:3) = 0.25/2.25 * curlA(:,3,2:3);
            %}
            
            % Same value on edges since they are just extended and
            % doesn't conserve vector potential
            curlA(1  , :,  1:3) = curlA(2,:,1:3);
            curlA(end, :,  1:3) = curlA(end-1,:,1:3);
            curlA(:  , 1,  1)   = curlA(:,2,1);
            curlA(:  , 1,  2:3) = - curlA(:,2,2:3);
            curlA(:  , end,1:3) = curlA(:,end-1,1:3);
            
            if self.debug
                if any(isnan(curlA(:)))
                    disp('ElectricField_HalfGrid.m cylindrical_ROT');
                    disp(strcat('Error_in_', TYPE));
                end
            end
        end
        
        function set_solver(self, solver)
            self.SOLVERMODE = solver;
        end
        
        function ef = open_boundary(self, ef)
            % Diminish the value fo electric field near open boundaries
            % This is not smart way but easy to implement
            denom = 5;
            cells = 8;
            z = 1:self.nz1;
            kl = repmat(0.5*(1+tanh((z'-cells)/denom)), [1,self.nr1]);
            kr = flipud(kl);
            r = 1:self.nr1;
            kt = repmat(0.5*(1+tanh((self.nr2-(r-cells))/denom)), [self.nz1,1]);
            
            gk = kr.*kt;
            ef = ef.*repmat(gk,[1 1 3]);
        end
        
        function store_old(self)
            self.z_old = self.z;
            self.r_old = self.r;
            self.theta_old = self.theta;
        end
    end
    
end

