%figure(17);imshow(log(abs(fft2(temp))))
figure(16);

idx = 20;
start = 1;
X = der(idx:end - idx + 1,start:end-000);

%% Test function
%{
[go, gk] = meshgrid(0:5:5*1023);
[go, null] = meshgrid(0:150:150*1023);
mo = 1/Fs_t * go;
mk = 1/Fs_k * gk;

X = sin((mk + mo));
%}
%%{

[m, n] = size(X);
%{
w = hann(m);
X = X.*repmat(w, [1 n]);
w = hann(n);
X = X.*repmat(w', [m 1]);

%}
figure(16);imagesc(X);
%{
for i = start:100:n
    figure(16);plot(temp(:,i));
    title(i);
end
hold off
%}

title(it);

temp = fft2(X);
Y = log(abs(temp))';

T = dt;         % Actual time step
Fs_t = 1/dt;
Fs_k = 1/dz;
[K, L] = size(X);

P2 = abs(Y/L);
%P1 = P2(1:floor(L/2)+1,:);
%P1(2:end-1) = 2*P1(2:end-1);
omega = 2*pi*Fs_t*(0:(L/2))/L;

P2 = abs(Y/K);
%P1 = P2(:,1:floor(K/2)+1);
%P1(2:end-1) = 2*P1(2:end-1);
k = 2*pi*Fs_k*(0:(K/2))/K;

Y = Y( 1:floor(L/2)+1, 1:floor(K/2)+1);


figure(8);
clf;
imagesc(k,omega,Y);
axis([0, 6, 0, 20]);
colormap(jet(64));
axis xy;
xlabel('kc/\omega_{pi}');
ylabel('\omega/\omega_{ci}'); %noramlized by angular frequency 
[m, n] = size(Y);
colorbar;
hold on;

mr = 1; % mass ratio of ion compared to proton
omega_ci = 1;
omega_ce = omega_ci * 1837*mr;
omega_pi = C0/normal.VEL;
omega_pe = omega_pi * sqrt(1837*mr);

omega_L = linspace(0,1);
L = 1 - (1./omega_L).*(omega_pi.^2./(omega_L - omega_ci) + (omega_pe.^2./(omega_L + omega_ce)));
R = 1 - (1./omega).*(omega_pi.^2./(omega + omega_ci) + (omega_pe.^2./(omega - omega_ce)));
%Nm = (A2 - sqrt(A2.*A2 - 4.*A1.*A3))/2./A1;
k_L = abs(omega_L./C0.*normal.VEL.*sqrt(L));
k_R = abs(omega./C0.*normal.VEL.*sqrt(R));
%k_wave = funcarray(@cold_plasma, omega);
p1 = plot(k_L, omega_L,'k-.', k_R, omega, 'k--');%,k,k,'k:', k,C0/normal.VEL*k,'k-');
legend('L wave', 'R wave');%, 'Alfven wave', '\omega = ck');
%ax = gca;
%ax.FontSize = 15;
%p1(1).LineWidth = 2;
%p1(2).LineWidth = 2;
%p1(3).LineWidth = 2;
%p1(4).LineWidth = 1;
hold off;
drawnow;