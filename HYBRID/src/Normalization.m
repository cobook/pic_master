classdef Normalization
    %NORMALISATION 
    %   Normalization units are specified here
    
    properties (SetAccess=private, GetAccess=public)
        B0;         %magnetic field intensity[Wb/m2]
        VEL;        %velocity[m/s]
        OMEGA_I;    %cyclotron frequency[/s]
        LENGTH;     %length[m]
        RHOM0;      %mass density[kg/m3]
        MASS;       %mass[kg]
        TIME;       %time[s]
        CHARGE;     %charge[C]
        ELEC;       %electricic field intensity[N/C]
        ENERGY;     %energy[J]
        TEMPERATURE;%electron temperature[eV]
        MU0;        %permittivity
        NDEN;       %number density [/m3]
        CURR;       %current density [C /s m2]
    end
    
    methods
        function normal = Normalization(B0, VA, OMEGA_CI, M, N0, QE, MU0)
            normal.B0 = B0; %[T]
            normal.VEL = VA;  %[m/s]
            normal.OMEGA_I = OMEGA_CI; %[/s] 
            normal.LENGTH = VA/OMEGA_CI; %[m]
            normal.RHOM0 = M*N0;   %[kg/m3]
            normal.MASS = normal.RHOM0*normal.LENGTH^3;  %[kg]
            normal.TIME = 1/OMEGA_CI;  %[s]
            normal.CHARGE = N0*QE*normal.LENGTH^3; %[C]
            normal.ELEC = VA*B0; %[V/m]
            normal.ENERGY = M*N0*normal.LENGTH^3*VA^2; %[J]
            normal.MU0 = MU0; %[H/m]
            normal.NDEN = 1/normal.LENGTH^3; %[/m3]
            normal.TEMPERATURE = normal.LENGTH*VA*B0*normal.CHARGE; %[kT]         
        end

    end
end

