function [MU0, QE, EPS0, C0, K, AMU] = CONSTANTS
% prepare constants
EPS0 = 8.854e-12;		%permittivity of free space [F/m]
QE = 1.602e-19;			%elementary charge [C]
K = 1.381e-23;			%boltzmann constant [J/K]
AMU = 1.661e-27;		%atomic mass unit [kg]
C0 = 3e8;               %light velocity in vacuum [m/s]
MU0 = 1/C0^2/EPS0;        %magnetic permeabiltiy [N/A2]
end