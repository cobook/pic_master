function [Bext, coil1_cell, coil2_cell] = USR_COIL(dz, dr, nz0, nr0, wall_pos, current1, coilc1, current2, coilc2)
% Generate magnetic field by the assmuption of induction current inside 4 turns coil
% for the details, refer to Master thesis 3.4.6 section
%
% Input: 
% dz:        grid size in z
% dr:        grid size in r
% nz0:       number of grid in z
% nr0:       number of grid in r
% current:   total current flow in coil
% coilc:     position of the corner of the coil. First line is in z, Second
% line is in r.
%
% Output: 
% Bext:         Calculated field
% coil1_cell:   cells where coil1 is located in the form of
%              [[minz maxz];[minr maxr]]
% coil2_cell:   cells where coil2 is located in the form of
%              [[minz maxz];[minr maxr]]

turn = 4; % number of turns of the coil
MU0 = 4*pi*1e-7; 

nz1 = nz0+1;
nr1 = nr0+1;
mmeter = 1e3;

%% Convert from mm to m
coilz1 = coilc1(1,:)/mmeter + wall_pos;
coilz2 = coilc2(1,:)/mmeter + wall_pos;
coilr1 = coilc1(2,:)/mmeter;
coilr2 = coilc2(2,:)/mmeter;

%% Identify cell position and corresponding area size
coil1_cell = [round(coilz1(1)/dz+1), round(coilz1(2)/dz+1);
            round(coilr1(1)/dr+1), round(coilr1(2)/dr+1)];
coil2_cell = [round(coilz2(1)/dz+1), round(coilz2(2)/dz+1);
            round(coilr2(1)/dr+1), round(coilr2(2)/dr+1)];
coil1_area = (coil1_cell(1,2) - coil1_cell(1,1) + 1) * (coil1_cell(2,2) - coil1_cell(2,1) + 1) * (dz*dr);
coil2_area = (coil2_cell(1,2) - coil2_cell(1,1) + 1) * (coil2_cell(2,2) - coil2_cell(2,1) + 1) * (dz*dr);     

%% set up multiplication matrix for potential solver
%here we are setting up the Finite Difference stencil
A = zeros(nz1*nr1);              %allocate empty nn * nn matrix
%set regular stencil on internal nodes
for j=2:nr1-1                    %only internal nodes
    for i=2:nz1-1
        u = (j-1)*nz1+i;         %unknown (row index)
        r = dr*abs(j-1.5);
        A(u,u) = -2*(1/(dz*dz)+1/(dr*dr)) - 1./(r.*r);    %phi(i,j)
        A(u,u-1)=1/(dz*dz);     %phi(i-1,j)
        A(u,u+1)=1/(dz*dz);     %phi(i+1,j)
        A(u,u-nz1)=1/(dr*dr)-1/(r*2*dr);    %phi(i,j-1)
        A(u,u+nz1)=1/(dr*dr)+1/(r*2*dr);    %phi(i,j+1)
    end
end

%%Neumann boundary on r=Lr
for i=1+1:nz1-1
    u=(nr1-1)*nz1+i;
    A(u,u-nz1) = 1/dr;            %phi(i,j-1)
    A(u,u)     = -1/dr;              %phi(i,j)
end
%neumann boundary on z=Lz
for j=1:nr1
    u=(j-1)*nz1+nz1;
    A(u,u-1) = 1/dz;             %phi(i-1,j)
    A(u,u)   = -1/dz;             %phi(i,j)
end

% neumann boundary on z=0
for j=1:nr1
    u=(j-1)*nz1+1;
    A(u,u)   = -1/dz;
    A(u,u+1) = 1/dz;            %phi(i+1,j)
end


% neumann boundary on r=0
for i=2:nz1-1
    u=(1-1)*nz1+i;
    A(u,u+nz1) = -1/dr;             %phi(i,j+1)
    A(u,u)     = -1/dr;            %phi(i,j)
end



%% Boundary conditions matrix
box_wall = zeros(2,2,4);
box_wall(:,:,1) = [1 nz1; 1 1];
box_wall(:,:,2) = [1 round(wall_pos/dz)+1; 1 nr1];
%box_wall(:,:,2) = [1 1; 1  1];
box_wall(:,:,3) = [nz1 nz1; 1 nr1];
box_wall(:,:,4) = [1 nz1; nr1 nr1];

%%{
% Set phi=0 on boundaries
for k = [2]
    for j=box_wall(2,1,k):box_wall(2,2,k)
        for i=box_wall(1,1,k):box_wall(1,2,k)
            u=(j-1)*nz1+i;
            A(u,:)=zeros(1,nz1*nr1);     %clear row
            A(u,u)=1;               %phi(i,j)
        end
    end
end
%}

A = sparse(A);

%% Set initial conditions
phi = zeros(nz1,nr1);
val = zeros(nz1,nr1);
% Define current flow on the grid
% Coil 1
val(coil1_cell(1,1):coil1_cell(1,2),coil1_cell(2,1):coil1_cell(2,2)) = current1/(coil1_area);
% Coil2
val(coil2_cell(1,1):coil2_cell(1,2),coil2_cell(2,1):coil2_cell(2,2)) = current2/(coil2_area);


val = -val*MU0*turn;
x = eval_A_SOR(phi, A, val, nz1, nr1);

Bext = rot(x, nz0, nr0, dz, dr);

% visualize(Bext, Lz, Lr, dz, dr);
end

function curlA = rot(A, nz, nr, dz, dr)
%% Rot E
curlA = zeros(nz,nr,3);
gr1 = meshgrid(1:nr+1,1:nz+1)-1.5;
gr0 = gr1(1:end-1,1:end-1)+0.5;

% z direction
rAt = (gr1).*A;         % r*Atheta
temp = ( rAt(:,2:end) - rAt(:,1:end-1) )/dr;
drAtdr = (temp(1:end-1,:) + temp(2:end,:))/2;
curlA(:,:,1) = drAtdr./gr0;
% Note on the axis, following formula should be applied
% Bz(n+3/2) = Bz(n+1/2) - dt*(2/dr)*2*E\theta
curlA(:,1,1) = 4/dr*(A(1:end-1,2)+A(2:end,2))/2;
% r direction
temp = (A(2:end,:) - A(1:end-1,:))/dz;
dAtheta_dz = (temp(:,2:end) + temp(:,1:end-1))/2;
curlA(:,:,2) = -dAtheta_dz;

%% Boundary condition
curlA(:,1,2:3) = 0;
end

function visualize(Bext, Lz, Lr, dz, dr)
mmeter = 1e3;
figure(1)
colormap jet
z = -dz/2:dz:Lz+dz/2;
r = -dr/2:dr:Lr+dr/2;
imagesc(z*mmeter, r*mmeter, sqrt(Bext(:,:,1).^2 + Bext(:,:,2).^2)');
axis xy
colorbar;

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Potential solver for a particle-in-cell example program
% Based on the Gauss-Seidel method
%
% For more, visit http://www.particleincell.com/2010/es-pic-method/
% and http://www.particleincell.com/2011/particle-in-cell-example/
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [x, err] = eval_A_SOR(phi, A, var, nz, nr)
max_it = 80000;
tol = 1e-4;
w = 1.8;
nn = nz*nr;

%convert density and potential into column vectors
x = reshape(phi, numel(phi),1);
b = reshape(var, numel(var),1); 

%set boundaries
b(1:nz) = 0;                 %zero current on r=0;
b(nn-nz+1:nn) = 0;           %zero current on y=L;
b(nz:nz:nn) = 0;             %zero current on x=L;
b(1:nz:nn) = 0;              %zero current on x=0;


[ x, err, iter, flag ] = sor(A, x, b, w, max_it, tol);
%[ x, flag, relres, iter] = cgs(A, b, tol, max_it);
%[x, err, iter] = mex_SOR_C(full(A),x,b,w,max_it,tol); %% Non sparse A

%check if the solver converged to the specified tolerance
if (iter == max_it)
    disp('  SOR of A failed to converge!!');
end

%return solution as a nx*ny array
x=reshape(x,nz,nr);


end

function [ x, err, iter, flag ] = sor(A, x, b, w, max_it, tol)
%
% [ x, err, iter, flag ] = sor(A, x, b, w, max_it, tol)
%
% SOR Successive Over-Relaxation Method
%   This function solves linear equation systems such as Ax=b using SOR 
%   method (Successive Over-Relaxation).
%   When the relaxation scalar w=1, the method used is Gauss-Seidel.
%
% Input:
%   A - input matrix
%   x - inicial vector
%   b - vector b
%   w - relaxation scalar
%   max_it - maximum number of iterations
%   tol - tolerance
%
% Output:
%   x - solution vector
%   err - norm err estimate
%   iter - nu,ber of performed iterations
%   flag - 0 = a solution was found / 1 = no convergence for max_it
%
% Example:
%   [ x, err, iter, flag ] = sor( [.5 .125; .125 .25], zeros(2,1),
%       ones(2,1), .5, 1e4, 1e-4 )
%
% Author:	Tashi Ravach
% Version:	1.0
% Date:     08/06/2006
%

    if nargin == 3
        w = .5;
        max_it = 1e4;
        tol = 1e-4;
    elseif nargin == 4
        max_it = 1e4;
        tol = 1e-4;
    elseif nargin == 5
        tol = 1e-4;
    elseif nargin ~= 6
        error('sor: invalid input parameters');
    end
    
    flag = 0;
    iter = 0;

    norma2_b = norm(b);
    if (norma2_b == 0.0)
        norma2_b = 1.0;
    end

    r = b - A * x;
    err = norm(r) / norma2_b;
    if (err < tol)
        return
    end

    % separate A into several matrix for SOR/Gauss-Seidel
    [ M, N, b ] = matsep(A, b, w, 1);

    for iter = 1 : max_it
        x_1 = x;
        x = M \ (N * x + b); % adjust the aproximation
        %err = norm(x - x_1) / norm(x); % compute error
        err = norm(x_1 - x, 1); % compute error
        if (err <= tol) % check for convergence
            break
        end          
    end
    b = b / w; % vector b

    if (err > tol) % no convergence
        flag = 1;
    end   
    
end


function [ M, N, b ] = matsep(A, b, w, flag)
%
% [ M, N, b ] = matsep(A, b, w, flag)
%
% MATSEP Matrix Separation
%   Input matrix is splitted into several others in diferent ways depending
%   on the method to be used: Jacobi and SOR (Gauss-Seidel when w = 1)
%
% Input:
%   A - input matrix
%   x - inicial vector
%   b - vector b
%   flag - 1 = Jacobi / 2 = SOR
%
% Output:
%   M - matrix M
%   N - matrix N
%   b - vector b (modified for SOR)
%
% Author:	Tashi Ravach
% Version:	1.0
% Date:     08/06/2006
%

    if nargin ~= 4
        error('matsep: invalid input parameters');
    end
    
    [ m, n ] = size(A);
    if m ~= n
        error('matsep: input matrix A must have dimension nXn');
    end
    
    [ l, o ] = size(b);
    if l ~= n && o ~= 1
        error('matsep: input matrix b must have dimension nX1');
    end

    if (flag == 1) % separation for Jacobi
        M = diag(diag(A));
        N = diag(diag(A)) - A;
    elseif (flag == 2) % separation for SOR/Gauss-Seidel
        b = w * b;
        M =  w * tril(A, -1) + diag(diag(A));
        N = -w * triu(A,  1) + (1.0 - w) * diag(diag(A));
    end

end
