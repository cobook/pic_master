function [nv, iv, lz, lr] = eval_VOLUME(nz,nr,dz,dr,zin,rin)
    %Calculate each cell's volume size 
    %%Input
    %nz:        number of cells in z dimension
    %nr:        number of cells in r dimension
    %dz:        discrite size of a z cell
    %dr:        discrite size of a r cell
    %zin:   injection node of z coordinate
    %rin:   injection node of r coordinate
    
    %%Output
    %nv: volume of the each nodes
    %iv: volumes used for the injections
    %lz: Length of the input region in z 
    %lr: Length of the input region in r
    nv = eval_NODE_VOLUME(nz,nr,dz,dr);    
    [iv, lz, lr] = eval_INJECT_VOLUME(dz,dr,zin,rin,nv);
end

function nv = eval_NODE_VOLUME(nz,nr,dz,dr)
    nv = zeros(nz+1,nr+1);
    for i=0:nz
        for j=0:nr
            nv(i+1,j+1) = dz*( (dr*j)^2 - (dr*(j-1))^2 )*pi;
        end
    end
    nv(:,1) = nv(:,2);
    nv(:,end) = nv(:,end-1);
    %% Imagine virtual node on the axis
    % node volume is reduced so as to take the axis into account
    nv(:,2) = nv(:,2)*(1-0.25^2);
    
end

function [iv, lz, lr] = eval_INJECT_VOLUME(dz,dr,zin,rin,nv)
    %Calculate the volume for injection
    iv = 0;
    lz = 0; lr = 0;
    
    for i=zin(1):zin(2)
        lz = lz + dz;    
    end
    for j=rin(1):rin(2)
        lr = lr + dr; 
    end
    for i=zin(1):zin(2)
        for j=rin(1):rin(2)
            % interpolate node volume to cell center to get cell volume
            iv = iv + nv(i,j);
        end
    end
end