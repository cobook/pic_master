classdef Ghost < handle
    %Ghost Particle specifications
    %   Mover, injection
    % Most functions are just replicated from the 'Particle' class.
    % Please refer to Sec. 3.3.1 for the information why this class is required
    
    properties(SetAccess=private, GetAccess=public)
        % basic specification
        pos;    %[m m rad]
        v;      %[m/s]
        max_part;  %max_number of particles     
        vth;    %veocity[v/m]    
        T;      %temperature[eV] 
        QE;     %unit charge 
        M;      %mass of unit particle
        QE_M;   %acceleration coefficient denoted as QE/M
        % inherited field parameters
        dz;     %in z direction [m]
        dr;     %in r direction [m]
        nr;
        nz;
        np;
        % injection parameters
        vz_drift;    %velocity[m/s]
        vr_drift;    %velocity[m/s]
        % initialization
        init_pos;
        %injection cells
        z_inj_node; % in z direction
        r_inj_node; % in r direction 
        inject_num_per_time; % number of particles per time
        inject_volume;      % Volume where to inject
        sample_posr;    % r position dictionary for gaussian distributed injection
        GPU;
        box_weight;     % Don't let particles move where coil exists.
        
        %% depotion information
        d_i;    % indice to which cell particle belongs in z direction
        d_fi;   % particle position in z in cell numbers
        d_hz;   % d_fi-d_i
        d_j;    % indice to which cell particle belongs in r direction
        d_fj;   % particle position in r in cell numbers
        d_hr;   % d_fj-d_j
        
        %% bilinear deposition information
        % bilinearly weighted depending on the radial positions
        fz0r0;
        fz0r1;
        fz1r0;
        fz1r1;
    end
    
    methods
        function part = Ghost( dz, dr, QE, M, nr, nz)
            % Specific to ghost particles
            np = (nz-1)*(nr-1);
            Ti = 0;
            
            part.pos = zeros(np,3);
            part.v = zeros(np,3);
            part.dz = dz;
            part.dr = dr;
            part.nz = nz;
            part.nr = nr;
            part.QE = QE;
            part.vth = sqrt(2*QE*Ti/M);   
            part.np = np;
            part.T = Ti;         
            part.M = M;
            part.QE_M = QE/M;
            part.init_position();
        end
        
        function set_injection( self, inject_volume )
            % Common
            self.vz_drift = vz_drift;
            self.vr_drift = vr_drift;
            self.inject_num_per_time = inject_num_per_time;
            self.inject_volume = inject_volume;
            % velocity field
        end

        
        function init_position( self )  
            % Store initial position in 'init_pos' to return them back to
            % the center of the cells.
            % Please refer to the Sec. 3.3.1
            j = 1:self.nr-1;
            i = 1:self.nz-1;            
            fj = (-0.5 + j)*self.dr;
            fi = (-0.5 + i)*self.dz;
                       
            for k = i
                for l = j
                    % position distribution
                    self.pos(k+(l-1)*(self.nz-1),1) = fi(k);
                    self.pos(k+(l-1)*(self.nz-1),2) = fj(l);
                end
            end
            
            self.init_pos = self.pos;
        end
        
        
        function set_box_weight(self, cells)
            gap = 1; % gap which seems to be on the box cells
            factor = 1; % decide the slope of tanh
            
            zp = (- tanh( ([1:self.nz+1]' - cells(1,2) )*factor -gap ) + 1) / 2;
            zm = (  tanh( ([1:self.nz+1]' - cells(1,1) )*factor +gap ) + 1) / 2;
            rp = (- tanh( ([1:self.nr+1]  - cells(2,2) )*factor -gap ) + 1) / 2;
            rm = (  tanh( ([1:self.nr+1]  - cells(2,1) )*factor +gap ) + 1) / 2;
            mask = 1 - zp.*zm.*rp.*rm;

            self.box_weight = repmat(reshape(mask(2:end-1,2:end-1),(self.nz-1)*(self.nr-1),1),[1 1 3]);
        end           
        
        function replace( self , grid_vel)
            % Return all the particles to the center of the cell
            % Input
            % grid_vel      : The velocity which should be replaced for the
            % ghost particles in the center of the cells
           self.pos = self.init_pos;
           v_z = grid_vel(2:end-1,2:end-1,1);
           v_r = grid_vel(2:end-1,2:end-1,2);
           v_t = grid_vel(2:end-1,2:end-1,3);
           self.v = [v_z(:),v_r(:),v_t(:)];
        end
        
        function reflective_all(self)
            % !!! only the reflection at bottom of the boundary is implemented
            temp1 = sign(self.pos(:,2));
            temp2 = (temp1 == 0);
            temp = temp1 + temp2;
            self.pos(:,2) = self.pos(:,2) .* temp;
            self.v(:,2) = self.v(:,2) .* temp;
            self.v(:,3) = self.v(:,3) .* temp;
            
        end
        
        function geometry_check(self, Lz, Lr)
            %reflective boundary on bottom          %r<0
            self.reflective_all(); % 1:parallel to z, 2: parallel to r
            % Don't let the particles out of geometry
            self.pos(self.pos(:,1)<-self.dz/2,1) = -self.dz/2;
            self.pos(self.pos(:,1)>=Lz+0.5*self.dz,1) = Lz+self.dz*0.49;
            %self.pos(self.pos(:,2)<0,2) = 0;
            self.pos(self.pos(:,2)>=Lr+0.5*self.dr,2) = Lr+self.dr*0.49;
        end
        
        function kill_all(self, del_flag, np)
            if (sum(del_flag(:))>0)
                self.pos = self.pos(del_flag==0, :);
                self.v = self.v(del_flag==0, :);
                self.pos(np+1:self.max_part,:) = zeros(self.max_part-np,3);
                self.v(np+1:self.max_part,:) = zeros(self.max_part-np,3);
            end
        end
        
        function push_all(self, dt)
            % Push 1 time step
            self.pos = self.pos + self.v*dt;            
        end
  
        
        function cyclotronic_mover_all( self, EF, MF, dt)
            % Cyclotronic mover. 
            % Please refer to Sec. 3.4.2 of master thesis
            [self.pos, self.v] = mex_MOVER( EF, MF, self.pos, self.v, self.QE_M, dt);
            if isnan(sum(self.v(:)))
                disp('Error : Ghost vel contains Nan value');
                self.v(isnan(self.v)) = 0;
            end
            if ~isfinite(sum(self.v(:)))
                disp('Error : Ghost vel contains Inf value');
                self.v(~isfinite(self.v)) = 0;
            end
            

        end
        

        function set_pos(self, pos, part)
            % set positions from another Particle class
            self.pos = pos;
            self.d_i = part.d_i;
            self.d_j = part.d_j;
            self.d_fi = part.d_fi;
            self.d_fj = part.d_fj;
            self.d_hz = part.d_hz;
            self.d_hr = part.d_hr;
        end
            
        
        function set_vel(self, vel)
            self.v = vel;
        end
        
                
            
        function [fi, i, hz, fj, j, hr] = deposition2nodes(self, dz, dr)
            % deposition of particles on the surrounding cells
            % Input 
            % dz        : grid spacing in z direction
            % dr        : grid spacing in r direction            
            % Output
            % fi        : real i index of particle's cell in z direction
            % i         : integral part
            % hz        : the reminder
            % fj        : real j index of particle's cell in r direction
            % j         : integral part
            % hr        : the reminder
            fi = 1.5+self.pos(:,1)/dz;        %real i index of particle's cell
            i = floor(fi);                  %integral part
            hz = fi-i;                      %the remainder
            
            fj = 1.5+self.pos(:,2)/dr;        %real j index of particle's cell
            j = floor(fj);                  %integral part
            hr = fj-j;                      %the remainder
                        
            self.d_fi = fi;
            self.d_i = i;
            self.d_hz = hz;
            self.d_fj = fj;
            self.d_j = j;
            self.d_hr = hr;
            %% Bilinear functions
            self.bil_weight();
        end
        
        function bil_weight(self)
            %% Refer to "Plasma Physics via computer simulation, pp 336
            % 14-11 Weighting in cylindrical coorinates for particles and
            % fields Written by Birdsall
            j = self.d_j(1:self.np);
            hz = self.d_hz(1:self.np);
            hr = self.d_hr(1:self.np);
            
            rj0_2 = (j - 1.5).^2;        % Cell to which particle belongs
            rj1_2 = (j - 0.5).^2;        % referenced cell for bilinear weighting
            rji_2 = (j - 1.5 + hr).^2;
            high = rj1_2 - rji_2;
            low  = rji_2 - rj0_2;
            denom = rj1_2 - rj0_2;
            %% Boundary on r=0
            idx = (denom ==0);
            denom(idx) = 0.5^2;
            high(idx)  = 0.5^2 -rji_2(idx);
            low(idx)   = rji_2(idx);
            %% Register values
            self.fz0r0 = ( high ).* (1-hz)./denom;
            self.fz0r1 = ( low  ).* (1-hz)./denom;
            self.fz1r0 = ( high ).* hz./denom;
            self.fz1r1 = ( low  ).* hz./denom;
        end        
        
        function [EF, MF] = ghost_field_weight(self,ef,mf)
            ef_temp = ef.fetch();
            mf_temp = mf.exp_interp();
            EF = reshape(ef_temp(2:end-1,2:end-1,:),(self.nz-1)*(self.nr-1),3);
            MF = reshape(mf_temp(2:end-1,2:end-1,:),(self.nz-1)*(self.nr-1),3);
        end
        
        function set_GPU( self, flag)
            self.GPU = flag;
        end
    end
    
end

