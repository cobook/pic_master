function plot_ANGULAR_CURRENT()
%close all
addpath('../src');
header = '0724';
every = 100;
end_num = 60000;
cup_pos = [ 30];




for k=[2:3]
    switch k
        %% resistivity change
  
        case 2
            folder = 'result0713_1857_TE0_COIL1.502_6_3_1_CRNT4000_MZ0.98955_WITHPERT_RSTY1e-05'
        case 3
            folder = 'result0714_1252_TE0_COIL1.502_6_3_1_CRNT2000_MZ0.4909_WITHPERT_RSTY1e-05'
        case 4
            folder = 'result0714_1730_TE0_COIL1.502_6_6_1_CRNT1800_MZ0.49883_WITHPERT_RSTY1e-05'
        case 5
            folder = 'result0715_1601_TE0_COIL1.502_6_6_1_CRNT3600_MZ1.0056_WITHPERT_RSTY1e-05'
        case 6
            folder = 'result0717_0139_TE0_COIL1.502_6_6_1_CRNT5400_MZ1.5113_WITHPERT_RSTY1e-05'
        case 7
            folder = 'result0717_1643_TE0_COIL1.502_12_6_1_CRNT4500_MZ0.49149_WITHPERT_RSTY1e-05'
        case 8
            folder = 'result0719_0120_TE0_COIL1.502_12_6_1_CRNT9000_MZ0.98853_WITHPERT_RSTY1e-05'
        case 9
            folder = 'result0720_0329_TE0_COIL1.502_6_3_1_CRNT4000_MZ0.98955_WITHPERT_RSTY1e-06'
        case 10
            folder = 'result0720_0330_TE0_COIL1.502_6_3_1_CRNT4000_MZ0.98955_WITHPERT_RSTY0.0001'
        case 11
            folder = 'result0721_0009_TE0_COIL1.502_12_6_1_CRNT13500_MZ1.4855_WITHPERT_RSTY1e-05'
        case 12
            folder = 'result0722_1941_TE0_COIL1.502_6_3_1_CRNT6000_MZ1.4871_WITHPERT_RSTY1e-05'
        case 13 % Current 0
            folder = 'result0724_0057_TE0_COIL1.502_6_3_1_CRNT0_MZ0_WITHPERT_RSTY1e-05';
    
    end

        
        %    folder = fgetl(fid)
        addpath(strcat('result/',folder));
        for p = cup_pos
            % try
            K = load('matlab.mat');
            K.Ti
            K.v_drift
            K.ratio
            wall_pos = K.wall_pos;
            plot_child(folder, end_num, p, every, wall_pos, header,k);
            %catch exception
            disp(strcat('Exeption ocuured : ', folder));
            disp('If you do not process sucessive results, debug it');
            %end
        end
        rmpath(strcat('result/',folder));
    end
end






function plot_child(folder, end_num,cup_pos, every, wall_pos, header,k_temp)
%%
load('matlab.mat');
load('normal.mat');
load(strcat('np_temp',num2str(1,'%08d'),'.mat'));
dz = dz*normal.LENGTH;
dr = dr*normal.LENGTH;
Lz = Lz*normal.LENGTH;
Lr = Lr*normal.LENGTH;
dt = dt*normal.TIME;
%% Faraday cup position
r = ((cup_pos*1e-3))/(dz);
%every = 100;
start_num = every;
[T] = [start_num:every:end_num]'*dt;
[R,Z] = meshgrid(0:dr:Lr+dr, 0:dz:Lz+dz);
theta = [0:10:50]*2*pi/360;
[tx, ty] = size(theta);
%% Loop to store current density
sj = zeros(max(size(T)),max(size(theta)));

j=0;

for num=[start_num:every:end_num]
    j=j+1;
    loadstr=strcat('J_temp',num2str(num,'%08d'),'.mat');
    
    load(loadstr);
    
    m = round(r*cos(theta) + wall_pos/dz) +1;
    n = round(r*sin(theta)            ) +1;
    J_temp = J_temp*normal.VEL*normal.CHARGE*normal.NDEN;
    for k = 1:ty
        idx = 0;
        for a = m(k):m(k)
            l=0;
            for b = n(k):n(k)
                l=l+1;
                idx = idx+1;
                curtemp(idx) = (n(k) - 3 + 2*l)*(J_temp(a,b,1)*cos(theta(k)) + J_temp(a,b,2)*sin(theta(k)));
            end
        end
        sj(j,k) = sum(curtemp)/(n(k));
        if k==1
            % add plus two to avoid singularity
            temp1 = J_temp(m(k)  ,n(k)+2  ,1);
            temp2 = J_temp(m(k)  ,n(k)+3,1);
            temp3 = J_temp(m(k)+1,n(k)+2,  1);
            temp4 = J_temp(m(k)+1,n(k)+3,1);
            
            
            sj(j,k) = ((temp1 + temp3) + (temp2 + temp4))/4;
            
        end
        
    end
    
    
    %% Sum of current calculated here
    %{
    slice = R.*R*pi;
    area  = slice(:,2:end) - slice(:,1:end-1);
    CUR = area.*J_temp(:,2:end,1);
    CUR = CUR(round(r),1:7);
    %}
    
end

save('sj','sj');

fsize = 35;
f = figure('visible','off');
set(gca, 'FontSize', fsize*0.8);
%f = figure(1);
cm2 = 1e4;
us = 1e6;
sj = imfilter(sj,fspecial('average',[25 1]));
p = plot(repmat((T+drift_t)*us, size(theta)),sj/cm2);
set(p,'LineWidth',4);
set(gca, 'FontSize',fsize*0.8)
xlim([0 5.8]);
if cup_pos == 30
ylim([-1 8]);
%ylim([-1 12]);

elseif cup_pos == 20
ylim([-1 20*5.5]);
end
%ylim([0 15e4]);
titlestr = strcat('CUP', num2str(cup_pos), ' Te', num2str(Te));
%title(titlestr,'FontSize',fsize);
xlabel('\bf Time [$\mu$s]','FontSize',fsize, 'Interpreter', 'LaTeX');
ylabel('\bf Current density [A/cm$^2$]','FontSize',fsize, 'Interpreter', 'LaTeX');
legend({'\bf 0 deg.','\bf 10 deg.','\bf 20 deg.','\bf 30 deg.','\bf 40 deg.','\bf 50 deg.'},'FontSize',fsize*0.7, 'Interpreter', 'LaTeX');
grid on

SaveImage(f, 1000,800, strcat('injecttest/CASE',num2str(k_temp,'%08d'),'_', header, titlestr));


end

function [] = SaveImage(figure, width, height, filename)
set(figure, 'PaperPositionMode', 'auto');
position = get(figure, 'Position');
position(3) = width;
position(4) = height;
set(gcf,'Position', position);
set(gcf, 'PaperSize', [20 17]);
saveas(figure, filename, 'pdf');
end