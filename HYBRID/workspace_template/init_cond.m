function init_cond(crnt)
%% Coil parameters 
%crnt = 1000;                % Current flow inside coil [A]
coil_pos = [1.5, 6];        % Coil position from the origin (z, r)[mm]
coil_length = [3.0, 1.0];   % Coil length from the left down edge of the coil specified by coil_pos (z,r)[mm]

%% Add paths to the source files
addpath('Input');
addpath('../src');
addpath('../mexfunctions');

for current = crnt
    % main routine    
    main_func(coil_pos, coil_length, current); 
end
end
