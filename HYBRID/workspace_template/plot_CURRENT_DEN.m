function plot_CURRENT_DEN
folder = 'result0117_1733_82mT_-5mm';

it = 2500;
colorflag = 0;
plot_child(it,colorflag,folder)

end




function plot_child(it_temp, colorflag,folder)
addpath(folder);
load('matlab.mat');
it = it_temp;

load('normal.mat');

A = load(strcat('pos',num2str(it),'.mat'));
np = 50000
[J_ion, n_ion] = FLUX_w( GPUFLAG, BOUNDARY, 'a', nz, nr, np, part_old, dz, dr, node_volume, spwt, A.pos_temp);
n_ion*normal.NDEN

dt*normal.TIME
MU0 = normal.MU0;

pos1 = A.pos_temp*normal.LENGTH;
B = load(strcat('mf',num2str(it),'.mat'));


dz = dz*normal.LENGTH;
dr = dr*normal.LENGTH;
Lz = Lz*normal.LENGTH;
Lr = Lr*normal.LENGTH;
[R,Z] = meshgrid(0:dr:Lr, 0:dz:Lz);

B = load('mf1.mat');
mf_ext = B.mf_temp*normal.B0;
load(strcat('mf',num2str(it),'.mat'));
mf_temp = mf_temp*normal.B0;

Bp = mf_temp - mf_ext;

%if TIME > z_inject/v_drift
%win = 220;
f = figure(40000);
clf;
%clf(win);
%subplot(2,1,1)



set(gca,'FontSize',15);

switch colorflag
    case 1
        X = load('pos1.mat');
        %        id1 = load('part_id1.mat');
        id2 = load(strcat('part_id',num2str(it),'.mat'));
        radius = 1e-3;
        c = sqrt((3e-3 - X.pos_temp(:,1)*normal.LENGTH).^2 + (0 - X.pos_temp(:,2)*normal.LENGTH).^2) / radius;
        colormap default
        %flag =
        c = c(id2.part_id);
    case 0
        c = 'g';
end
%NP = sum(pos1~=0);
%NP > 10
NP=20000;

mmeter = 1e3;
%hand = scatter(pos1(1:NP,1)*mmeter,pos1(1:NP,2)*mmeter,1,c,'filled');%, 'g.');
%set(hand, 'MarkerSize', 1);
hold on
Z = Z*mmeter;
R = R*mmeter;
%n_ion = padarray(n_ion, [1 1]);
n_int = 0.25*(n_ion(1:end-1,1:end-1)+n_ion(2:end,1:end-1)+n_ion(1:end-1,2:end)+n_ion(2:end,2:end));
contour(Z',R',n_int'*normal.NDEN);
axis xy
colorbar;
streamslice(Z', R', mf_ext(:,:,1)', mf_ext(:,:,2)');
colorbar;
ofs = 0.019*mmeter;
axis([0, Lz*mmeter - ofs, 0, Lr*mmeter]);
xlabel('Position in z [mm]','FontSize',14);
ylabel('Position in r [mm]','FontSize',14);
%title(strcat('Magnetic field [Wb/m2] iteration=', num2str(it)), 'FontSize',15);%, ' Particle=', num2str(np) ));
grid on
%contour(0-dz/2:dz:Lz+dz/2,0-dr/2:dr:Lr+dr/2,object');
hold off
%saveas(f,strcat('result/Bp',num2str(it)),'fig');

rmpath(folder);

end



function [fi, i, hz, fj, j, hr] = deposition2nodes(pos, dz, dr)

fi = 1.5+pos(:,1)/dz;        %real i index of particle's cell
i = floor(fi);                  %integral part
hz = fi-i;                      %the remainder

fj = 1.5+pos(:,2)/dr;        %real j index of particle's cell
j = floor(fj);                  %integral part
hr = fj-j;                      %the remainder
end


function [J_filtered, n_filtered, vel_filtered] = FLUX_w( GPUFLAG, BOUNDARY, TYPE, nz, nr, np, part, dz, dr, node_volume, spwt,pos)
% FLUX WEIGHTING
% linear weighting in z direction
% bilinear weighting in r direction
% l: left, r: right, t: top, b: bottom

% after calculating raw value of number density,
% apply filter so that the result doesn't diverge

% boundaries are expanded one line for each side with the value of side
% line

% weight parameters are decided according to the equation
%                    M*phi(center) + 4S*phi(side) + 4K*phi(corner)
% phi(filtered) = -------------------------------------------------
%                                M+4S+4K

% Weight parameters
M = 20; % Main
S = 2;  % Side
K = 1;  % Corner
denom = M+4*S+4*K;

% The domain length is larger than magnetic field by 1
nz = nz + 1;
nr = nr + 1;

% Import particle position information
[fi, i, hz, fj, j, hr] = deposition2nodes( pos, dz, dr );

% USE only present particles for time-reductions
i = i(1:np);
j = j(1:np);
hz = hz(1:np);
hr = hr(1:np);

%% Weighting functions
% weight cylindrically ( bilinearly )
%{
    lb = (1-hz) .* (j.^2 - (j -1.5 + hr).^2) ./ (j.^2 - (j-1.5).^2);
    rb = hz .* (j.^2 - (j -1.5 + hr).^2) ./ (j.^2 - (j-1.5).^2);
    lt = (1-hz) .* ((j - 1.5 + hr).^2 - (j-1.5).^2) ./ (j.^2 - (j-1.5).^2);
    rt = hz .* ((j - 1.5 + hr).^2 - (j-1.5).^2) ./ (j.^2 - (j-1.5).^2);
if strcmp(TYPE, 'a')
    lb = lb*spwt;
    rb = rb*spwt;
    lt = lt*spwt;
    rt = rt*spwt;
elseif strcmp(TYPE, 'g')
    % The case of ghost particles
    weight = spwt(2:end-1,2:end-1);
    weight = weight(:);
    
    lb = lb.*weight;
    rb = rb.*weight;
    lt = lt.*weight;
    rt = rt.*weight;
end
    
%}
%% linear weighting
%%{
if strcmp(TYPE, 'a')
    %the case of actual particles
    lb = (1-hz).*(1-hr)*spwt;
    rb = hz.*(1-hr)*spwt;
    lt = (1-hz).*hr*spwt;
    rt = hz.*hr*spwt;
    
elseif strcmp(TYPE, 'g')
    % The case of ghost particles
    weight = spwt(2:end-1,2:end-1);
    weight = weight(:);
    
    lb = (1-hz).*(1-hr).*weight;
    rb = hz.*(1-hr).*weight;
    lt = (1-hz).*hr.*weight;
    rt = hz.*hr.*weight;
end
%}

%% cell nodes set for using accumarray
cell_idx = [i+nz*(j-1); i+1+nz*(j-1);  i+nz*j;  i+1+nz*j];
cell_weight = [ lb;
    rb;
    lt;
    rt];
flux_weight = cell_weight(:,ones(3,1)).*repmat(part.v(1:np,:),[4 1]);
chg = accumarray(cell_idx,cell_weight,[nz*nr, 1], [], 0);

if (np~=0)
    chg = reshape(chg, [nz nr] );
end

accum_idx = [cell_idx, cell_idx+nz*nr, cell_idx+2*nz*nr];

flux = accumarray(accum_idx(:), flux_weight(:), [nz*nr*3 1], [], 0);
flux = reshape(flux, [nz, nr, 3]);


%% Node expansion for the ease of filtering

[chg, flux] = copy_cells(BOUNDARY, chg, flux);

n = eval_number_density( chg, node_volume );
J = eval_current_density( flux, node_volume, part.QE );

if (S==0 && K==0)
    n_filtered = n;
    J_filtered = J;
    vel_filtered = J_filtered./repmat(n_filtered,[1, 1, 3])./part.QE;
    return
end

%% GPU utilization configuration
if (GPUFLAG>=1)
    gpu = gpuDevice;
    rows = gpuArray.colon(1,nz)';
    cols = gpuArray.colon(1,nr);
    vrows = gpuArray.colon(1,nz)';
    vcols = gpuArray.colon(1,nr);
    dims = reshape(gpuArray.colon(1,3),[1 1 3]);
    %vel = gpuArray(vel);
    n = gpuArray(n);
    J = gpuArray(J);
else
    [cols, rows] = meshgrid(1:nr, 1:nz);
    [vcols, vrows, dims] = meshgrid(1:nr, 1:nz, 1:3);
end

%% stencil calculation for arrayfunction
% thse functions needs to be declared inside function
    function out = J_filter(row, col, dim)
        if (row~=1 && row~=nz && col~=1 && col~=nr)
            % name index value
            up = row-1;
            down = row+1;
            left = col-1;
            right = col+1;
            % Side components
            rowsum = J(up,col,dim) + J(down,col,dim);
            colsum = J(row,left,dim) + J(row,right,dim);
            % Corner components
            cornersum = J(up,left,dim) + J(up,right,dim) ...
                + J(down,left,dim) + J(down,right,dim);
            
            out = (J(row,col,dim)*M + (rowsum + colsum)*S + cornersum*K)/denom;
        else
            out = NaN;
        end
    end

    function out = n_filter(row,col)
        if (row~=1 && row~=nz && col~=1 && col~=nr)
            % name index value
            up = row-1;
            down = row+1;
            left = col-1;
            right = col+1;
            % Side components
            rowsum = n(up,col) + n(down,col);
            colsum = n(row,left) + n(row,right);
            % Corner components
            cornersum = n(up,left) + n(up,right) ...
                + n(down,left) + n(down,right);
            
            out = (n(row,col)*M + (rowsum + colsum)*S + cornersum*K)/denom;
        else
            out = NaN;
        end
    end

%% Filter chg and flux
n_filtered = arrayfun(@n_filter, rows, cols);
if(GPUFLAG>=1)
    wait(gpu);
    n_filtered = gather(n_filtered);
end

J_filtered = arrayfun(@J_filter, vrows, vcols, dims);
if(GPUFLAG>=1)
    wait(gpu);
    J_filtered = gather(J_filtered);
end

%% Convert to number density and current
% Avoid singular point
[n_filtered, J_filtered] = copy_cells(BOUNDARY, n_filtered, J_filtered);

%% Velocity should be derived using number density and current
vel_filtered = J_filtered./repmat(n_filtered,[1, 1, 3])./part.QE;

end

function n_ion = eval_number_density( chg,node_volume)
%EVAL_NUMBER_DENSITY
% evaluate the value of number density
n_ion = chg./node_volume;
end

function J_ion = eval_current_density( flux,node_volume, Ze)
%EVAL_CURRENT_DENSITY
% evaluate the value of number density
J_ion = Ze*flux./repmat(node_volume, [1,1,3]);

end
function [n,J] =  copy_cells(BOUNDARY, n, J)
if strcmp(BOUNDARY, 'a')
    % Copy cells assuming neumann boundary
    n(:,1) = n(:,2);
    n(:,end) = n(:,end-1);
    n(1,:) = n(2,:);
    n(end,:) = n(end-1,:);
    
    J(:,1,1) =  J(:,2,1);
    J(:,1,2) = -J(:,2,2);
    J(:,1,3) = -J(:,2,3);
    J(:,end,:) = J(:,end-1,:);
    J(1,:,1) = - J(2,:,1);
    J(1,:,2:3) = J(2,:,2:3);
    J(end,:,1) = - J(end-1,:,1);
    J(end,:,2:3) = J(end-1,:,2:3);
elseif strcmp(BOUNDARY, 'p')
    if isnan(n(1,1))
        % Copy cells assuming periodic boundary
        n(:,1) = n(:,2);
        n(:,end) = n(:,end-1);
        n(1,:) = n(end-1,:);
        n(end,:) = n(2,:);
        
        J(:,1,1) =  J(:,2,1);
        J(:,1,2) = -J(:,2,2);
        J(:,1,3) = -J(:,2,3);
        J(:,end,:) = J(:,end-1,:);
        J(1,:,:) = J(end-1,:,:);
        J(end,:,:) = J(2,:,:);
    else
        % Copy cells assuming periodic boundary
        n(:,1) = n(:,2);
        n(:,end) = n(:,end-1);
        n(2,:) = n(2,:) + n(end,:);
        n(end-1,:) = n(end-1,:) + n(1,:);
        n(1,:) = n(end-1,:);
        n(end,:) = n(2,:);
        
        J(:,1,1) =  J(:,2,1);
        J(:,1,2) = -J(:,2,2);
        J(:,1,3) = -J(:,2,3);
        J(:,end,:) = J(:,end-1,:);
        J(2,:,:) = J(2,:,:) + J(end,:,:);
        J(end-1,:,:) = J(end-1,:,:) + J(1,:,:);
        J(1,:,:) = J(end-1,:,:);
        J(end,:,:) = J(2,:,:);
    end
end
end
