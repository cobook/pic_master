function plot_TRAJECTORY()
clear all
close all
prettify_figure_presen();

fsize = 35;
header = '0723';
%% Finish when the cross section of v_drift reaches zreach
every = 1000; %% The interval of data fetched
end_it = 60000;

%% Axial Cross section of interest
vd = [1.0e4, 1.5e4, 2.0e4];
dt = 0.05e-6;

cz = vd*dt;
%% Voloring order
cm = [[0 0 0]; % Black 1
    [1 1 1]; % white 2
    [1 0 0]; % red 3
    0.7*[0 1 0]; % green 4
    [0 0 1]; % blue 5
    0.8*[1 1 0]; % yellow 6
    0.7*[1 0 1]; % red purple 7
    [0.5 0 0]; % dark red 8
    [0.49 0.8 1] % aqua marine 9
    ];
clist = [cm(5,:);cm(3,:);cm(6,:);cm(7,:);cm(4,:)];

%% Which date to use
%for k_temp = [5:12]
%for k_temp = [2:3,5:13]
for k_temp = 12   
    switch k_temp
        case 1

        case 2
            folder = 'result0713_1857_TE0_COIL1.502_6_3_1_CRNT4000_MZ0.98955_WITHPERT_RSTY1e-05'
            every = 2000;
        case 3
            folder = 'result0714_1252_TE0_COIL1.502_6_3_1_CRNT2000_MZ0.4909_WITHPERT_RSTY1e-05'
            every = 1000;
        case 4
            folder = 'result0714_1730_TE0_COIL1.502_6_6_1_CRNT1800_MZ0.49883_WITHPERT_RSTY1e-05'
        case 5
            folder = 'result0715_1601_TE0_COIL1.502_6_6_1_CRNT3600_MZ1.0056_WITHPERT_RSTY1e-05'
        case 6
            folder = 'result0717_0139_TE0_COIL1.502_6_6_1_CRNT5400_MZ1.5113_WITHPERT_RSTY1e-05'
        case 7
            folder = 'result0717_1643_TE0_COIL1.502_12_6_1_CRNT4500_MZ0.49149_WITHPERT_RSTY1e-05'
        case 8
            folder = 'result0719_0120_TE0_COIL1.502_12_6_1_CRNT9000_MZ0.98853_WITHPERT_RSTY1e-05'
        case 9
            folder = 'result0720_0329_TE0_COIL1.502_6_3_1_CRNT4000_MZ0.98955_WITHPERT_RSTY1e-06'
        case 10
            folder = 'result0720_0330_TE0_COIL1.502_6_3_1_CRNT4000_MZ0.98955_WITHPERT_RSTY0.0001'
        case 11
            folder = 'result0721_0009_TE0_COIL1.502_12_6_1_CRNT13500_MZ1.4855_WITHPERT_RSTY1e-05'
        case 12
            folder = 'result0722_1941_TE0_COIL1.502_6_3_1_CRNT6000_MZ1.4871_WITHPERT_RSTY1e-05'            
    end


addpath('../src');
addpath(strcat('result/', folder));

load('normal.mat');
load('matlab.mat');




f = figure(30);
p1= plot(0,0,0,0,0,0);

hold on


%% Just to show current magnetic field intensity
B = load('mf_ext.mat');
mf_ext = B.mf_ext*normal.B0;
allmf_ext = mf_ext(:,1,1);
maxmf = max(allmf_ext)
%% Initial particle list
load('pos_temp_ini00000000.mat');
pos1 = pos_temp_ini*normal.LENGTH;
load('part_id_ini00000000.mat');
id1 = part_id_ini;
mmeter = 1e3;


%{
f2 = figure(20);
plot((pos1(1:10000,1)-wall_pos)*mmeter,pos1(1:10000,2)*mmeter,'.')
xlabel('\bf Longitudinal axis [mm]', 'FontSize', fsize);
ylabel('\bf Radius [mm]', 'FontSize', fsize);
grid on

SaveImage(f2, 1200, 800, strcat('seminar/', header, 'CASE', num2str(k_temp, '%06u'), 'TRAJECTORY_INIT'));
%}

for k = [1:3]
%    set(f1(k),'marker','none','color',clist(k,:));    
    color_temp = clist(k,:);    
    f = TRAJECTORY(normal, folder, color_temp, cz(k), pos1, wall_pos, id1, v_drift, dt,dz, dr, Lz, Lr, coil1_cell, end_it, every);    
end


hold off
%title(strcat('Trajectories, ',blanks(1), num2str(mT),'mT'), 'FontSize', 15);
xlabel('\bf Axial position [mm]', 'FontSize', fsize);
ylabel('\bf Radius [mm]', 'FontSize', fsize);
l = legend({'\bf Rear', '\bf Center', '\bf Front'});
l.FontSize = fsize;
l.Location = 'northwest';

SaveImage(f, 1200, 800, strcat('seminar/', header, 'CASE', num2str(k_temp, '%06u'), 'TRAJECTORY'));

rmpath(folder);
    
end
end

function [ f1 ] = TRAJECTORY( normal, folder, color, cz, pos1, wall_pos, id1, v_drift, dt, dz, dr, Lz, Lr, coil1_cell, end_it, every)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    fsize = 35;

    
    %% Criteria for tracking
    eps = 1e-5; % Allowable error
    for cr = [2e-4:3e-4:8e-4]
        flag1 = (cz <= (pos1(:,1) - wall_pos));
        flag2 = (pos1(:,1)-wall_pos) < (cz+eps);
        flag3 = cr <= pos1(:,2);
        flag4 = pos1(:,2) < (cr+eps);
        flag = flag1.*flag2.*flag3.*flag4;
        idx = id1(flag==1);
idx(1)

         end_it = 40000;
        %%
        k = 0;
        for it = [1,every:every:end_it]
            try
                load(strcat('part_id',num2str(it,'%08d'),'.mat'));
                load(strcat('pos_temp',num2str(it, '%08d'),'.mat'));
                temp = (part_id == idx(1));
                if sum(temp) == 1
                    k = k + 1;
                    pos_it = pos_temp(temp,:);
                    dpos(k,:) = pos_it(1,:)*normal.LENGTH;
                else 
                    break;
                end
                
            catch exception
                disp('exception occured');
                break
            end
        end
        
        
        
        f1 = figure(30);
        size(dpos)
 
        posz = dpos(1:k,1) - wall_pos;

        posr = dpos(1:k,2);
        mmeter = 1e3;
        
%        xx = 0:.25e-3:max(posz);
%        yy = spline(posz,posr,xx);        
%        h1 = plot(posz*mmeter,posr*mmeter, 'o', xx*mmeter, yy*mmeter);
        h1 = plot(posz*mmeter,posr*mmeter);
        set(h1,'LineWidth',3);
        set(h1,'marker','none');
        set(h1,'color',color);
        hold on
        grid on        

        %% The realm of the simulation domain
        xlim([0,0.030]*mmeter);
        ylim([0,0.020]*mmeter);
    end
    drawnow
    
    [X,Y] = meshgrid([-dz/2:dz:Lz+dz/2]-wall_pos,[-dr/2:dr:Lr+dr/2]);
    Z = zeros(size(X));
    cell1 = coil1_cell;
    Z(cell1(2,1):cell1(2,2), cell1(1,1):cell1(1,2)) = 1;
    contour(X*mmeter,Y*mmeter,Z, 1, 'LineWidth', 2);
    
end

function [] = SaveImage(figure, width, height, filename)
set(figure, 'PaperPositionMode', 'auto');
position = get(figure, 'Position');
position(3) = width;
position(4) = height;
set(gcf,'Position', position);
set(gcf, 'PaperSize', [width/100+2 height/100+2]);
saveas(gcf, filename, 'pdf');
end


