function plot_DIAGNOSIS_new
prettify_figure_presen()
%% Filename header
close all
header = '0723';
addpath('../src');
for k = [12:13]   
    
%    %for k = [-1 0 1 2]
    every = 2000;
    it_start = 2000;
    it_end = 60000;



    switch k
        %% resistivity change
  
        case 2
            folder = 'result0713_1857_TE0_COIL1.502_6_3_1_CRNT4000_MZ0.98955_WITHPERT_RSTY1e-05'
        case 3
            folder = 'result0714_1252_TE0_COIL1.502_6_3_1_CRNT2000_MZ0.4909_WITHPERT_RSTY1e-05'
        case 4
            folder = 'result0714_1730_TE0_COIL1.502_6_6_1_CRNT1800_MZ0.49883_WITHPERT_RSTY1e-05'
        case 5
            folder = 'result0715_1601_TE0_COIL1.502_6_6_1_CRNT3600_MZ1.0056_WITHPERT_RSTY1e-05'
        case 6
            folder = 'result0717_0139_TE0_COIL1.502_6_6_1_CRNT5400_MZ1.5113_WITHPERT_RSTY1e-05'
        case 7
            folder = 'result0717_1643_TE0_COIL1.502_12_6_1_CRNT4500_MZ0.49149_WITHPERT_RSTY1e-05'
        case 8
            folder = 'result0719_0120_TE0_COIL1.502_12_6_1_CRNT9000_MZ0.98853_WITHPERT_RSTY1e-05'
        %{
        case 9
            folder = 'result0720_0329_TE0_COIL1.502_6_3_1_CRNT4000_MZ0.98955_WITHPERT_RSTY1e-06'
        case 10
            folder = 'result0720_0330_TE0_COIL1.502_6_3_1_CRNT4000_MZ0.98955_WITHPERT_RSTY0.0001'
%}        
        case 11
            folder = 'result0721_0009_TE0_COIL1.502_12_6_1_CRNT13500_MZ1.4855_WITHPERT_RSTY1e-05'
        case 12
            folder = 'result0722_1941_TE0_COIL1.502_6_3_1_CRNT6000_MZ1.4871_WITHPERT_RSTY1e-05'
        case 13 % Current 0
            folder = 'result0724_0057_TE0_COIL1.502_6_3_1_CRNT0_MZ0_WITHPERT_RSTY1e-05';
        
        %%  8/1 additional
        case 14
            folder = 'result0731_2029_TE0_COIL4.502_6_3_1_CRNT1500_MZ0.50362_WITHPERT_RSTY0.0001';
        case 15
            folder = 'result0731_2029_TE0_COIL4.502_6_3_1_CRNT3000_MZ1.0157_WITHPERT_RSTY0.0001';
        case 16
            folder = 'result0731_2030_TE0_COIL4.502_6_3_1_CRNT4500_MZ1.5266_WITHPERT_RSTY0.0001';            
    end
    

        %zaxis = 0;
        colorflag = 1;
        
   for it = [it_start:every:it_end]
       %for it = [1,30000]
       
   %for it = 150000
   fprintf('iteration=%d\n\n',it);        
           addpath(strcat('result/',folder));
       %try
            %% Show Current density and Profile at the same time
            %radius = 6e-4;
            radius = 1.2e-3;
            plot_child(it,colorflag,folder, radius, k, header, every);
            drawnow;
            
        %catch exception
            rmpath(strcat('result/',folder));
        %    break
        %end
    end
end
end




function plot_child(it_temp, colorflag, folder, radius, k_temp, header, every)
load('matlab.mat');
load('normal.mat');
% Forgot to set v_drift value.
% experimental value is 20000
v_drift = 6000;
zaxis = v_drift*drift_t;
%zaxis = 0.3;
it = it_temp;
MU0 = normal.MU0*1.26e-6;

A = load(strcat('pos_temp',num2str(it,'%08d'),'.mat'));
pos1 = A.pos_temp*normal.LENGTH;
C = load(strcat('vel_temp',num2str(it,'%08d'),'.mat'));
vel1 = C.vel_temp*normal.VEL;


load('normal.mat');
dz = dz*normal.LENGTH;
dr = dr*normal.LENGTH;
Lz = Lz*normal.LENGTH;
Lr = Lr*normal.LENGTH;
dt = dt*normal.TIME;


load(strcat('mf_ext.mat'));
mf_ext = mf_ext*normal.B0;
load(strcat('mf_temp',num2str(it,'%08d'),'.mat'));
mf_temp = mf_temp*normal.B0;

Bp = mf_temp - mf_ext;

mmeter = 1e3;
NP = sum(pos1(:,1)~=0);
if NP > 20000
    NP=20000;
end


switch colorflag
    case 1
        
      switch 0
            case 0
             % Radius   
                id2 = load(strcat('part_id',num2str(it, '%08d'),'.mat'));        
                X = load(strcat('pos_temp_ini',num2str(0,'%08d'),'.mat'));
                % radius = 1e-3;
                X.pos_temp_ini(:,1) = X.pos_temp_ini(:,1) - wall_pos;
                c = sqrt((((zaxis - X.pos_temp_ini(:,1))*normal.LENGTH)/radius).^2 + ((0 - X.pos_temp_ini(:,2)*normal.LENGTH)/(radius)).^2);
                c(c>1) = 1;
                c = c(id2.part_id);
                rflag = (X.pos_temp_ini(:,1) > 6000*dt);
                rflag = rflag(id2.part_id);
            case 1
              % r-velocity
                X = load(strcat('vel_temp', num2str(it, '%08d'), '.mat'));
                Y = load(strcat('pos_temp', num2str(it, '%08d'), '.mat'));
                xvel = X.vel_temp(:,2).*(Y.pos_temp(:,2) < 15e-3);
                thre = max(max(xvel, abs(min(xvel))));
                c = X.vel_temp(:,2)/2/thre + thre/2;
        end

        colormap jet
    case 0
        c = 'g';
end



%{
Bp_exp = field_expand(Bp);
Bp_exp(1,:,1) = Bp_exp(2,:,1) - Bp_exp(2,:,2);
Bp_exp(1,:,2) = Bp_exp(2,:,2) + Bp_exp(1,:,1);
Bp_exp(end,:,1) = Bp_exp(end-1,:,1) + Bp_exp(end-1,:,2);
Bp_exp(end,:,2) = Bp_exp(end-1,:,2) - Bp_exp(end-1,:,1);

curlBp = cylindrical_ROT(Bp_exp, dr,dz,nr+1,nz+1);
J = curlBp/MU0;
%}
load(strcat('cB_temp',num2str(it, '%08d'),'.mat'));
J = cB_temp/MU0;

%% Current density
%PLOT_CURRENT_DENSITY(dz,dr, Lz,Lr, J, mmeter, it, k_temp, header, wall_pos);
%% Profile
%PLOT_PROFILE( c, pos1, NP, it, dr, dz, Lr, Lz, mf_temp, k_temp, header, wall_pos, vel1);
%% Show phase diagram
%%%don't use any more%%%%%PLOT_SLICE(normal, dt, it, pos1, vel1, c, flag, k_temp, header, wall_pos, Lr);
PLOT_PHASE(normal, dt, it, pos1, vel1, c, rflag, k_temp, header, wall_pos, Lr, Lz);
%% EVALUATE EMITTANCE within a certain angle
%{
p_weight = nump/max_part;
for L = [1.2]
    for angle = [2.5 5]
        EVAL_EMITTANCE(normal, dt, it, pos1, vel1, c, rflag, k_temp, header, wall_pos, Lr, Lz, L, angle, p_weight);
    end
end
%}
%% Number density
%PLOT_NDEN(it, folder, k_temp, every, header, dz, dr, Lz, Lr, normal, dt, wall_pos)
%% Force on each clice
%PLOT_SLICE_FORCE(it, folder, k_temp, every, header, dz, dr, Lz, Lr, normal, dt, wall_pos)
%% Magnetic field difference
%PLOT_MGF(it, folder, k_temp, every, header, dz, dr, Lz, Lr, normal, dt, wall_pos, nz, nr)
%% Detchment survey
%plot_VISUALIZE_VECTOR(it, folder, k_temp, every, header, dz, dr, Lz, Lr, normal, dt, wall_pos, nz, nr, pos1, vel1);
end


function plot_VISUALIZE_VECTOR(it, folder, k_temp, every, header, dz, dr, Lz, Lr, normal, ~, wall_pos, nz, nr, pos_temp, vel_temp)
mmeter = 1e3;
us = 1e6;
rmax = 1.5;
zmax = 2.;
close all
fsize = 40;
pos_temp(:,1) = pos_temp(:,1) - wall_pos;


%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
filename = 'vector';
str = strcat('seminar/', header, 'CASE', num2str(k_temp, '%06u'), filename, num2str(it));

f1 = figure(3);
N = 1000;
vlength = max(sqrt(vel_temp(1:N,1).^2 + vel_temp(1:N,2).^2));
s1 = quiver((pos_temp(1:N,1))*mmeter,pos_temp(1:N,2)*mmeter,vel_temp(1:N,1)/vlength, vel_temp(1:N,2)/vlength, 3);
set(s1, 'LineWidth', 1.2);
set(gca, 'FontSize', fsize*0.8);
hold on
%%%%%%%%%%%%%
%Plot
loadstr = strcat('mf_temp',num2str(it,'%08d'),'.mat');
load(loadstr);
loadstr = strcat('mf_ext','.mat');
load(loadstr);

%%f = figure('visible','off');
fsize = 35;

%zidx = ((0-dz/2:dz:Lz+dz/2) - wall_pos)*mmeter;
%ridx = (0-dr/2:dr:Lr+dr/2)*mmeter;
zidx = ((0:2*dz:Lz)-wall_pos)*mmeter;
ridx = (0:2*dr:Lr)*mmeter;
dmgf = mf_temp - mf_ext;
dmgf = dmgf(1:2:end, 1:2:end, :);

[gr, gz] = meshgrid(ridx,zidx);
mlength = sqrt(dmgf(:,:,1).^2 + dmgf(:,:,2).^2);
mmlength = max(mlength(:));
s2 = quiver(gz', gr', dmgf(:,:,1)'/mmlength, dmgf(:,:,2)'/mmlength,'r');
set(s2, 'LineWidth', 1.5);
set(s2, 'AutoScale', 'on');
set(s2, 'MaxHeadSize', 0.6);
set(s2, 'AutoScaleFactor', 1.8); 
%%%%%%%%%%%%%%%%%
hold off
colormap(jet);

grid on
xlabel('Axial position [mm]', 'FontSize', fsize);
ylabel('Radial position [mm]', 'FontSize', fsize);
axis([0, 12, 0, 8]);
%caxis([0e4*mmeter/us 3e4*mmeter/us]);
grid on

SaveImage(gcf, 1000, 800, str);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
filename = 'dvector';
str = strcat('seminar/', header, 'CASE', num2str(k_temp, '%06u'), filename, num2str(it));

f1 = figure(3);
N = 1000;

%%%%
vlength = max(sqrt(vel_temp(1:N,1).^2 + vel_temp(1:N,2).^2));
s1 = quiver((pos_temp(1:N,1))*mmeter,pos_temp(1:N,2)*mmeter,vel_temp(1:N,1)/vlength, vel_temp(1:N,2)/vlength, 3);
set(s1, 'LineWidth', 1.2);
set(gca, 'FontSize', fsize*0.8);
hold on
%%%%%%%%%%%%%
%Plot


%f = figure('visible','off');
fsize = 35;

%zidx = ((0-dz/2:dz:Lz+dz/2) - wall_pos)*mmeter;
%ridx = (0-dr/2:dr:Lr+dr/2)*mmeter;
zidx = ((0:2*dz:Lz)-wall_pos)*mmeter;
ridx = (0:2*dr:Lr)*mmeter;
dmgf = mf_temp;
dmgf = dmgf(1:2:end, 1:2:end, :);

[gr, gz] = meshgrid(ridx,zidx);
mlength = sqrt(dmgf(:,:,1).^2 + dmgf(:,:,2).^2);
mmlength = max(mlength(:));
s2 = quiver(gz', gr', dmgf(:,:,1)'/mmlength, dmgf(:,:,2)'/mmlength,'r');
set(s2, 'LineWidth', 1.5);
set(s2, 'AutoScale', 'on');
set(s2, 'MaxHeadSize', 0.6);
set(s2, 'AutoScaleFactor', 1.8); 
%%%%%%%%%%%%%%%%%
hold off
colormap(jet);

grid on
xlabel('\bf Axial position [mm]', 'FontSize', fsize);
ylabel('\bf Radial position [mm]', 'FontSize', fsize);
axis([0, 12, 0, 8]);
%caxis([0e4*mmeter/us 3e4*mmeter/us]);
grid on

SaveImage(gcf, 1000, 800, str);





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
filename = 'udvector';
loadstr = strcat('J_temp',num2str(it,'%08d'),'.mat');
load(loadstr);

loadstr = strcat('n_temp',num2str(it,'%08d'),'.mat');
load(loadstr);
n_temp = n_temp + 1e20;

str = strcat('seminar/', header, 'CASE', num2str(k_temp, '%06u'), filename, num2str(it));

f1 = figure(3);
N = 1000;


zidx = ((0-dz/2:2*dz:Lz+dz/2)-wall_pos)*mmeter;
ridx = (0-dr/2:2*dr:Lr+dr/2)*mmeter;
[gr, gz] = meshgrid(ridx,zidx);
%%%%
u_temp = J_temp(1:2:end, 1:2:end,:)./repmat(n_temp(1:2:end,1:2:end), [1 1 3]);
s1 = quiver(gz', gr', u_temp(:,:,1)', u_temp(:,:,2)', 3);
set(s1, 'LineWidth', 1.2);
set(gca, 'FontSize', fsize*0.8);
hold on




dmgf = mf_temp;
dmgf = dmgf(1:2:end, 1:2:end, :);

zidx = ((0:2*dz:Lz)-wall_pos)*mmeter;
ridx = (0:2*dr:Lr)*mmeter;
[gr, gz] = meshgrid(ridx,zidx);
mlength = sqrt(dmgf(:,:,1).^2 + dmgf(:,:,2).^2);
mmlength = max(mlength(:));
s2 = quiver(gz', gr', dmgf(:,:,1)'/mmlength, dmgf(:,:,2)'/mmlength,'r');
set(s2, 'LineWidth', 1.5);
set(s2, 'AutoScale', 'on');
set(s2, 'MaxHeadSize', 0.6);
set(s2, 'AutoScaleFactor', 1.8); 
%%%%%%%%%%%%%%%%%
hold off
colormap(jet);

grid on
xlabel('\bf Axial position [mm]', 'FontSize', fsize);
ylabel('\bf Radial position [mm]', 'FontSize', fsize);
axis([0, 12, 0, 8]);
%caxis([0e4*mmeter/us 3e4*mmeter/us]);
grid on

SaveImage(gcf, 1000, 800, str);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
filename = 'uvector';
loadstr = strcat('J_temp',num2str(it,'%08d'),'.mat');
load(loadstr);

loadstr = strcat('n_temp',num2str(it,'%08d'),'.mat');
load(loadstr);
n_temp = n_temp + 1e20;

str = strcat('seminar/', header, 'CASE', num2str(k_temp, '%06u'), filename, num2str(it));

f1 = figure(3);
N = 1000;


zidx = ((0-dz/2:2*dz:Lz+dz/2)-wall_pos)*mmeter;
ridx = (0-dr/2:2*dr:Lr+dr/2)*mmeter;
[gr, gz] = meshgrid(ridx,zidx);
%%%%
u_temp = J_temp(1:2:end, 1:2:end,:)./repmat(n_temp(1:2:end,1:2:end), [1 1 3]);
s1 = quiver(gz', gr', u_temp(:,:,1)', u_temp(:,:,2)', 3);
set(s1, 'LineWidth', 1.2);
set(gca, 'FontSize', fsize*0.8);
hold on




dmgf = mf_temp - mf_ext;
dmgf = dmgf(1:2:end, 1:2:end, :);

zidx = ((0:2*dz:Lz)-wall_pos)*mmeter;
ridx = (0:2*dr:Lr)*mmeter;
[gr, gz] = meshgrid(ridx,zidx);
mlength = sqrt(dmgf(:,:,1).^2 + dmgf(:,:,2).^2);
mmlength = max(mlength(:));
s2 = quiver(gz', gr', dmgf(:,:,1)'/mmlength, dmgf(:,:,2)'/mmlength,'r');
set(s2, 'LineWidth', 1.5);
set(s2, 'AutoScale', 'on');
set(s2, 'MaxHeadSize', 0.6);
set(s2, 'AutoScaleFactor', 1.8); 
%%%%%%%%%%%%%%%%%
hold off
colormap(jet);

grid on
xlabel('\bf Axial position [mm]', 'FontSize', fsize);
ylabel('\bf Radial position [mm]', 'FontSize', fsize);
axis([0, 12, 0, 8]);
%caxis([0e4*mmeter/us 3e4*mmeter/us]);
grid on

SaveImage(gcf, 1000, 800, str);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

filename = 'guvector';
loadstr = strcat('J_temp',num2str(it,'%08d'),'.mat');
load(loadstr);

loadstr = strcat('n_temp',num2str(it,'%08d'),'.mat');
load(loadstr);

loadstr = strcat('gvel_temp',num2str(it,'%08d'),'.mat');
load(loadstr);


n_temp = n_temp(2:end-1,2:end-1);
J_temp = J_temp(2:end-1,2:end-1,:);
g_temp(:,:,1) = reshape(gvel_temp(:,1), 79, 79);
g_temp(:,:,2) = reshape(gvel_temp(:,2), 79, 79);
g_temp(:,:,3) = reshape(gvel_temp(:,3), 79, 79);

u_temp = J_temp(1:2:end, 1:2:end,:)./repmat(n_temp(1:2:end,1:2:end), [1 1 3]);

str = strcat('seminar/', header, 'CASE', num2str(k_temp, '%06u'), filename, num2str(it));

f1 = figure(3);
N = 1000;


zidx = ((0-dz/2:2*dz:Lz+dz/2)-wall_pos)*mmeter;
ridx = (0-dr/2:2*dr:Lr+dr/2)*mmeter;
[gr, gz] = meshgrid(ridx,zidx);
%%%%

s1 = quiver(gz', gr', u_temp(:,:,1)', u_temp(:,:,2)', 3);
set(s1, 'LineWidth', 1.2);
set(gca, 'FontSize', fsize*0.8);
hold on




dmgf = mf_temp - mf_ext;
dmgf = dmgf(1:2:end, 1:2:end, :);

zidx = ((0:2*dz:Lz)-wall_pos)*mmeter;
ridx = (0:2*dr:Lr)*mmeter;
[gr, gz] = meshgrid(ridx,zidx);
mlength = sqrt(dmgf(:,:,1).^2 + dmgf(:,:,2).^2);
mmlength = max(mlength(:));
s2 = quiver(gz', gr', dmgf(:,:,1)'/mmlength, dmgf(:,:,2)'/mmlength,'r');
set(s2, 'LineWidth', 1.5);
set(s2, 'AutoScale', 'on');
set(s2, 'MaxHeadSize', 0.6);
set(s2, 'AutoScaleFactor', 1.8); 
%%%%%%%%%%%%%%%%%
hold off
colormap(jet);

grid on
xlabel('\bf Axial position [mm]', 'FontSize', fsize);
ylabel('\bf Radial position [mm]', 'FontSize', fsize);
axis([0, 12, 0, 8]);
%caxis([0e4*mmeter/us 3e4*mmeter/us]);
grid on

SaveImage(gcf, 1000, 800, str);





end



function PLOT_NDEN(iteration, folder, k_temp, every, header, dz, dr, Lz, Lr, normal, dt, wall_pos)
loadstr = strcat('n_temp',num2str(iteration,'%08d'),'.mat');
load(loadstr);

mmeter = 1e3;
x =  ([-1/2*dr:dr:Lr+1/2*dr])*mmeter;
z =  ([-1/2*dz:dz:Lz+1/2*dz] -wall_pos)*mmeter;
f = figure('visible', 'off');
hold on
colormap jet


fsize = 60;

M=10; S = 5; K = 1;
denom = M+4*S+4*K;
H = 1/denom * [K, S, K;
    S, M, S;
    K, S, K];
n_temp = imfilter(n_temp, H, 'symmetric');
%n_temp(:,2) = n_temp(:,3);
imagesc(z, x, (n_temp)'*normal.NDEN);
set(gca, 'FontSize', fsize);
%title(strcat('Number density, iteration=', num2str(iteration, '%06u')));
xlim([0, z(end)]);
ylim([0, x(end)]);
 xlabel('\bf Axial position [mm]', 'FontSize', fsize);
ylabel('\bf Radius [mm]', 'FontSize', fsize');
colorbar
axis xy
SaveImage(f, 1400, 1200, strcat('seminar/', header, 'CASE', num2str(k_temp, '%06u'), 'NDEN_iter', num2str(iteration, '%06u')));

%% Reference frame velocity
for k = 1:3
vdidx  = [1.0e4 1.5e4 2.0e4];
vdrift = vdidx(k);
zmin = 2. + dt*iteration*vdrift*mmeter;
number = zmin*1e-3/normal.LENGTH/dz;
number = floor(number)

%%
fsize = 50;
f2 = figure(k);

hold on

us = 1e6;
c_time = every*normal.TIME*dt*us;

try
    cm3 = 1e-6;
h1 = plot(x,(n_temp(number,:)  )*normal.NDEN*cm3);


set(gca,'yscale','log');
set(gca, 'FontSize', fsize);

%title(strcat('Number density, Every ', num2str(c_time, '%.2g'), ' \mu s'));
xlim([0 15])
ylim([2.5e18*cm3 6e23*cm3]);
 xlabel('\bf Radius [mm]', 'FontSize', fsize);
ylabel('\bf Number density [/cm3]', 'FontSize', fsize);
grid on
hold off
SaveImage(f2, 1600, 1200, strcat('seminar/', header, 'CASE', num2str(k_temp, '%06u'), 'SLICENDEN', num2str(vdrift)));
catch error
    disp('error caught in NDEN');

end
figure(5);
end
%{
f3 = figure(49); 
set(gca, 'FontSize', fsize);
hold on
load(strcat('mf_temp',num2str(iteration, '%08d')));
mz = mf_temp(:,:,1);
x = normal.LENGTH * [0:dr:Lr]*mmeter;
h2 = plot( x,(mz(number,:) )*normal.B0);
%set(h2, 'LineWidth', 1);
%title(strcat('Axial Magnetic Field: Every ', num2str(c_time, '%.2g'), ' \mu s'));
xlim([0 15]);
ylim([0 1.0]);
 xlabel('\bf Radius [mm]', 'FontSize', fsize);
ylabel('\bf Field intensity [T]', 'FontSize', fsize);
grid on
hold off
SaveImage(f3, 1400, 1200, strcat('seminar/', header, 'CASE', num2str(k_temp, '%06u'), 'MZ'));
catch error
    doerror = 1
end
%}

end

function PLOT_SLICE_FORCE(it, folder, k_temp, every, header, dz, dr, Lz, Lr, normal, dt, wall_pos)
loadstr = strcat('n_temp',num2str(it,'%08d'),'.mat');
load(loadstr);
loadstr = strcat('J_temp',num2str(it,'%08d'),'.mat');
load(loadstr);
loadstr = strcat('ef_temp',num2str(it,'%08d'),'.mat');
load(loadstr);
loadstr = strcat('mf_temp',num2str(it,'%08d'),'.mat');
load(loadstr);

QE = 1.6e-19;
mmeter = 1e3;
us = 1e6;


fsize = 50;
f = figure('visible','off');clf;
set(gca, 'FontSize', fsize);
try
for vd = [1.0e4, 1.5e4, 2.0e4]
zmin = normal.TIME*dt*it*vd + wall_pos;
    

n_temp = n_temp+1e20;

temp = zmin / dr + 1;
idx = round(temp);
hz = temp - idx;
uit0 = (J_temp(idx,1:end-1,3)./n_temp(idx,1:end-1)+J_temp(idx,2:end,3)./n_temp(idx,2:end))/2/QE;
uit1 = (J_temp(idx+1,1:end-1,3)./n_temp(idx+1,1:end-1)+J_temp(idx+1,2:end,3)./n_temp(idx+1,2:end))/2/QE;
uit = uit0*(1-hz) + uit1*hz;
uit(isnan(uit)) = 0;
ef0 = (ef_temp(idx,1:end-1,2)+ef_temp(idx,2:end,2))/2;
ef1 = (ef_temp(idx+1,1:end-1,2)+ef_temp(idx+1,2:end,2))/2;
eft = ef0*(1-hz) + ef1*hz;
mft = (1-hz)*mf_temp(idx,:,1) + hz*mf_temp(idx+1,:,1);
r = 0:dr:Lr;
plot(r*mmeter,QE*((eft)+mft.*uit));
hold on 
%xlim([0 Lr*mmeter]);
xlim([0 15]);
%ylim([-5000, 50]);
xlabel('\bf Radius [mm]', 'FontSize', fsize);
ylabel('\bf Radial force [N]', 'FontSize', fsize);
hold on
legend('\bf 10 mm/us ','\bf 15 mm/us ','\bf 20 mm/us','Location','southeast');

end
plot(r*mmeter, QE*eft, 'k--','LineWidth', 1.5);
plot(r*mmeter, QE*mft.*uit, 'b--','LineWidth', 1.5);
%legend('\bf 10 mm/us ','\bf 15 mm/us ','\bf 20 mm/us','\bf Electrostatic force','\bf Lorentz force','Location','southeast');
l = legend('\bf 10 mm/us ','\bf 15 mm/us ','\bf 20 mm/us','\bf Electrostatic','\bf Lorentz force','Location','southeast');
l.Position = [0.6 0.2 0.3 0.3];

hold off
str = strcat('CASE', num2str(k_temp, '%06u'), 'RFORCE', '_iter', num2str(it, '%06u'), '_TIMEINUS', num2str(dt*it*normal.TIME*us),'.pdf');
SaveImage(f, 1400, 1200, strcat('seminar/', header, 'CASE', num2str(k_temp, '%06u'), str));
catch exception
    disp('Exception occured, possibly reference frame out of range');
end
    
end

%{
function PLOT_SLICE(normal, dt, it, pos2, vel2, c, flag, k_temp, header, wall_pos, Lr)
% Compensate wall
pos2(:,1) = pos2(:,1) - wall_pos;
mmeter = 1e3;
%% R-phase
for vd = [1.0e4, 1.5e4, 2.0e4]
zmin = normal.TIME*dt*it*vd;
thickness = 0.5/mmeter; % thickness of reference frame for the r-phase evalusation
flag = (zmin<pos2(:,1)) .* ((zmin+ thickness)>pos2(:,1));
flag = flag.*((Lr-wall_pos)>pos2(:,1));
flag = logical(flag);
c2 = c(flag, :);


rmax = 30;
us = 1e6;

posr = pos2( flag, 2 );
velr = vel2( flag, 2 );

NP = sum(posr(:,1)~=0);
if NP > 100000
    NP=100000;
end

%    f = figuRPre(320001);
f = figure('visible','off');
colormap jet
emittance = sqrt(mean(posr.*posr)*mean(velr.*velr) - mean(posr.*velr)^2);
hand = scatter(posr(1:NP,:)*mmeter,velr(1:NP,:)*mmeter/us,4,c2(1:NP,:),'filled');
titlestr = strcat('r-phase diagram, Field: it=', num2str(it, '%06u'),', Emittance=',num2str(emittance, '%0.1e'),', Vperp=',num2str(rms(velr(1:NP)), '%0.1e'));
%title(titlestr);
 xlabel('\bf Radius [mm]');
ylabel('\bf Radial velocity [mm/us]');
axis([0, rmax, -10, 30]);
grid on


% number of particles whoes radius is less than rmax mm
np1 = sum(posr(:,1)<rmax*1e-3);

str = strcat('CASE', num2str(k_temp, '%06u'), 'RP','_framevel',num2str(vd), '_iter', num2str(it, '%06u'),'_NP1', num2str(np1));
SaveImage(f,1400, 1200,strcat('seminar/',header, str));

end



%% Z-phase
rlim = 2/mmeter;
flag2 = (rlim>pos2(:,2));
flag2 = logical(flag2);
c2 = c(flag2, :);

posz = pos2(flag2, 1);
velz = vel2(flag2, 1);

NP = sum(posz(:,1)~=0);
if NP > 100000
    NP=100000;
end
NP

f = figure('visible','off');
colormap jet
emittance = sqrt(mean(posz.*posz)*mean(velz.*velz) - mean(posz.*velz)^2);
hand = scatter(posz(1:NP,:)*mmeter,velz(1:NP,:)*mmeter/us,3,c2(1:NP,:),'filled');
titlestr = strcat('z-phase diagram, Field: it=', num2str(it, '%06u'),', Emittance=',num2str(emittance, '%0.1e'));
%title(titlestr);
xwin = 120;
ywin = 40;
xc = mean(posz*mmeter);
yc = mean(velz*mmeter/us);
xlim([0, xc+xwin/2]);
ylim([yc-ywin/2, yc+ywin/2]);
 xlabel('\bf Axial position [mm]');
ylabel('\bf Axial velocity [mm/us]');
%caxis(
grid on

str = strcat('CASE', num2str(k_temp, '%06u'), 'ZP_iter', num2str(it, '%06u'));
SaveImage(f,1400, 1200,strcat('seminar/',header, str));


end
%}

function PLOT_MGF(it, folder, k_temp, every, header, dz, dr, Lz, Lr, normal, dt, wall_pos, nz, nr)
mmeter = 1e3;

loadstr = strcat('mf_temp',num2str(it,'%08d'),'.mat');
load(loadstr);
loadstr = strcat('mf_ext','.mat');
load(loadstr);

f = figure('visible','off');
fsize = 35;


zidx = ((0-dz/2:dz:Lz+dz/2) - wall_pos)*mmeter;
ridx = (0-dr/2:dr:Lr+dr/2)*mmeter;
dmgf = mf_temp - mf_ext;
colormap jet
imagesc(zidx,ridx,dmgf(:,:,1)');
axis xy; colorbar;
axis([0, 15, 0, 15]);
cmax = 2e-4;
caxis([ min(min(min(dmgf(:,:,1))),-cmax), max(max(max(dmgf(:,:,1),cmax )))]);
xlabel('\bf Axial position [mm]', 'FontSize', fsize);
ylabel('\bf Radius [mm]', 'FontSize', fsize);
%title(strcat('Current density in theta direction, iteration=', num2str(it, '%06u')));
grid on
titlestr = strcat('seminar/', header, 'CASE', num2str(k_temp, '%06u'), 'DMGFZ_iter',num2str(it, '%06u'));
SaveImage(f, 1200, 1000, titlestr);


colormap jet
imagesc(zidx,ridx,dmgf(:,:,2)');
axis xy; colorbar;
axis([0, 15, 0, 15]);
caxis([ min(min(min(dmgf(:,:,2))),-cmax), max(max(max(dmgf(:,:,2),cmax )))]);
xlabel('\bf Axial position [mm]', 'FontSize', fsize);
ylabel('\bf Radius [mm]', 'FontSize', fsize);
%title(strcat('Current density in theta direction, iteration=', num2str(it, '%06u')));
grid on
titlestr = strcat('seminar/', header, 'CASE', num2str(k_temp, '%06u'), 'DMGFR_iter',num2str(it, '%06u'));
SaveImage(f, 1200, 1000, titlestr);


colormap jet
imagesc(zidx,ridx,dmgf(:,:,3)');
axis xy; colorbar;
axis([0, 15, 0, 15]);
caxis([ min(min(min(dmgf(:,:,3))),-cmax), max(max(max(dmgf(:,:,3),cmax )))]);
xlabel('\bf Axial position [mm]', 'FontSize', fsize);
ylabel('\bf Radius [mm]', 'FontSize', fsize);
%title(strcat('Current density in theta direction, iteration=', num2str(it, '%06u')));
grid on
titlestr = strcat('seminar/', header, 'CASE', num2str(k_temp, '%06u'), 'DMGFT_iter',num2str(it, '%06u'));
SaveImage(f, 1200, 1000, titlestr);


curlBp = cylindrical_ROT(dmgf,dr,dz,nr-1,nz-1)

end



function PLOT_PHASE(normal, dt, it, pos2, vel2, c, rflag, k_temp, header, wall_pos, Lr, Lz)
% Compensate wall
pos2(:,1) = pos2(:,1) - wall_pos;
mmeter = 1e3;
rmax = 30;
zmax = 35;
us = 1e6;
fsize = 50;

%{
%% R-phase
flag = (0<pos2(:,2)) .* ((rmax/mmeter)>pos2(:,2));
flag = flag.*((Lz-wall_pos)>pos2(:,1));
flag = logical(flag);
c2 = vel2(flag,1);


posr = pos2( flag, 2 );
velr = vel2( flag, 2 );
velz = vel2( flag, 2);


%%{

NP = sum(posr(:,1)~=0);
if NP > 100000
    NP=100000;
end
rflag = rflag(1:NP);


f = figure('visible','off');
set(gca, 'FontSize', fsize);
colormap jet
emittance = sqrt(mean(posr.*posr)*mean(velr.*velr) - mean(posr.*velr)^2);
hand = scatter(posr(1:NP,:)*mmeter.*rflag,velr(1:NP,:)*mmeter/us.*rflag,4,c2(1:NP,:)*mmeter/us,'filled');
titlestr = strcat('r-phase diagram, Field: it=', num2str(it, '%06u'),', Emittance=',num2str(emittance, '%0.1e'),', Vperp=',num2str(rms(velr(1:NP)), '%0.1e'));
%title(titlestr);
 xlabel('\bf Radius [mm]', 'FontSize', fsize, 'Interpreter', 'LaTeX');
ylabel('\bf Radial velocity [mm/$\mu$s]', 'FontSize', fsize, 'Interpreter', 'LaTeX');
axis([0, rmax, -10, 20]);
caxis([6e0 2e1]);
c = colorbar;
cstr = {'Axial velocity [mm/\mus]'};
c.Label.String = cstr;
c.Label.FontSize = fsize;
grid on


% number of particles whoes radius is less than rmax mm
np1 = sum(posr(:,1)<rmax*1e-3);

str = strcat('CASE', num2str(k_temp, '%06u'), 'RP', '_iter', num2str(it, '%06u'));
SaveImage(f,1400, 1200,strcat('seminar/',header, str));
%}

%% Z-phase
rlim = 30/mmeter;
flag2 = (pos2(:,2)>0) .* (pos2(:,2) < rlim);
flag2 = logical(flag2);
c2 = vel2(flag2, 2);

posz = pos2(flag2, 1);
velz = vel2(flag2, 1);

NP = sum(posz(:,1)~=0);
if NP > 100000
    NP=100000;
end
NP

f = figure('visible','off');
colormap jet
emittance = sqrt(mean(posz.*posz)*mean(velz.*velz) - mean(posz.*velz)^2);
hand = scatter(posz(1:NP,:)*mmeter,velz(1:NP,:)*mmeter/us,4,c2(1:NP,:)*mmeter/us,'filled');
titlestr = strcat('z-phase diagram, Field: it=', num2str(it, '%06u'),', Emittance=',num2str(emittance, '%0.1e'));
%title(titlestr);
xwin = 120;
ywin = 40;
xc = mean(posz*mmeter);
yc = mean(velz*mmeter/us);
xlim([0, zmax]);
ylim([-10 40]);
 xlabel('\bf Axial position [mm]', 'Interpreter', 'LaTeX');
ylabel('\bf Axial velocity [mm/$\mu$s]', 'Interpreter', 'LaTeX');
caxis([-5e0 2e1]);
c = colorbar;
cstr = {'Radial velocity [mm/\mus]'};
c.Label.String = cstr;
c.Label.FontSize = fsize;
grid on

str = strcat('CASE', num2str(k_temp, '%06u'), 'ZP_iter', num2str(it, '%06u'));
SaveImage(f,1400, 1200,strcat('seminar/',header, str));
%}

%% X(Y)-phase
phi = rand(numel(pos2(:,1)),1)*2*pi;
xflag = (0<pos2(:,2)) .* ((rmax/mmeter)>pos2(:,2));
xflag = xflag.*((Lz-wall_pos)>pos2(:,1));
xflag = xflag.*(vel2(:,1)>6000);
xflag = logical(xflag);
c2 = vel2(xflag,1);


posr = pos2( xflag, 2 );
posz = pos2( xflag, 1 );
velz = vel2( xflag, 1 );
velr = vel2( xflag, 2 );
velt = vel2( xflag, 3 );
phi = phi(xflag);


cosphi = cos(phi);
sinphi = sin(phi);

posx = posr.*cosphi;
velx = velr.*cosphi + velt.*sinphi;

%%{
NP = sum(posx(:,1)~=0);
if NP > 100000
   NP = 100000;
end
%}
xflag = xflag(1:NP);

fsize = 50;
%    f = figuRPre(320001);
f = figure('visible','off');
set(gca, 'FontSize', fsize);
set(gca, 'LineWidth', 1);
colormap jet

hand = scatter(posx(1:NP,:)*mmeter.*xflag,velx(1:NP,:)*mmeter/us.*xflag,4,c2(1:NP,:)*mmeter/us,'filled');
%titlestr = strcat('x(y)-phase diagram, Field: it=', num2str(it, '%06u'),', Emittance=',num2str(emittance, '%0.1e'),', Vperp=',num2str(rms(velx(1:NP)), '%0.1e'));
%title(titlestr);
xlabel('\bf x [mm]', 'FontSize', fsize);
ylabel('\bf v_x [mm/us]', 'FontSize', fsize);
axis([-rmax, rmax, -15, 15]);
caxis([6e0 2e1]);
colorbar
grid on

str = strcat('CASE', num2str(k_temp, '%06u'), 'XP', '_iter', num2str(it, '%06u'));
SaveImage(f,1400, 1000,strcat('seminar/',header, str));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% show by angle
rad = atan(velx./velz);
beta = velz/3e8;

hand = scatter(posx(1:NP,:)*mmeter.*xflag, beta(1:NP,:).*rad(1:NP,:)*mmeter.*xflag,4,c2(1:NP,:)*mmeter/us,'filled');
%titlestr = strcat('x(y)-phase diagram, Field: it=', num2str(it, '%06u'),', Emittance=',num2str(emittance, '%0.1e'),', Vperp=',num2str(rms(velx(1:NP)), '%0.1e'));
%title(titlestr);
 xlabel('  $x$ [mm]', 'Interpreter','LaTeX','FontSize',  fsize);
ylabel('  $ \beta \gamma x$'' [mrad]', 'Interpreter','LaTeX','FontSize', fsize);
axis([-rmax, rmax, -0.03, 0.03]);
caxis([6e0 2e1]);
c = colorbar;
cstr = {'Axial velocity [mm/\mus]'};
c.Label.String = cstr;
c.Label.FontSize = 40;

grid on

str = strcat('CASE', num2str(k_temp, '%06u'), 'AP', '_iter', num2str(it, '%06u'));
SaveImage(f,1400, 1000,strcat('seminar/',header, str));


end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function EVAL_EMITTANCE(normal, dt, it, pos2, vel2, c, rflag, k_temp, header, wall_pos, Lr, Lz, L, angle, p_weight)
% Recalculate solid angle in degrees
%angle = atan(diam/2/L)*180/pi;
% Compensate wall
pos2(:,1) = pos2(:,1) - wall_pos;
mmeter = 1e3;
rmax = 30;
zmax = 35;
us = 1e6;

%% X(Y)-phase
phi = rand(numel(pos2(:,1)),1)*2*pi;
xflag = (0<pos2(:,2)) .* ((rmax/mmeter)>pos2(:,2));
xflag = xflag.*((Lz-wall_pos)>pos2(:,1));
xflag = xflag.*(vel2(:,1)>6000);
xflag = logical(xflag);
c2 = vel2(xflag,1);


posr = pos2( xflag, 2 );
posz = pos2( xflag, 1 );
velz = vel2( xflag, 1 );
velr = vel2( xflag, 2 );
velt = vel2( xflag, 3 );
phi = phi(xflag);


cosphi = cos(phi);
sinphi = sin(phi);

posx = posr.*cosphi;
velx = velr.*cosphi + velt.*sinphi;
posy = posr.*sinphi;
vely = velr.*sinphi + velt.*cosphi;


%SaveImage(f,1400, 1000,strcat('seminar/',header, str));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% show by angle
% L     : position of circle from the target
% angle : solid angle
radx = atan(velx./velz);
rady = atan(vely./velz);
beta = velz/3e8;

dphi = angle*pi/180;



%% Which appears inside a circle
posx_L = (posx+velx./velz.*(L-posz));
posy_L = (posy+vely./velz.*(L-posz));
flag_c = sqrt(posx_L.^2+posy_L.^2)<L*tan(dphi);
flag_c = logical(flag_c);


%% Visualize usable particle positions on 3d diagram
%{
NP=100000;
sz = 2;
colormap(cool);
scatter3(posx(1:NP)*mmeter,posz(1:NP)*mmeter,posy(1:NP)*mmeter,sz,flag_c(1:NP), 'filled');
grid on
view(70,30)
radius = 17;
xlim([-radius radius])
zlim([-radius radius])
ylim([10 35])
xlabel('X [mm]');
zlabel('Y [mm]');
ylabel('Z [mm]');
set(gcf,'pos',[10 10 1200 1000])


SaveImage(gcf, 1200, 1000, strcat('seminar/',header, 'CASE', num2str(k_temp, '%06u'), '_L',num2str(L*100, '%06u'), '_ANGLE',num2str(round(angle))));

%}

%% Reduce particle nums for emittance calculation
%%{
beta = beta(flag_c);
posx = posx(flag_c);
radx = radx(flag_c);
velz = velz(flag_c);
posz = posz(flag_c);





%% Emittance calculation
    function [emittance, br, np] = calc_emittance(posx, rad, velz, vd, diff)
        vdp = vd+diff;
        vdm = vd-diff;
        flag = find( (velz>vdm) .*  (velz<vdp));
%        c0 = 3e8;
        QE=1.6e-19;
        posx = posx(flag);
        rad = rad(flag);
%        beta_temp = beta(flag);
        beta_temp = vd/3e8;
        emittance = beta_temp .* sqrt(mean(posx.*posx)*mean(rad.*rad) - mean(posx.*rad)^2);
        np = numel(flag)*p_weight;
        br = QE*np*vd/emittance^2;
        
        %% modify unit from m to mm
        emittance = emittance*1e6;
        % m to mm plus A to mA
        br = br/1e6/1e6*1e3;
    end

fileID = fopen('EMI_BRI.txt','a');
%fprintf(fileID,'%6s %12s\n','x','exp(x)');
fprintf(fileID,'CASE %6s ITERATION %6s L %6s dphi %6s\n',num2str(k_temp),num2str(it),num2str(L),num2str(angle));

diff = 1000;
% Frame 20
[e20,b20,np20] = calc_emittance(posx,radx,velz,20000, diff);
% Frame 15
[e15,b15,np15] = calc_emittance(posx,radx,velz,15000, diff);
% Frame 10
[e10,b10,np10] = calc_emittance(posx,radx,velz,10000, diff);
A = [e20, e15, e10];
B = [b20, b15, b10];
C = [np20, np15, np10];
fprintf(fileID,'%6s %6s %6s %6s %6s %6s %6s %6s %6s\n','e20','e15','e10','b20','b15','b10','np20','np15','np10');
fprintf(fileID,'%6.2e %6.2e %6.2e %6.2e %6.2e %6.2e %6.2e %6.2e %6.2e\n',[A, B, C]);
fclose(fileID);
%}
end






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function PLOT_PROFILE( c, pos1, NP, it, dr, dz, Lr, Lz, mf_temp, k_temp, header, wall_pos, vel1)
mmeter = 1e3;
us = 1e6;

pos1(:,1) = pos1(:,1) - wall_pos;

f = figure('visible','off');
%f =figure;
colormap jet
NP = 500000;
NP2 = sum(pos1(:,1)~=0);
if NP > NP2
    NP=NP2;
end

fsize = 50;

scatter(pos1(1:NP,1)*mmeter, pos1(1:NP,2)*mmeter,1,c(1:NP),'filled');%, 'g.');
set(gca, 'FontSize', 0.7*fsize);
hold on
x =  [-1/2*dr:dr:Lr+1/2*dr]*mmeter;
z =  ([-1/2*dz:dz:Lz+1/2*dz] - wall_pos)*mmeter;
axis([0, z(end), 0, x(end)]);
 xlabel('\bf Axial position [mm]', 'FontSize', fsize);
ylabel('\bf Radius [mm]', 'FontSize', fsize);
%title(strcat('Geometry of plasma, iteration=', num2str(it, '%06u')));%, ' Particle=', num2str(np) ));
grid on
hold on

[R,Z] = meshgrid(0:dr:Lr, 0:dz:Lz);
Z = (Z - wall_pos)*mmeter;
R = R*mmeter;
hlines = streamslice(Z', R', mf_temp(:,:,1)', mf_temp(:,:,2)');
set(hlines, 'Color','b');
set(hlines, 'LineWidth', 1);
hold off

SaveImage(f, 1800, 1200, strcat('seminar/',header, 'CASE', num2str(k_temp, '%06u'), 'PROF_INITPOS_iter',num2str(it, '%06u')));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Colored by radial velocity
scatter(pos1(1:NP,1)*mmeter, pos1(1:NP,2)*mmeter,1,vel1(1:NP,2)*mmeter/us,'filled');%, 'g.');
set(gca, 'FontSize', 0.7*fsize);
hold on
x =  [-1/2*dr:dr:Lr+1/2*dr]*mmeter;
z =  ([-1/2*dz:dz:Lz+1/2*dz] - wall_pos)*mmeter;
axis([0, z(end), 0, x(end)]);
colormap(jet);
colorbar('FontSize', fsize);
caxis([-5 15]);
xlabel('\bf Axial position [mm]', 'FontSize', fsize);
ylabel('\bf Radius [mm]', 'FontSize', fsize);
%title(strcat('Geometry of plasma, iteration=', num2str(it, '%06u')));%, ' Particle=', num2str(np) ));
grid on
hold on

[R,Z] = meshgrid(0:dr:Lr, 0:dz:Lz);
Z = (Z - wall_pos)*mmeter;
R = R*mmeter;
hlines = streamslice(Z', R', mf_temp(:,:,1)', mf_temp(:,:,2)');
set(hlines, 'Color','b');
set(hlines, 'LineWidth', 1);
hold off

SaveImage(f, 1800, 1200, strcat('seminar/',header, 'CASE', num2str(k_temp, '%06u'), 'PROF_VELR_iter',num2str(it, '%06u')));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Colored by radial velocity
scatter(pos1(1:NP,1)*mmeter, pos1(1:NP,2)*mmeter,1,vel1(1:NP,1)*mmeter/us,'filled');%, 'g.');
set(gca, 'FontSize', 0.7*fsize);
hold on
x =  [-1/2*dr:dr:Lr+1/2*dr]*mmeter;
z =  ([-1/2*dz:dz:Lz+1/2*dz] - wall_pos)*mmeter;
axis([0, z(end), 0, x(end)]);
colormap(jet);
colorbar('FontSize', fsize);
caxis([-5 25]);
xlabel('\bf Axial position [mm]', 'FontSize', fsize);
ylabel('\bf Radius [mm]', 'FontSize', fsize);
%title(strcat('Geometry of plasma, iteration=', num2str(it, '%06u')));%, ' Particle=', num2str(np) ));
grid on
hold on

[R,Z] = meshgrid(0:dr:Lr, 0:dz:Lz);
Z = (Z - wall_pos)*mmeter;
R = R*mmeter;
hlines = streamslice(Z', R', mf_temp(:,:,1)', mf_temp(:,:,2)');
set(hlines, 'Color','b');
set(hlines, 'LineWidth', 1);
hold off

SaveImage(f, 1800, 1200, strcat('seminar/',header, 'CASE', num2str(k_temp, '%06u'), 'PROF_VELZ_iter',num2str(it, '%06u')));




end

function PLOT_CURRENT_DENSITY(dz,dr, Lz, Lr, J, mmeter, it, k_temp, header, wall_pos)
f = figure('visible','on');
fsize = 35;
set(gca, 'FontSize', fsize);
%imagesc(dz*(nz-1),dr*(nr-1), Jtheta');
M=10; S = 5; K = 1;
denom = M+4*S+4*K;
H = 1/denom * [K, S, K;
    S, M, S;
    K, S, K];
J(:,:,1) = imfilter(J(:,:,1), H, 'symmetric');
J(:,:,2) = imfilter(J(:,:,2), H, 'symmetric');
J(:,:,3) = imfilter(J(:,:,3), H, 'symmetric');

zidx = ((0-dz/2:dz:Lz+dz/2) - wall_pos)*mmeter;
ridx = (0-dr/2:dr:Lr+dr/2)*mmeter;
colormap jet
cm2 = 1e4;
imagesc(zidx,ridx,J(:,:,3)'/cm2);
axis xy; 
caxis([ min(min(min(J(:,:,3)/cm2)),-80), max(max(max(J(:,:,3),2 )))/cm2]);
%caxis([ -1000 20])
c = colorbar();
%c.Label.String = '\bf Current density [A/cm2]';
axis([0, 15, 0, 15]);
xlabel('\bf Axial position [mm]', 'FontSize', fsize);
ylabel('\bf Radius [mm]', 'FontSize', fsize);
%title(strcat('Current density in theta direction, iteration=', num2str(it, '%06u')));
grid on
SaveImage(f, 1000, 700, strcat('seminar/', header, 'CASE', num2str(k_temp, '%06u'), 'CURR_iter',num2str(it, '%06u')));
end

function out = field_expand(A)
X = padarray(A,[1 1 0],'replicate','both');
if (ndims(X)==2)
    X(:,1) = X(:,3);
elseif (ndims(X)==3)
    % Boundary condition on the axis
    X(:,1,1) = X(:,3,1);    % z direction on the axis
    X(:,1,2:3) = -X(:,3,2:3);   % r,theta direction on the axis
end
out = X;
end

function [] = SaveImage(figure, width, height, filename)
set(figure, 'PaperPositionMode', 'auto');
position = get(figure, 'Position');
position(3) = width;
position(4) = height;
set(gcf,'Position', position);
set(gcf, 'PaperSize', [width/100+2 height/100+2]);
saveas(gcf, filename, 'png');
end


function curlA = cylindrical_ROT(A,dr,dz,nr,nz)

% calculate curlA from electric field A
% no changes in theta direction

            curlA = zeros(nz,nr,3);
            gr_A = (meshgrid(1:nr+1,1:nz+1)-2.);       
            gr_A2 = abs(gr_A(1:end-1,1:end-1) + 0.5);
            % Apply rotation operator to magnetic field
            % z direction
            
            rAtheta = (gr_A).*A(:,:,3);         % r*Atheta
            temp = ( rAtheta(:,2:end) - rAtheta(:,1:end-1) )/dr;
            drAtheta_dr = (temp(1:end-1,:) + temp(2:end,:))/2;
            curlA(:,:,1) = drAtheta_dr./(gr_A2);
            % r direction
            temp = (A(2:end,:,3) - A(1:end-1,:,3))/dz;
            dAtheta_dz = (temp(:,2:end) + temp(:,1:end-1))/2;
            curlA(:,:,2) = -dAtheta_dz;
            % theta direction
            temp = (A(2:end,:,2) - A(1:end-1,:,2))/dz;
            dAr_dz = (temp(:,1:end-1) + temp(:,2:end))/2;
            temp = (A(:,2:end,1) - A(:,1:end-1,1))/dr;
            dAz_dr = (temp(1:end-1,:) + temp(2:end,:))/2;
            curlA(:,:,3) = dAr_dz - dAz_dr;
            
            % Same value on edge since they are just extended and
            % doesn't conserve vector potential
            curlA(1  , :,  1:3) = curlA(2,:,1:3);
            curlA(end, :,  1:3) = curlA(end-1,:,1:3);
            curlA(:  , 1,  1)   = curlA(:,2,1);
            curlA(:  , 1,  2:3) = - curlA(:,2,2:3);
            curlA(:  , end,1:3) = curlA(:,end-1,1:3);
            

end