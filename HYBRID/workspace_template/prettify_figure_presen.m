function [] = prettify_figure_presen()
%PRETTIFY_FIGURE_PRESEN グラフのフォントをプレゼン用に最適化
% この関数を呼び出すと
% 以降に描かれるプロットの見た目がプレゼン向けになります．
% 元に戻すにはMATLABを再起動するしかないです．
%
% ※ ボード線図は別口(prettify_bodeplot_presen参照)
%    なので注意してください．


%% フォント
set(0,'defaultAxesFontSize',35);
set(0,'defaultAxesFontName','メイリオ');
set(0,'defaultAxesFontWeight','demi'); % normal/demi/bold
set(0,'defaultTextFontSize',35);
set(0,'defaultTextFontName','メイリオ');
set(0,'defaultTextFontWeight','demi');

%% 線の幅
set(0,'defaultAxesLineWidth', 1.5); % 軸
set(0,'defaultLineLineWidth', 3); % プロット

%% プロットの色
%{
clorder = ...
    [  0,   0, 255; % 青
       0, 128,   0; % 緑
     255,   0,   0; % 赤
     204,   8, 204; % 紫
     222, 125,   0; % 茶
       0,  51, 153; % 紺 (青と区別しづらい，注意！)
      64,  64,  64];% 濃い灰色  
set(0,'defaultAxesColorOrder',clorder/255);
%}

%% 最初からgrid on & box on
set(0,'DefaultAxesXGrid','on');
set(0,'DefaultAxesYGrid','on');
set(0,'DefaultAxesBox','on');

%% コピペ時にサイズを変更しない
set(0,'DefaultFigurePaperPositionMode','auto');

end