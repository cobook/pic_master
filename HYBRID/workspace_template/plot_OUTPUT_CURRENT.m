range = 80;
f = figure(range);
clf;
%mT = 110;
mT = [0 55 82 110 137];
for temp = mT;
    switch temp
        case 110
            folder = 'filtered_110mT_moment';
            %dt = 2.e-5;
        case 82
            folder = 'filtered_82mT_moment';
        case 0
            folder = 'filtered_0mT_moment';
        case 137 
            folder = 'filtered_137mT_moment';
        case 55
            folder = 'filtered_55mT_moment';
        case -1
            folder = 'result1124_0247_110mT_-1mm';
            dt = 2.e-5;
        case 1
            folder = 'result1120_2141_110mT_1mm';
            dt = 2.e-5;
        case 2
            folder = 'result1120_2143_110mT_2mm';
            dt = 2.e-5;
    end

C = load(strcat(folder,'/matlab.mat'));
node_area = C.node_volume(1,:)/C.dz;
A = load(strcat(folder,'/current.mat'));
current_density = A.store_current;
B = load(strcat(folder,'/normal.mat'));
normal = B.normal;

sample_p = C.current_sample;
[ts, num, dim] = size(current_density);

current = current_density.*repmat(node_area,[ts,1,dim]);
current = current*normal.CHARGE/normal.TIME;

x = linspace(1,ts,ts)';
x = x*normal.TIME*sample_p*C.dt;
%plot(x, current(:,1), '-b');
%figure(win)
%clf(win)
hold on;
%{
coeff = ones(1,number)/number;
repeat = 8;
for i = 2:8:80
    current_sum = 0;
    
    for j = i:i+repeat-1
        current_sum = current_sum + current(:,j,1);
    end
    current_sum = current_sum/repeat;
    current_sum = filter(coeff,1,current_sum);
    plot(x,current_sum,'-','LineWidth',2);
end
%}
current_sum = 0;

for i = 2:range
    current_sum = current_sum + current(:,i,1);
end
us = 1e6;
plot(x*us,current_sum,'-','LineWidth',2);
%plot(x, current(:,1,1), '-r', 'LineWidth', 2);%, x, current(:,1), '-r');
titlestr = strcat('Current: Radius ', num2str(range*C.dr*normal.LENGTH), 'm');
title(titlestr);
legstr = cell(size(mT,2),1);
for i = 1:size(mT,2)
   legstr(i) = {strcat(num2str(mT(i)) ,'mm')};
end
l = legend(legstr{1},legstr{2},legstr{3},legstr{4},legstr{5});%,legstr{6});%,num2str(mT(5)),'330mT','','','','','','','')
l.FontSize = 16;
xlabel(strcat('Time [','\mu','s]'));
ylabel('A');
xlim([0,10e-6*us]);
ylim([0,70]);
%hold off
grid on

end

saveas(f,strcat('seminar20170110/',titlestr,'.fig'),'fig');
saveas(f,strcat('seminar20170110/',titlestr,'.bmp'),'bmp');
saveas(f,strcat('seminar20170110/',titlestr,'.eps'),'epsc');