CYTIME = 1e-2;
% Period of displaying outcomes
PERIOD=100;

%% Particle setup
M = 1*AMU;             %ion mass (molecular Copper) [kg]
Z = 1;                  %valence
n0 = 1e16*(2.2710^2)*10/11;				%density in [/m^3]
n_thre_denom = 10;%n0;
n_thre = n0/n_thre_denom;          % threashold to calculate Electric field
Te = .000001;                 %electron temperature in [eV] 
Ti = 0.00000001;              	%ion velocity in [eV]
%calculate plasma parameters
%Ion_energy = 100*1e1;               % Accelerated energy[eV]
%v_drift = sqrt(Ion_energy*QE*2/M)/0.16257/9.537/0.34596/100;        	%ion injection velocity [m/s]
v_drift = 0;  % [m/s]
% insert inject_num_per_time particles in total every time step

%simulation domain
nz = 1024;               %number of nodes in z direction
nr = 40;                %number of nodes in r direction

range = 10;
z_inj_node = [2+range nz-1-range];
r_inj_node = [1 nr-1 - 10];


dr = 0.5;        %cell size in r direction [m]
dz = dr;
% set injection length
split = r_inj_node(2) - r_inj_node(1) + 1;
r_inject = dr*split; % beam length in r direction[m]
z_inject = 100*r_inject;  % beam length in z direction[m]
%calculate specific weight
max_part=200000;             %buffer size

inject_num_per_time = max_part;    % inject around this number of particle per time step 


%% specify plate dimensions
% On the plate grid, Er, Etheta, Ez is set to be 0
% First box
box(1,:,1) = [1, -1];  % nz direction
box(2,:,1) = [1, -1];  % nr direction
% Second box
box(1,:,2) = [1, -1];  % nz direction
box(2,:,2) = [1, -1];  % nr direction

%% FLAGS
% Reference value for renewing subcycling time to meet the demand of CFL conditions according to Alfven speed
B_REF = 1;

%boundary condition at each side ('p'eriodic or 'a'bsorb'
BOUNDARY = 'a';
% Beam profile type
% 1. UNIFORM, 2. GAUSS
BEAMTYPE='UNIFORM';


%% Read Magnetic field 
% Magnetic field type
% Currently 'NOZZLE' and 'CONST' available
MAGNETIC_FIELD='CONST';
if strcmp(MAGNETIC_FIELD, 'FILE')
    mf_ext = 1*READ_MF('MagneticField/mf_ext20160607.txt',dz,dr,nz,nr, -0.2);
elseif strcmp(MAGNETIC_FIELD, 'NOZZLE')
    mf_ext = READ_MF('MagneticField/mf_nozzle20160713.txt',dz,dr,nz,nr,100,100,0.00016);
elseif strcmp(MAGNETIC_FIELD, 'CONST')
    mf_ext = cat(3,ones(nz,nr)*1,zeros(nz,nr),zeros(nz,nr));
elseif strcmp(MAGNETIC_FIELD, 'ARBITRAL')
    mf_ext = 1e1*READ_MF('MagneticField/mf_ext20160607.txt', 0.25/nz, 0.125/nr, nz, nr,201,401,-0.3);
else
    mf_ext = 1e-30*ones(nz,nr,3);
end
