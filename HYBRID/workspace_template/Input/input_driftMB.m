%% Reference value for renewing subcycling time to meet the demand of CFL conditions according to Alfven speed
B_REF = 1;
%% Time management
CYTIME = 2e-4; % Time compared to cyclic frequency of Ions 
PERIOD = 500;  % Period of saving outcomes
acurrent_sample = 50; % Sampling period of current density. This value should be smaller than PERIOD for describing angular distribution.
ts = 60000;   % number of time steps of entire simulation
drift_t = [0.05e-6]; % initiation time of electromagnetic interaction. Until this time, ions are thought to fly ballistically.

%% Particles setup
nump = 0.6e15;   % Number of physical particles
M = 63.5*AMU;             % ion mass (Copper) [kg]
Z = 1;                    % valence
n0 = 2e24;				  % initial density of plasma[/m^3]
n_thre_denom = 20000;     % denominator to decide the threashold of number density
n_thre = n0/n_thre_denom; % minimum threashold of the number density to calculate Electric field
max_part=3000000;                  % particle buffer size
inject_num_per_time = max_part;    % inject this number of particle per time step 
Te = 0;                   % electron temperature in [eV] 
RSTY = 1e-4;              % Plasma resistivity [Ohm m]

%% These parameters will be used when you use shifted-Maxwellian particle distribution 
ratio = 1.5;            % ratio of long side and short side of ellips
Ti = 26;              	  % ion velocity dispersion [eV]
v_drift = 6e3;            % drift velocity of the center of plasma [m/s]

%% Simulation Domain setup
BOUNDARY = 'a';     %boundary condition at each side ('p'eriodic or 'a'bsorb'
nz = 100;           %number of nodes in z direction
nr = 70;            %number of nodes in r direction
dr = 0.4e-3;        %cell size in r direction [m]
dz = 0.4e-3;        %cell size in r direction [m]
wall_pos = 2e-3;    % position of target from the wall at the left hand side. [m]

%% specify plate dimensions
% On the 2 boxes ((minz,maxz, minr,maxr), Er, Etheta, Ez is set to be 0
box = zeros(2,2,2);
% First box
box(1,:,1) = [1, -1];  % z direction
box(2,:,1) = [1, -1];  % r direction
% Second box
box(1,:,2) = [1, -1];  % z direction
box(2,:,2) = [1, -1];  % r direction

%% Beam profile 
% 1. UNIFORM, 2. GAUSS, 3. EXPERIMENT
BEAMTYPE='EXPERIMENT';

% The parameters below will not be used in 'EXPERIMENT' mode, but in 'UNIFORM' injection mode
z_inj_node = [6 8]; % Injection cells where you'll initially put particles on in z direction
r_inj_node = [1 2]; % Injection cells where you'll initially put particles on in r direction
split = r_inj_node(2) - r_inj_node(1) + 1;
r_inject = dr*split; 		% beam length in r direction[m]
z_inject = 100*r_inject;  	% beam length in z direction[m]

%% Generate Magnetic field 
coil_pos(1) = coil_pos(1) + wall_pos; % modify z position of coil by adding wall position [m]
coilc1 = [coil_pos(1), coil_pos(1)+coil_length(1);coil_pos(2), coil_pos(2)+coil_length(2)]; % Position of the coil [mm]
%    coilc2 = [20 22;12 13];
%% Magnetic field generation by 'USR_COIL.m'
[mf_ext, coil1_cell] = USR_COIL(dz, dr, nz, nr, wall_pos, current, coilc1, current, coilc1);
box(:,:,1) = coil1_cell;
Baxis = max(mf_ext(:,1,1)); % Magnetic field on the axis [T]

% Determine how many times M-E leapfrog is performed. 
% This value is empirically determined
CL_iteration = max(round(Baxis*400),4);  


